package com.pti.tagshash.model.sub_category;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.pti.tagshash.model.category.CatData;

import java.io.Serializable;
import java.util.List;

public class SubCategoryContainer implements Serializable {
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<SubCategoryList> data = null;


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<SubCategoryList> getData() {
        return data;
    }

    public void setData(List<SubCategoryList> data) {
        this.data = data;
    }


}
