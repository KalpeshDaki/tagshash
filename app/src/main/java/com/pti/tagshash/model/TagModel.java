package com.pti.tagshash.model;

import java.io.Serializable;

public class TagModel implements Serializable {

    String tagString;
    String isEnable;

    public String getTagString() {
        return tagString;
    }

    public void setTagString(String tagString) {
        this.tagString = tagString;
    }

    public String isEnable() {
        return isEnable;
    }

    public void setEnable(String enable) {
        isEnable = enable;
    }
}
