package com.pti.tagshash.utils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.webkit.MimeTypeMap;
import android.widget.Toast;


import com.pti.tagshash.BuildConfig;
import com.pti.tagshash.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Locale;
import java.util.regex.Pattern;

import okhttp3.ResponseBody;


public class Utility {

    public static void hideKeyboard(View view, Context context) {
        InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static Uri getOutputMediaFileUri() {
        File file = getOutputMediaFile();
        if (file != null)
            return Uri.fromFile(file);
        else
            return null;
    }

    public static File getOutputMediaFile() {
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), Constant.IMAGE_DIRECTORY_NAME);
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                if (BuildConfig.DEBUG)
                    Log.d(Constant.IMAGE_DIRECTORY_NAME, "Oops! Failed create " + Constant.IMAGE_DIRECTORY_NAME + " directory");
                return null;
            }
        }
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        File mediaFile = new File(mediaStorageDir.getPath() + File.separator + "IMG_" + timeStamp + ".jpg");
        return mediaFile;
    }
    public static Uri getOutputMediaFileUri2() {
        File file = getOutputMediaFile2();
        if (file != null)
            return Uri.fromFile(file);
        else
            return null;
    }

    public static File getOutputMediaFile2() {
        File mediaStorageDir = new File(String.valueOf(Environment.getDownloadCacheDirectory()));
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                if (BuildConfig.DEBUG)
                    Log.d(Constant.IMAGE_DIRECTORY_NAME, "Oops! Failed create " + Constant.IMAGE_DIRECTORY_NAME + " directory");
                return null;
            }
        }
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        File mediaFile = new File(mediaStorageDir.getPath() + File.separator + "PPPP_" + timeStamp + ".xls");
        return mediaFile;
    }

    public static ProgressDialog createProgressDialog(Context context) {
        ProgressDialog dialog = new ProgressDialog(context);
        try {
            dialog.show();
        } catch (WindowManager.BadTokenException e) {

        }
        dialog.setCancelable(false);
        dialog.getWindow()
                .setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.progressdialogue);
        // dialog.setMessage(Message);
        return dialog;
    }

    public static boolean isNetworkConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }


    public static void showToast(Context ctx, String msg) {
        Toast.makeText(ctx, msg, Toast.LENGTH_SHORT).show();
    }

    public static String getMimeType(Uri uri, Context context) {
        String mimeType = null;
        if (uri.getScheme().equals(ContentResolver.SCHEME_CONTENT)) {
            ContentResolver cr = context.getContentResolver();
            mimeType = cr.getType(uri);
        } else {
            String fileExtension = MimeTypeMap.getFileExtensionFromUrl(uri
                    .toString());
            mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(
                    fileExtension.toLowerCase());
        }
        return mimeType;
    }

    public static String getImageName(Context mContext, Uri uri){
        String name=null;
        String scheme = uri.getScheme();
        if (scheme.equals("file")) {
            name = uri.getLastPathSegment();
        } else if (scheme.equals("content")) {
            String[] proj = {MediaStore.Images.Media.TITLE};
            Cursor cursor = mContext.getContentResolver().query(uri, proj, null, null, null);
            if (cursor != null && cursor.getCount() != 0) {
                int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.TITLE);
                cursor.moveToFirst();
                name = cursor.getString(columnIndex);
                String[] projection = {MediaStore.MediaColumns.DISPLAY_NAME};
                Cursor metaCursor = mContext.getContentResolver().query(uri, projection, null, null, null);
                if (metaCursor != null) {
                    try {
                        if (metaCursor.moveToFirst()) {
                            name = metaCursor.getString(0);
                        }
                    } finally {
                        metaCursor.close();
                    }
                }
            }
        }
        return name;
    }

    public static boolean isValidEmailId(String email){

        return Pattern.compile("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$").matcher(email).matches();
    }

    public static String getError(ResponseBody errorBody) {
        StringBuilder sb = new StringBuilder();
        boolean foundOne = false;
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(errorBody.string());
            if (isJSONValid(jsonObject.getString("message"))){
                Log.e("getError","if");
                JSONObject obj = new JSONObject(jsonObject.getString("message"));
                Iterator<?> iterator = obj.keys();
                while (iterator.hasNext()) {
                    Object key = iterator.next();
                    Object value = obj.get(key.toString());
                    JSONArray array = new JSONArray(value.toString());
                    if (foundOne) {
                        sb.append(",\n");
                    }
                    foundOne = true;
                    sb.append(array.get(0));
                }
            }else {
                sb.append(jsonObject.getString("message"));
                Log.e("getError","else");
            }
        } catch (JSONException | IOException e) {
            e.printStackTrace();
        }
        return sb.toString();
    }
    public static boolean isJSONValid(String test) {
        try {
            new JSONObject(test);
        } catch (JSONException ex) {
            try {
                new JSONArray(test);
            } catch (JSONException ex1) {
                return false;
            }
        }
        return true;
    }
    public static String pasrDate(String strDate) {
        String formattedDate = null;
        try {
            SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            SimpleDateFormat outputFormat = new SimpleDateFormat("dd MMMM yyyy");
            Date date = inputFormat.parse(strDate);
            formattedDate = outputFormat.format(date);
            System.out.println(formattedDate); // prints 10-04-2018
            return formattedDate;
        } catch (Exception e) {

        }
        return formattedDate;
    }
    public static String date_ddmmmmyyyy(String strDate) {
        String formattedDate = null;
        try {
            SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat outputFormat = new SimpleDateFormat("dd MMMM yyyy");
            Date date = inputFormat.parse(strDate);
            formattedDate = outputFormat.format(date);
            System.out.println(formattedDate); // prints 10-04-2018
            return formattedDate;
        } catch (Exception e) {

        }
        return formattedDate;
    }
    public static String date_yyMMdd_ddmmyyyy(String strDate) {
        String formattedDate = null;
        try {
            SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat outputFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date date = inputFormat.parse(strDate);
            formattedDate = outputFormat.format(date);
            System.out.println(formattedDate); // prints 10-04-2018
            return formattedDate;
        } catch (Exception e) {

        }
        return formattedDate;
    }

    public static void logout(Context mContext) {
       /* Intent intent = new Intent(mContext, SigninActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        AppSharedPref appSharedPref=new AppSharedPref(mContext);
        appSharedPref.setIslogin(false);
        AppSharedPref.setLoginDetail(mContext, null);
        mContext.startActivity(intent);*/
    }
}
