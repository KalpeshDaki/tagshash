package com.pti.tagshash.custome_font.button;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.EditText;

public class Button_light extends Button {

    public Button_light(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public Button_light(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public Button_light(Context context) {
        super(context);
        init();
    }

    private void init() {
        if (!isInEditMode()) {
            Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/arial_regular.ttf");
            setTypeface(tf);
        }
    }

}