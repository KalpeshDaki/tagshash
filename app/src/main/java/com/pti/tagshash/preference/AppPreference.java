package com.pti.tagshash.preference;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

public class AppPreference {

    static SharedPreferences sharedPreferences;
    static SharedPreferences sharedPreferencesFirst;
    public static final String FCM_TOKEN = "fcm_token";
    public static final String TOKEN = "Token";
    Context mContext;


    interface SharedPrefVar{
        String sharedPrefName = "Tagshash";
        String sharedPrefFirst = "Tagshashfirst";
        String isFirstTimeLaunch = "isFirstTimeLaunch";
        String isLogin = "isLogin";
    }
    public AppPreference(Context context){
        this.mContext = context;
        sharedPreferences = context.getSharedPreferences(SharedPrefVar.sharedPrefName, Context.MODE_PRIVATE);
        sharedPreferencesFirst = context.getSharedPreferences(SharedPrefVar.sharedPrefFirst, Context.MODE_PRIVATE);
    }


    public static boolean getFirstTimeLaunch(Context mContext) {
        SharedPreferences sp = mContext.getSharedPreferences(SharedPrefVar.sharedPrefFirst, Activity.MODE_PRIVATE);
        return sp.getBoolean(SharedPrefVar.isFirstTimeLaunch, false);
    }
    public static boolean setFirstTimeLaunch(Context context, boolean isFirstTime){
        SharedPreferences sp = context.getSharedPreferences(SharedPrefVar.sharedPrefFirst, Activity.MODE_PRIVATE);
        return sp.edit().putBoolean(SharedPrefVar.isFirstTimeLaunch, isFirstTime).commit();
    }

    public static boolean isLogin(Context mContext) {
        SharedPreferences sp = mContext.getSharedPreferences(SharedPrefVar.sharedPrefName, Activity.MODE_PRIVATE);
        return sp.getBoolean(SharedPrefVar.isLogin, false);
    }

    public static boolean setIslogin(Context context, boolean islogin){
        SharedPreferences sp = context.getSharedPreferences(SharedPrefVar.sharedPrefName, Activity.MODE_PRIVATE);
        return sp.edit().putBoolean(SharedPrefVar.isLogin, islogin).commit();
    }

}
