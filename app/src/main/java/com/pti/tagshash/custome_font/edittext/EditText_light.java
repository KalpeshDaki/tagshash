package com.pti.tagshash.custome_font.edittext;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;
import android.widget.TextView;

public class EditText_light extends EditText {

    public EditText_light(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public EditText_light(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public EditText_light(Context context) {
        super(context);
        init();
    }

    private void init() {
        if (!isInEditMode()) {
            Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/arial_regular.ttf");
            setTypeface(tf);
        }
    }

}