package com.pti.tagshash.activity;

import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.RequestConfiguration;
import com.pti.tagshash.R;
import com.pti.tagshash.adapter.TagsAdepter;
import com.pti.tagshash.model.TagModel;
import com.pti.tagshash.model.category.CatData;
import com.pti.tagshash.model.category.SubCategory;
import com.pti.tagshash.model.sub_category.SubCategoryContainer;
import com.pti.tagshash.model.sub_category.SubCategoryList;
import com.pti.tagshash.module.subcategory.SubCategoryPresenterImpl;
import com.pti.tagshash.module.subcategory.SubCategoryView;
import com.pti.tagshash.utils.Constant;
import com.pti.tagshash.utils.CustomeTagLayout;
import com.pti.tagshash.utils.Log;
import com.pti.tagshash.utils.Utility;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TagsListActivity extends AppCompatActivity implements SubCategoryView, View.OnClickListener, TagsAdepter.onClickTag {
    Context mContext;
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.edt_search)
    EditText edt_search;
    @BindView(R.id.mainview)
    RelativeLayout mainview;
    @BindView(R.id.check_layout)
    LinearLayout check_layout;
    @BindView(R.id.insta_layout)
    LinearLayout insta_layout;
    @BindView(R.id.copy_layout)
    LinearLayout copy_layout;
    @BindView(R.id.tag_layout)
    LinearLayout tag_layout;
    @BindView(R.id.send_layout)
    LinearLayout send_layout;
    @BindView(R.id.copy_count)
    TextView copy_count;
    @BindView(R.id.taglayout)
    CustomeTagLayout taglayout;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.adView)
    AdView adView;

    CatData catData;
    List<SubCategory> subCategory;
    TagsAdepter mAdapter;

    List<TagModel> tagModels;
    LayoutInflater layoutInflater;
    SubCategoryPresenterImpl presenter;
    ProgressDialog progressDialog;
    int count = 0;
    boolean isSelectAll = false;
    List<SubCategoryList> taglist;
    private InterstitialAd interstitial;


    String categoryId="",subcategoryId="",keywords="";
    StringBuilder sb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tags_list);
        ButterKnife.bind(this);
        mContext = TagsListActivity.this;
        if (getIntent().hasExtra(Constant.categoryData)) {
            catData = (CatData) getIntent().getSerializableExtra(Constant.categoryData);
        }
        SetUpview();

        presenter = new SubCategoryPresenterImpl(mContext, this);
        if (catData != null) {
            subCategory.addAll(catData.getSubCategory());
            if (subCategory.size() > 0) {
                tag_layout.setVisibility(View.VISIBLE);
                mAdapter = new TagsAdepter(mContext, subCategory, this);
                recyclerView.setAdapter(mAdapter);
                recyclerView.scrollToPosition(subCategory.size() - 1);
                categoryId=subCategory.get(subCategory.size() - 1).getCategoryId();
                subcategoryId=subCategory.get(subCategory.size() - 1).getId();
                keywords="";
                getHashTag(categoryId, subcategoryId, keywords);
            } else {
                tag_layout.setVisibility(View.GONE);
                categoryId=catData.getId();
                subcategoryId="";
                keywords="";
                getHashTag(categoryId,subcategoryId, keywords);
            }
        }
        //Prepair();


        /*ScheduledExecutorService scheduler =
                Executors.newSingleThreadScheduledExecutor();
        scheduler.scheduleAtFixedRate(new Runnable() {
            public void run() {
                runOnUiThread(new Runnable() {
                    public void run() {

                        if (interstitial == null) {
                            showAdsView();
                        } else {
                            if (interstitial.isLoaded()) {
                                interstitial.show();
                                Log.e("isLoaded","isLoaded");
                            } else {
                                Log.e("isLoaded","else");
                            }
                            showAdsView();
                        }
                    }
                });
            }
        }, Constant.adsTime, Constant.adsTime, TimeUnit.SECONDS);*/
       // showAdsView();
    }

    private void showAdsView() {

        AdRequest adIRequest = new AdRequest.Builder().build();
        interstitial = new InterstitialAd(TagsListActivity.this);
        interstitial.setAdUnitId(getString(R.string.full_page));
        interstitial.loadAd(adIRequest);
        interstitial.setAdListener(new AdListener() {
            public void onAdLoaded() {
                displayInterstitial();
            }
        });

    }

    public void displayInterstitial() {
        if (interstitial.isLoaded()) {
            interstitial.show();
        }
    }


    private void getHashTag(String catid, String subcatid, String keyword) {
        if (Utility.isNetworkConnected(mContext)) {
            presenter.getHashTag(catid, subcatid, keyword);
        } else {
            Utility.showToast(mContext, mContext.getResources().getString(R.string.no_internet));
        }
    }

    private void SetUpview() {
        subCategory = new ArrayList<>();
        taglist = new ArrayList<>();
        recyclerView.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, true));
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        MobileAds.initialize(this, getString(R.string.app_id));
        List<String> testDevices = new ArrayList<>();
        testDevices.add(AdRequest.DEVICE_ID_EMULATOR);

                String android_id = Settings.Secure.getString(getContentResolver(),
                Settings.Secure.ANDROID_ID);
        List<String> testDevices1 = new ArrayList<>();
        testDevices1.add(android_id);
        RequestConfiguration requestConfiguration
                = new RequestConfiguration.Builder()
                .setTestDeviceIds(testDevices)
                .setTestDeviceIds(testDevices1)
                .build();
        MobileAds.setRequestConfiguration(requestConfiguration);
        adView.loadAd(new AdRequest.Builder().build());



//        String android_id = Settings.Secure.getString(getContentResolver(),
//                Settings.Secure.ANDROID_ID);
//        AdRequest request = new AdRequest.Builder()
//                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
//                .addTestDevice(android_id)
//                .build();
//        adView.loadAd(request);

        edt_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length()>0) {
                    if (!s.equals("")) {
                        if (s != null) {
                            keywords=s.toString();
                            getHashTag(categoryId,subcategoryId, keywords);
                        }
                    }else {
                        keywords="";
                        getHashTag(categoryId,subcategoryId,keywords);
                    }
                }else {
                    keywords="";
                    getHashTag(categoryId,subcategoryId,keywords);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        adView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
            }

            @Override
            public void onAdClosed() {
                super.onAdClosed();
                //Toast.makeText(getApplicationContext(), "Ad is closed!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                Log.e("onAdFailedToLoad",errorCode+"");
                //Toast.makeText(getApplicationContext(), "Ad failed to load! error code: " + errorCode, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdLeftApplication() {
                //Toast.makeText(getApplicationContext(), "Ad left application!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdOpened() {
                super.onAdOpened();
            }
        });
    }

    private void Prepair() {
        tagModels = new ArrayList<>();
        layoutInflater = getLayoutInflater();
        List<String> tagArray = Arrays.asList(getResources().getStringArray(R.array.taglist));
        for (String tag : tagArray) {
            TagModel model = new TagModel();
            model.setTagString(tag.toString());
            model.setEnable("0");
            tagModels.add(model);
        }
        for (String tag : tagArray) {
            TagModel model = new TagModel();
            model.setTagString(tag.toString());
            model.setEnable("0");
            tagModels.add(model);
        }
        for (int i = 0; i < tagModels.size(); i++) {
            //View tagView = layoutInflater.inflate(R.layout.tags_raw, null, false);
            View tagView = layoutInflater.inflate(R.layout.test_tags_raw, null, false);
            final LinearLayout child_view = (LinearLayout) tagView.findViewById(R.id.child_view);
            final TextView tagTextView = (TextView) tagView.findViewById(R.id.tags);
            if (tagModels.get(i).isEnable().equals("1")) {
                child_view.setBackgroundResource(R.drawable.tagsback_select);
                tagTextView.setTextColor(getResources().getColor(R.color.white));
            } else {
                child_view.setBackgroundResource(R.drawable.tagsback);
                tagTextView.setTextColor(getResources().getColor(R.color.blue_light));
            }
            tagTextView.setText(tagModels.get(i).getTagString() + "-" + taglayout.getChildCount());
            final int finalI = i;
            child_view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (tagModels.get(finalI).isEnable().equals("1")) {
                        child_view.setBackgroundResource(R.drawable.tagsback);
                        tagTextView.setTextColor(getResources().getColor(R.color.blue_light));
                        tagModels.get(finalI).setEnable("0");
                    } else {
                        child_view.setBackgroundResource(R.drawable.tagsback_select);
                        tagTextView.setTextColor(getResources().getColor(R.color.white));
                        tagModels.get(finalI).setEnable("1");
                    }
                    selectCount();
                }
            });
            taglayout.addView(tagView);

        }
        Log.e("taglayout", taglayout.getChildCount() + "");
    }

    private void selectCount() {
        count = 0;
        for (SubCategoryList model : taglist) {
            if (model.isEnable()) {
                count++;
            }
        }
        copy_count.setText("" + count);
        copyTags();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back:
                finish();
                break;
            case R.id.check_layout:
                if (isSelectAll) {
                    isSelectAll = false;
                } else {
                    isSelectAll = true;
                }
                selectAllTags();
                break;
            case R.id.insta_layout:
                if (count > 0) {
                    callInstagram();
                } else {
                    Utility.showToast(mContext, mContext.getResources().getString(R.string.select_tag));
                }
                break;
            case R.id.copy_layout:
                if (count > 0) {
                    copyTags();
                    Utility.showToast(mContext, mContext.getResources().getString(R.string.copy_url));
                } else {
                    Utility.showToast(mContext, mContext.getResources().getString(R.string.select_tag));
                }

                break;
            case R.id.send_layout:

                if (count > 0) {
                    showAdsView();
                    shareIntent();
                } else {
                    Utility.showToast(mContext, mContext.getResources().getString(R.string.select_tag));
                }
                break;


        }
    }

    private void selectAllTags() {
        if (isSelectAll) {
            for (int i = 0; i < taglist.size(); i++) {
                taglist.get(i).setEnable(true);
            }
        } else {
            for (int i = 0; i < taglist.size(); i++) {
                taglist.get(i).setEnable(false);
            }
        }
        setTagsView();
    }

    private void copyTags() {
        sb = new StringBuilder();
        boolean foundOne = false;
        for (int i = 0; i < taglist.size(); ++i) {
            if (taglist.get(i).isEnable()) {
                if (foundOne) {
                    sb.append(", ");
                }
                foundOne = true;
                sb.append(taglist.get(i).getTag_name());
            }
        }

        ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("Copied Text", sb.toString());
        clipboard.setPrimaryClip(clip);
        //Utility.showToast(mContext, mContext.getResources().getString(R.string.copy_url));
    }

    private void shareIntent() {
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, "" + sb.toString());
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "");
        startActivity(Intent.createChooser(sharingIntent, "Share link!"));
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    private void callInstagram() {
        String apppackage = "com.instagram.android";
        Context cx = this;
        try {
            Intent i = cx.getPackageManager().getLaunchIntentForPackage(apppackage);
            cx.startActivity(i);
        } catch (Exception e) {
            Utility.showToast(mContext, mContext.getResources().getString(R.string.intstagram_notfound));
        }



//        Intent shareIntent = new Intent(Intent.ACTION_SEND);
//        shareIntent.setType("text/plain");
//        shareIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        shareIntent.putExtra(Intent.EXTRA_TEXT,sb.toString());
//        shareIntent.setPackage("com.instagram.android");
//       startActivity(shareIntent);

    }

    @Override
    public void showDialog() {
        if (progressDialog == null) {
            progressDialog = Utility.createProgressDialog(mContext);
            progressDialog.show();
        } else {
            progressDialog.show();
        }
    }

    @Override
    public void hideDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    @Override
    public void onError(String message) {
        Utility.showToast(mContext, message);
        removeAllTags();
    }

    private void removeAllTags() {
        if (taglist != null) {
            taglist.clear();
        }
        if (taglayout.getChildCount() > 0) {
            taglayout.removeAllViews();
        }
    }

    @Override
    public void getSuccessSubCategory(SubCategoryContainer body) {
        if (body.getData().size() > 0) {
            if (taglist != null) {
                taglist.clear();
            }
            //if (taglayout.getChildCount()>0){taglayout.removeAllViews();}
            taglist.addAll(body.getData());
            setTagsView();
        }
    }

    private void setTagsView() {
        layoutInflater = getLayoutInflater();
        if (taglayout.getChildCount() > 0) {
            taglayout.removeAllViews();
        }
        for (int i = 0; i < taglist.size(); i++) {
            View tagView = layoutInflater.inflate(R.layout.tags_raw, null, false);
            final LinearLayout child_view = (LinearLayout) tagView.findViewById(R.id.child_view);
            final TextView tagTextView = (TextView) tagView.findViewById(R.id.tags);
            if (taglist.get(i).isEnable()) {
                child_view.setBackgroundResource(R.drawable.tagsback_select);
                tagTextView.setTextColor(getResources().getColor(R.color.white));
            } else {
                child_view.setBackgroundResource(R.drawable.tagsback);
                tagTextView.setTextColor(getResources().getColor(R.color.blue_light));
            }
            tagTextView.setText(taglist.get(i).getTag_name());
            final int finalI = i;
            child_view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (taglist.get(finalI).isEnable()) {
                        child_view.setBackgroundResource(R.drawable.tagsback);
                        tagTextView.setTextColor(getResources().getColor(R.color.blue_light));
                        taglist.get(finalI).setEnable(false);
                    } else {
                        child_view.setBackgroundResource(R.drawable.tagsback_select);
                        tagTextView.setTextColor(getResources().getColor(R.color.white));
                        taglist.get(finalI).setEnable(true);
                    }
                    selectCount();
                }
            });
            taglayout.addView(tagView);
            selectCount();
        }
    }

    @Override
    public void clickTag(SubCategory data, int pos) {
        recyclerView.scrollToPosition(pos);

        categoryId=data.getCategoryId();
        subcategoryId=data.getId();
        keywords="";
        clearSearch();
        getHashTag(categoryId, subcategoryId, keywords);
    }

    private void clearSearch() {
        edt_search.setText("");
        Utility.hideKeyboard(mainview,mContext);
    }
}
