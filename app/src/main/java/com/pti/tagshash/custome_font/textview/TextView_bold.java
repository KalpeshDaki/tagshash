package com.pti.tagshash.custome_font.textview;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;


public class TextView_bold extends TextView {

    public TextView_bold(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public TextView_bold(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public TextView_bold(Context context) {
        super(context);
        init();
    }

    private void init() {
        if (!isInEditMode()) {
            Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/arial_bold.ttf");
            setTypeface(tf);
        }
    }

}