package com.pti.tagshash.module.category;

import com.pti.tagshash.model.category.CategoryContainer;

public interface CategoryView {

    void showDialog();
    void hideDialog();
    void onError(String message);
    void getSuccessCategory(CategoryContainer body);
}
