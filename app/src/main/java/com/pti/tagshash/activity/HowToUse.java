package com.pti.tagshash.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.TextView;

import com.pti.tagshash.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HowToUse extends AppCompatActivity implements View.OnClickListener {
    Context mContext;
    @BindView(R.id.letsgo)
    TextView letsgo;
   @BindView(R.id.disc_title)
    TextView disc_title;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_how_to_use);
        ButterKnife.bind(this);
        mContext=HowToUse.this;
        setUpView();
    }

    private void setUpView() {
        String disc="<font color='#000000'>Welcome\nto </font> <font color='#0068D8'> Hashtags for \nInstagram</font>";
        disc_title.setText((Html.fromHtml(disc.replace("\n","<br />"))));


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.letsgo:
                startActivity(new Intent(mContext,Dashboard.class));
                finish();
                break;
        }
    }
}
