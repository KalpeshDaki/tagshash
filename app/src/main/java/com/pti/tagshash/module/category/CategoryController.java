package com.pti.tagshash.module.category;

import android.content.Context;

import com.pti.tagshash.model.category.CategoryContainer;
import com.pti.tagshash.network.ApiClient;
import com.pti.tagshash.network.ApiInterface;
import com.pti.tagshash.utils.Constant;
import com.pti.tagshash.utils.Log;
import com.pti.tagshash.utils.Utility;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CategoryController {
    private Presenter presenter;
    Context mContext;
    ApiInterface apiInterface;

    public CategoryController(Context context, Presenter pr) {
        presenter = pr;
        mContext = context;
        apiInterface = ApiClient.createService(ApiInterface.class);
    }

    public void getCategory() {
        Call<CategoryContainer> call = apiInterface.getCategory();
        call.enqueue(new Callback<CategoryContainer>() {
            @Override
            public void onResponse(Call<CategoryContainer> call, Response<CategoryContainer> response) {
                if (response.code() == 200) {
                    if (response.body().getStatus().equals(Constant.code201)) {
                        presenter.getSuccessCategory(response.body());
                    } else {
                        presenter.onError(response.body().getMessage());
                    }
                }else {
                    presenter.onError(Utility.getError(response.errorBody()));
                }
            }

            @Override
            public void onFailure(Call<CategoryContainer> call, Throwable t) {
                presenter.onError(t.toString());
            }
        });
    }

    interface Presenter {
        void onError(String toString);

        void getSuccessCategory(CategoryContainer body);

    }
}
