package com.pti.tagshash.module.subcategory;

import com.pti.tagshash.model.sub_category.SubCategoryContainer;

public interface SubCategoryView {

    void showDialog();
    void hideDialog();
    void onError(String message);
    void getSuccessSubCategory(SubCategoryContainer body);
}
