package com.pti.tagshash.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.ads.MobileAds;
import com.pti.tagshash.R;
import com.pti.tagshash.preference.AppPreference;

public class Splash extends AppCompatActivity {
    Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        mContext = Splash.this;

        Thread thread = new Thread() {
            public void run() {
                try {
                    sleep(2000);
                    if (!AppPreference.getFirstTimeLaunch(mContext)) {
                        Intent intent = new Intent(mContext, HowToUse.class);
                        startActivity(intent);
                        finish();
                    } else {
                        Intent intent = new Intent(mContext, Dashboard.class);
                        startActivity(intent);
                        finish();
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        thread.start();
    }
}
