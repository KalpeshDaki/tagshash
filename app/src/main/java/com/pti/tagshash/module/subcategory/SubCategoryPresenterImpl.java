package com.pti.tagshash.module.subcategory;

import android.content.Context;

import com.pti.tagshash.model.category.CategoryContainer;
import com.pti.tagshash.model.sub_category.SubCategoryContainer;
import com.pti.tagshash.utils.Constant;

public class SubCategoryPresenterImpl implements SubCategoryController.Presenter{
    private Context context;
    private SubCategoryView mainView;
    private SubCategoryController controller;


    public SubCategoryPresenterImpl(Context context, SubCategoryView mainActivity) {
        this.context=context;
        mainView=mainActivity;
        controller = new SubCategoryController(context,this);
    }

    public void getHashTag(String cat_id,String sub_cat_id,String keyword) {
        mainView.showDialog();
        controller.getHashTag(cat_id,sub_cat_id,keyword);
    }

    @Override
    public void onError(String msg) {
        mainView.hideDialog();
        mainView.onError(msg);
    }

    @Override
    public void getSuccessSubCategory(SubCategoryContainer body) {
        mainView.hideDialog();
        mainView.getSuccessSubCategory(body);
    }

}
