package com.pti.tagshash.activity;

import android.app.Application;
import android.os.StrictMode;

import androidx.multidex.MultiDex;

import com.google.android.gms.ads.MobileAds;
import com.pti.tagshash.R;

public class MyApplication extends Application {


    public static final String LOGTAG = MyApplication.class
            .getSimpleName();
    private static MyApplication mInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        MultiDex.install(this);
        mInstance = this;
        MobileAds.initialize(this, getString(R.string.app_id));
        StrictMode.ThreadPolicy policy =
                new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        /*Thread t = new Thread(new Runnable() {
            public void run() {

                Thread t = new Thread(new Runnable() {
                    public void run() {
                        String GCMRegId = "";//PreferenceUtils.getDeviceGcmId(getAppContext());
                        Log.e("GCMRegId", "GCMRegId --> " + GCMRegId);
                        while (TextUtils.isEmpty(GCMRegId)) {
                            try {
                                GCMRegId = FirebaseInstanceId.getInstance().getToken();
                            } catch (Exception e) {
                                if (BuildConfig.DEBUG)
                                    Log.e(LOGTAG, e.toString());
                            }
                            //PreferenceUtils.setDeviceGcmId(getAppContext(), GCMRegId);
                            if (BuildConfig.DEBUG)
                                Log.d(LOGTAG, "GCMRegId --> " + GCMRegId);
                        }
                    }
                });
                t.start();
            }
        });
        t.start();*/
    }

    public static synchronized MyApplication getInstance() {
        return mInstance;
    }
}
