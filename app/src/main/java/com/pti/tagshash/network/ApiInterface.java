package com.pti.tagshash.network;


import com.google.gson.JsonObject;
import com.pti.tagshash.model.category.CategoryContainer;
import com.pti.tagshash.model.sub_category.SubCategoryContainer;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface ApiInterface {

    /*@POST("user/login")
    Call<LoginContainer> getlogin(@Body RequestBody body);

    @POST("report/report_date")
    Call<DateContainer> getReportDate(@Body RequestBody body);

    @POST("report/report_list")
    Call<ReportContainer> getSelectDateReport(@Body RequestBody body);

    @POST("report/add_report")
    Call<AddReportContainer> getAddReport(@Body MultipartBody body);*/


    @GET("category-list")
    Call<CategoryContainer> getCategory();

    @POST("category-wise-hashtag")
    Call<SubCategoryContainer> getCategory_hashtag(@Body RequestBody body);
}
