package com.pti.tagshash.activity;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.pti.tagshash.R;
import com.pti.tagshash.adapter.ViewAdapter;
import com.pti.tagshash.model.TagModel;
import com.pti.tagshash.model.ViewItemDTO;
import com.pti.tagshash.utils.CustomeLayoutManager;
import com.pti.tagshash.utils.CustomeTagLayout;
import com.pti.tagshash.utils.Log;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FlexActivity extends AppCompatActivity {
    private String textArr[] = {"dev2qa.com", "is", "a very good", "android example website", "there are", "a lot of", "android, java examples"};


    List<TagModel> tagModels;
    LayoutInflater layoutInflater;
    CustomeTagLayout flexlayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flex);

        RecyclerView recyclerView = (RecyclerView)findViewById(R.id.flex_box_recycler_view);
         flexlayout = (CustomeTagLayout)findViewById(R.id.flexlayout);
        CustomeLayoutManager flexboxLayoutManager = new CustomeLayoutManager(getApplicationContext());
        flexboxLayoutManager.setFlexDirection(2);
        flexboxLayoutManager.setJustifyContent(4);
        recyclerView.setLayoutManager(flexboxLayoutManager);
        ViewAdapter viewAdapter = new ViewAdapter(this.initViewItemDtoList());
        recyclerView.setAdapter(viewAdapter);
        prepair();
    }
    private List<ViewItemDTO> initViewItemDtoList()
    {
        List<ViewItemDTO> ret = new ArrayList<ViewItemDTO>();

        for(int i=0;i < this.textArr.length; i++)
        {
            ViewItemDTO itemDto = new ViewItemDTO();
            itemDto.setText(this.textArr[i]);

            ret.add(itemDto);
        }
        return ret;
    }



    private void prepair() {
        tagModels = new ArrayList<>();
        layoutInflater = getLayoutInflater();
        List<String> tagArray = Arrays.asList(getResources().getStringArray(R.array.taglist));
        Log.e("Prepair", tagArray.size() + "");
        for (String tag : tagArray) {
            TagModel model = new TagModel();
            model.setTagString(tag.toString());
            model.setEnable("0");
            tagModels.add(model);
        }
        for (String tag : tagArray) {
            TagModel model = new TagModel();
            model.setTagString(tag.toString());
            model.setEnable("0");
            tagModels.add(model);
        }
        for (int i = 0; i < tagModels.size(); i++) {
            View tagView = layoutInflater.inflate(R.layout.test_tags_raw, null, false);
            final LinearLayout child_view = (LinearLayout) tagView.findViewById(R.id.child_view);
            final TextView tagTextView = (TextView) tagView.findViewById(R.id.tags);
            if (tagModels.get(i).isEnable().equals("1")) {
                child_view.setBackgroundResource(R.drawable.tagsback_select);
                tagTextView.setTextColor(getResources().getColor(R.color.white));
            } else {
                child_view.setBackgroundResource(R.drawable.tagsback);
                tagTextView.setTextColor(getResources().getColor(R.color.blue_light));
            }
            tagTextView.setText(tagModels.get(i).getTagString());
            final int finalI = i;
            child_view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (tagModels.get(finalI).isEnable().equals("1")) {
                        child_view.setBackgroundResource(R.drawable.tagsback);
                        tagTextView.setTextColor(getResources().getColor(R.color.blue_light));
                        tagModels.get(finalI).setEnable("0");
                    } else {
                        child_view.setBackgroundResource(R.drawable.tagsback_select);
                        tagTextView.setTextColor(getResources().getColor(R.color.white));
                        tagModels.get(finalI).setEnable("1");
                    }
                }
            });
            flexlayout.addView(tagView);

        }
        Log.e("taglayout", flexlayout.getChildCount() + "");
    }

}
