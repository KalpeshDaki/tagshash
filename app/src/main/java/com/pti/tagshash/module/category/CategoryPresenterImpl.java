package com.pti.tagshash.module.category;

import android.content.Context;

import com.pti.tagshash.model.category.CategoryContainer;
import com.pti.tagshash.utils.Constant;

public class CategoryPresenterImpl implements CategoryController.Presenter{
    private Context context;
    private CategoryView mainView;
    private CategoryController controller;


    public CategoryPresenterImpl(Context context, CategoryView mainActivity) {
        this.context=context;
        mainView=mainActivity;
        controller = new CategoryController(context,this);
    }

    public void getCategory() {
        mainView.showDialog();
        controller.getCategory();
    }

    @Override
    public void onError(String msg) {
        mainView.hideDialog();
        mainView.onError(msg);
    }

    @Override
    public void getSuccessCategory(CategoryContainer body) {
        mainView.hideDialog();
        Constant.Category_ImagePath=body.getCatImagePath();
        mainView.getSuccessCategory(body);
    }

}
