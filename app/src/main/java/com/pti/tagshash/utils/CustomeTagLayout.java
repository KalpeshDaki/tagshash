package com.pti.tagshash.utils;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.SparseIntArray;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.IntDef;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import androidx.core.view.MarginLayoutParamsCompat;
import androidx.core.view.ViewCompat;

import com.pti.tagshash.R;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static androidx.recyclerview.widget.RecyclerView.NO_POSITION;


public class CustomeTagLayout extends ViewGroup implements TagContainer {
    private int mFlexDirection;
    private int mFlexWrap;
    private int mJustifyContent;
    private int mAlignItems;
    private int mAlignContent;

    @IntDef(flag = true,
            value = {
                    SHOW_DIVIDER_NONE,
                    SHOW_DIVIDER_BEGINNING,
                    SHOW_DIVIDER_MIDDLE,
                    SHOW_DIVIDER_END
            })
    @Retention(RetentionPolicy.SOURCE)
    @SuppressWarnings("WeakerAccess")
    public @interface DividerMode {
    }

    public static final int SHOW_DIVIDER_NONE = 0;
    public static final int SHOW_DIVIDER_BEGINNING = 1;
    public static final int SHOW_DIVIDER_MIDDLE = 1 << 1;
    public static final int SHOW_DIVIDER_END = 1 << 2;
    @Nullable
    private Drawable mDividerDrawableHorizontal;
    @Nullable
    private Drawable mDividerDrawableVertical;
    private int mShowDividerHorizontal;
    private int mShowDividerVertical;
    private int mDividerHorizontalHeight;
    private int mDividerVerticalWidth;
    private int[] mReorderedIndices;
    private SparseIntArray mOrderCache;

    private FlexboxHelper mFlexboxHelper = new FlexboxHelper(this);
    private List<FlexLine> mFlexLines = new ArrayList<>();
    private FlexboxHelper.FlexLinesResult mFlexLinesResult = new FlexboxHelper.FlexLinesResult();

    public CustomeTagLayout(Context context) {
        this(context, null);
    }

    public CustomeTagLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CustomeTagLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        TypedArray a = context.obtainStyledAttributes(
                attrs, R.styleable.FlexboxLayout, defStyleAttr, 0);
        mFlexDirection = a
                .getInt(R.styleable.FlexboxLayout_flexDirection, FlexDirection.ROW);
        mFlexWrap = a.getInt(R.styleable.FlexboxLayout_flexWrap, FlexWrap.NOWRAP);
        mJustifyContent = a
                .getInt(R.styleable.FlexboxLayout_justifyContent, JustifyContent.FLEX_START);
        mAlignItems = a.getInt(R.styleable.FlexboxLayout_alignItems, AlignItems.STRETCH);
        mAlignContent = a.getInt(R.styleable.FlexboxLayout_alignContent, AlignContent.STRETCH);
        Drawable drawable = a.getDrawable(R.styleable.FlexboxLayout_dividerDrawable);
        if (drawable != null) {
            setDividerDrawableHorizontal(drawable);
            setDividerDrawableVertical(drawable);
        }
        Drawable drawableHorizontal = a
                .getDrawable(R.styleable.FlexboxLayout_dividerDrawableHorizontal);
        if (drawableHorizontal != null) {
            setDividerDrawableHorizontal(drawableHorizontal);
        }
        Drawable drawableVertical = a
                .getDrawable(R.styleable.FlexboxLayout_dividerDrawableVertical);
        if (drawableVertical != null) {
            setDividerDrawableVertical(drawableVertical);
        }
        int dividerMode = a.getInt(R.styleable.FlexboxLayout_showDivider, SHOW_DIVIDER_NONE);
        if (dividerMode != SHOW_DIVIDER_NONE) {
            mShowDividerVertical = dividerMode;
            mShowDividerHorizontal = dividerMode;
        }
        int dividerModeVertical = a
                .getInt(R.styleable.FlexboxLayout_showDividerVertical, SHOW_DIVIDER_NONE);
        if (dividerModeVertical != SHOW_DIVIDER_NONE) {
            mShowDividerVertical = dividerModeVertical;
        }
        int dividerModeHorizontal = a
                .getInt(R.styleable.FlexboxLayout_showDividerHorizontal, SHOW_DIVIDER_NONE);
        if (dividerModeHorizontal != SHOW_DIVIDER_NONE) {
            mShowDividerHorizontal = dividerModeHorizontal;
        }
        a.recycle();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        if (mOrderCache == null) {
            mOrderCache = new SparseIntArray(getChildCount());
        }
        if (mFlexboxHelper.isOrderChangedFromLastMeasurement(mOrderCache)) {
            mReorderedIndices = mFlexboxHelper.createReorderedIndices(mOrderCache);
        }
        switch (mFlexDirection) {
            case FlexDirection.ROW: // Intentional fall through
            case FlexDirection.ROW_REVERSE:
                measureHorizontal(widthMeasureSpec, heightMeasureSpec);
                break;
            case FlexDirection.COLUMN: // Intentional fall through
            case FlexDirection.COLUMN_REVERSE:
                measureVertical(widthMeasureSpec, heightMeasureSpec);
                break;
            default:
                throw new IllegalStateException(
                        "Invalid value for the flex direction is set: " + mFlexDirection);
        }
    }

    @Override
    public int getFlexItemCount() {
        return getChildCount();
    }

    @Override
    public View getFlexItemAt(int index) {
        return getChildAt(index);
    }

    public View getReorderedChildAt(int index) {
        if (index < 0 || index >= mReorderedIndices.length) {
            return null;
        }
        return getChildAt(mReorderedIndices[index]);
    }

    @Override
    public View getReorderedFlexItemAt(int index) {
        return getReorderedChildAt(index);
    }

    @Override
    public void addView(View child, int index, ViewGroup.LayoutParams params) {
        if (mOrderCache == null) {
            mOrderCache = new SparseIntArray(getChildCount());
        }
        mReorderedIndices = mFlexboxHelper
                .createReorderedIndices(child, index, params, mOrderCache);
        super.addView(child, index, params);
    }

    private void measureHorizontal(int widthMeasureSpec, int heightMeasureSpec) {
        mFlexLines.clear();

        mFlexLinesResult.reset();
        mFlexboxHelper
                .calculateHorizontalFlexLines(mFlexLinesResult, widthMeasureSpec,
                        heightMeasureSpec);
        mFlexLines = mFlexLinesResult.mFlexLines;

        mFlexboxHelper.determineMainSize(widthMeasureSpec, heightMeasureSpec);
        if (mAlignItems == AlignItems.BASELINE) {
            int viewIndex = 0;
            for (FlexLine flexLine : mFlexLines) {
                int largestHeightInLine = Integer.MIN_VALUE;
                for (int i = viewIndex; i < viewIndex + flexLine.mItemCount; i++) {
                    View child = getReorderedChildAt(i);
                    LayoutParams lp = (LayoutParams) child.getLayoutParams();
                    if (mFlexWrap != FlexWrap.WRAP_REVERSE) {
                        int marginTop = flexLine.mMaxBaseline - child.getBaseline();
                        marginTop = Math.max(marginTop, lp.topMargin);
                        largestHeightInLine = Math.max(largestHeightInLine,
                                child.getHeight() + marginTop + lp.bottomMargin);
                    } else {
                        int marginBottom = flexLine.mMaxBaseline - child.getMeasuredHeight() +
                                child.getBaseline();
                        marginBottom = Math.max(marginBottom, lp.bottomMargin);
                        largestHeightInLine = Math.max(largestHeightInLine,
                                child.getHeight() + lp.topMargin + marginBottom);
                    }
                }
                flexLine.mCrossSize = largestHeightInLine;
                viewIndex += flexLine.mItemCount;
            }
        }

        mFlexboxHelper.determineCrossSize(widthMeasureSpec, heightMeasureSpec,
                getPaddingTop() + getPaddingBottom());
        mFlexboxHelper.stretchViews();
        setMeasuredDimensionForFlex(mFlexDirection, widthMeasureSpec, heightMeasureSpec,
                mFlexLinesResult.mChildState);
    }

    private void measureVertical(int widthMeasureSpec, int heightMeasureSpec) {
        mFlexLines.clear();
        mFlexLinesResult.reset();
        mFlexboxHelper.calculateVerticalFlexLines(mFlexLinesResult, widthMeasureSpec,
                heightMeasureSpec);
        mFlexLines = mFlexLinesResult.mFlexLines;

        mFlexboxHelper.determineMainSize(widthMeasureSpec, heightMeasureSpec);
        mFlexboxHelper.determineCrossSize(widthMeasureSpec, heightMeasureSpec,
                getPaddingLeft() + getPaddingRight());
        mFlexboxHelper.stretchViews();
        setMeasuredDimensionForFlex(mFlexDirection, widthMeasureSpec, heightMeasureSpec,
                mFlexLinesResult.mChildState);
    }

    private void setMeasuredDimensionForFlex(@FlexDirection int flexDirection, int widthMeasureSpec,
                                             int heightMeasureSpec, int childState) {
        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        int heightSize = MeasureSpec.getSize(heightMeasureSpec);
        int calculatedMaxHeight;
        int calculatedMaxWidth;
        switch (flexDirection) {
            case FlexDirection.ROW: // Intentional fall through
            case FlexDirection.ROW_REVERSE:
                calculatedMaxHeight = getSumOfCrossSize() + getPaddingTop()
                        + getPaddingBottom();
                calculatedMaxWidth = getLargestMainSize();
                break;
            case FlexDirection.COLUMN: // Intentional fall through
            case FlexDirection.COLUMN_REVERSE:
                calculatedMaxHeight = getLargestMainSize();
                calculatedMaxWidth = getSumOfCrossSize() + getPaddingLeft() + getPaddingRight();
                break;
            default:
                throw new IllegalArgumentException("Invalid flex direction: " + flexDirection);
        }

        int widthSizeAndState;
        switch (widthMode) {
            case MeasureSpec.EXACTLY:
                if (widthSize < calculatedMaxWidth) {
                    childState = ViewCompat
                            .combineMeasuredStates(childState, ViewCompat.MEASURED_STATE_TOO_SMALL);
                }
                widthSizeAndState = ViewCompat.resolveSizeAndState(widthSize, widthMeasureSpec,
                        childState);
                break;
            case MeasureSpec.AT_MOST: {
                if (widthSize < calculatedMaxWidth) {
                    childState = ViewCompat
                            .combineMeasuredStates(childState, ViewCompat.MEASURED_STATE_TOO_SMALL);
                } else {
                    widthSize = calculatedMaxWidth;
                }
                widthSizeAndState = ViewCompat.resolveSizeAndState(widthSize, widthMeasureSpec,
                        childState);
                break;
            }
            case MeasureSpec.UNSPECIFIED: {
                widthSizeAndState = ViewCompat
                        .resolveSizeAndState(calculatedMaxWidth, widthMeasureSpec, childState);
                break;
            }
            default:
                throw new IllegalStateException("Unknown width mode is set: " + widthMode);
        }
        int heightSizeAndState;
        switch (heightMode) {
            case MeasureSpec.EXACTLY:
                if (heightSize < calculatedMaxHeight) {
                    childState = ViewCompat.combineMeasuredStates(childState,
                            ViewCompat.MEASURED_STATE_TOO_SMALL
                                    >> ViewCompat.MEASURED_HEIGHT_STATE_SHIFT);
                }
                heightSizeAndState = ViewCompat.resolveSizeAndState(heightSize, heightMeasureSpec,
                        childState);
                break;
            case MeasureSpec.AT_MOST: {
                if (heightSize < calculatedMaxHeight) {
                    childState = ViewCompat.combineMeasuredStates(childState,
                            ViewCompat.MEASURED_STATE_TOO_SMALL
                                    >> ViewCompat.MEASURED_HEIGHT_STATE_SHIFT);
                } else {
                    heightSize = calculatedMaxHeight;
                }
                heightSizeAndState = ViewCompat.resolveSizeAndState(heightSize, heightMeasureSpec,
                        childState);
                break;
            }
            case MeasureSpec.UNSPECIFIED: {
                heightSizeAndState = ViewCompat.resolveSizeAndState(calculatedMaxHeight,
                        heightMeasureSpec, childState);
                break;
            }
            default:
                throw new IllegalStateException("Unknown height mode is set: " + heightMode);
        }
        setMeasuredDimension(widthSizeAndState, heightSizeAndState);
    }

    @Override
    public int getLargestMainSize() {
        int largestSize = Integer.MIN_VALUE;
        for (FlexLine flexLine : mFlexLines) {
            largestSize = Math.max(largestSize, flexLine.mMainSize);
        }
        return largestSize;
    }

    @Override
    public int getSumOfCrossSize() {
        int sum = 0;
        for (int i = 0, size = mFlexLines.size(); i < size; i++) {
            FlexLine flexLine = mFlexLines.get(i);
            if (hasDividerBeforeFlexLine(i)) {
                if (isMainAxisDirectionHorizontal()) {
                    sum += mDividerHorizontalHeight;
                } else {
                    sum += mDividerVerticalWidth;
                }
            }
            if (hasEndDividerAfterFlexLine(i)) {
                if (isMainAxisDirectionHorizontal()) {
                    sum += mDividerHorizontalHeight;
                } else {
                    sum += mDividerVerticalWidth;
                }
            }
            sum += flexLine.mCrossSize;
        }
        return sum;
    }

    @Override
    public boolean isMainAxisDirectionHorizontal() {
        return mFlexDirection == FlexDirection.ROW || mFlexDirection == FlexDirection.ROW_REVERSE;
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        int layoutDirection = ViewCompat.getLayoutDirection(this);
        boolean isRtl;
        switch (mFlexDirection) {
            case FlexDirection.ROW:
                isRtl = layoutDirection == ViewCompat.LAYOUT_DIRECTION_RTL;
                layoutHorizontal(isRtl, left, top, right, bottom);
                break;
            case FlexDirection.ROW_REVERSE:
                isRtl = layoutDirection != ViewCompat.LAYOUT_DIRECTION_RTL;
                layoutHorizontal(isRtl, left, top, right, bottom);
                break;
            case FlexDirection.COLUMN:
                isRtl = layoutDirection == ViewCompat.LAYOUT_DIRECTION_RTL;
                if (mFlexWrap == FlexWrap.WRAP_REVERSE) {
                    isRtl = !isRtl;
                }
                layoutVertical(isRtl, false, left, top, right, bottom);
                break;
            case FlexDirection.COLUMN_REVERSE:
                isRtl = layoutDirection == ViewCompat.LAYOUT_DIRECTION_RTL;
                if (mFlexWrap == FlexWrap.WRAP_REVERSE) {
                    isRtl = !isRtl;
                }
                layoutVertical(isRtl, true, left, top, right, bottom);
                break;
            default:
                throw new IllegalStateException("Invalid flex direction is set: " + mFlexDirection);
        }
    }

    private void layoutHorizontal(boolean isRtl, int left, int top, int right, int bottom) {
        int paddingLeft = getPaddingLeft();
        int paddingRight = getPaddingRight();
        float childLeft;

        int height = bottom - top;
        int width = right - left;
        int childBottom = height - getPaddingBottom();
        int childTop = getPaddingTop();
        float childRight;
        for (int i = 0, size = mFlexLines.size(); i < size; i++) {
            FlexLine flexLine = mFlexLines.get(i);
            if (hasDividerBeforeFlexLine(i)) {
                childBottom -= mDividerHorizontalHeight;
                childTop += mDividerHorizontalHeight;
            }
            float spaceBetweenItem = 0f;
            switch (mJustifyContent) {
                case JustifyContent.FLEX_START:
                    childLeft = paddingLeft;
                    childRight = width - paddingRight;
                    break;
                case JustifyContent.FLEX_END:
                    childLeft = width - flexLine.mMainSize + paddingRight;
                    childRight = flexLine.mMainSize - paddingLeft;
                    break;
                case JustifyContent.CENTER:
                    childLeft = paddingLeft + (width - flexLine.mMainSize) / 2f;
                    childRight = width - paddingRight - (width - flexLine.mMainSize) / 2f;
                    break;
                case JustifyContent.SPACE_AROUND:
                    int visibleCount = flexLine.getItemCountNotGone();
                    if (visibleCount != 0) {
                        spaceBetweenItem = (width - flexLine.mMainSize)
                                / (float) visibleCount;
                    }
                    childLeft = paddingLeft + spaceBetweenItem / 2f;
                    childRight = width - paddingRight - spaceBetweenItem / 2f;
                    break;
                case JustifyContent.SPACE_BETWEEN:
                    childLeft = paddingLeft;
                    int visibleItem = flexLine.getItemCountNotGone();
                    float denominator = visibleItem != 1 ? visibleItem - 1 : 1f;
                    spaceBetweenItem = (width - flexLine.mMainSize) / denominator;
                    childRight = width - paddingRight;
                    break;
                default:
                    throw new IllegalStateException(
                            "Invalid justifyContent is set: " + mJustifyContent);
            }
            spaceBetweenItem = Math.max(spaceBetweenItem, 0);

            for (int j = 0; j < flexLine.mItemCount; j++) {
                int index = flexLine.mFirstIndex + j;
                View child = getReorderedChildAt(index);
                if (child == null || child.getVisibility() == View.GONE) {
                    continue;
                }
                LayoutParams lp = ((LayoutParams) child.getLayoutParams());
                childLeft += lp.leftMargin;
                childRight -= lp.rightMargin;
                int beforeDividerLength = 0;
                int endDividerLength = 0;
                if (hasDividerBeforeChildAtAlongMainAxis(index, j)) {
                    beforeDividerLength = mDividerVerticalWidth;
                    childLeft += beforeDividerLength;
                    childRight -= beforeDividerLength;
                }
                if (j == flexLine.mItemCount - 1 && (mShowDividerVertical & SHOW_DIVIDER_END) > 0) {
                    endDividerLength = mDividerVerticalWidth;
                }

                if (mFlexWrap == FlexWrap.WRAP_REVERSE) {
                    if (isRtl) {
                        mFlexboxHelper.layoutSingleChildHorizontal(child, flexLine,
                                Math.round(childRight) - child.getMeasuredWidth(),
                                childBottom - child.getMeasuredHeight(), Math.round(childRight),
                                childBottom);
                    } else {
                        mFlexboxHelper.layoutSingleChildHorizontal(child, flexLine,
                                Math.round(childLeft), childBottom - child.getMeasuredHeight(),
                                Math.round(childLeft) + child.getMeasuredWidth(), childBottom);
                    }
                } else {
                    if (isRtl) {
                        mFlexboxHelper.layoutSingleChildHorizontal(child, flexLine,
                                Math.round(childRight) - child.getMeasuredWidth(),
                                childTop, Math.round(childRight),
                                childTop + child.getMeasuredHeight());
                    } else {
                        mFlexboxHelper.layoutSingleChildHorizontal(child, flexLine,
                                Math.round(childLeft), childTop,
                                Math.round(childLeft) + child.getMeasuredWidth(),
                                childTop + child.getMeasuredHeight());
                    }
                }
                childLeft += child.getMeasuredWidth() + spaceBetweenItem + lp.rightMargin;
                childRight -= child.getMeasuredWidth() + spaceBetweenItem + lp.leftMargin;

                if (isRtl) {
                    flexLine.updatePositionFromView(child, /*leftDecoration*/endDividerLength, 0,
                            /*rightDecoration*/ beforeDividerLength, 0);
                } else {
                    flexLine.updatePositionFromView(child, /*leftDecoration*/beforeDividerLength, 0,
                            /*rightDecoration*/ endDividerLength, 0);
                }
            }
            childTop += flexLine.mCrossSize;
            childBottom -= flexLine.mCrossSize;
        }
    }

    private void layoutVertical(boolean isRtl, boolean fromBottomToTop, int left, int top,
                                int right, int bottom) {
        int paddingTop = getPaddingTop();
        int paddingBottom = getPaddingBottom();

        int paddingRight = getPaddingRight();
        int childLeft = getPaddingLeft();

        int width = right - left;
        int height = bottom - top;
        int childRight = width - paddingRight;
        float childTop;

        float childBottom;

        for (int i = 0, size = mFlexLines.size(); i < size; i++) {
            FlexLine flexLine = mFlexLines.get(i);
            if (hasDividerBeforeFlexLine(i)) {
                childLeft += mDividerVerticalWidth;
                childRight -= mDividerVerticalWidth;
            }
            float spaceBetweenItem = 0f;
            switch (mJustifyContent) {
                case JustifyContent.FLEX_START:
                    childTop = paddingTop;
                    childBottom = height - paddingBottom;
                    break;
                case JustifyContent.FLEX_END:
                    childTop = height - flexLine.mMainSize + paddingBottom;
                    childBottom = flexLine.mMainSize - paddingTop;
                    break;
                case JustifyContent.CENTER:
                    childTop = paddingTop + (height - flexLine.mMainSize) / 2f;
                    childBottom = height - paddingBottom - (height - flexLine.mMainSize) / 2f;
                    break;
                case JustifyContent.SPACE_AROUND:
                    int visibleCount = flexLine.getItemCountNotGone();
                    if (visibleCount != 0) {
                        spaceBetweenItem = (height - flexLine.mMainSize)
                                / (float) visibleCount;
                    }
                    childTop = paddingTop + spaceBetweenItem / 2f;
                    childBottom = height - paddingBottom - spaceBetweenItem / 2f;
                    break;
                case JustifyContent.SPACE_BETWEEN:
                    childTop = paddingTop;
                    int visibleItem = flexLine.getItemCountNotGone();
                    float denominator = visibleItem != 1 ? visibleItem - 1 : 1f;
                    spaceBetweenItem = (height - flexLine.mMainSize) / denominator;
                    childBottom = height - paddingBottom;
                    break;
                default:
                    throw new IllegalStateException(
                            "Invalid justifyContent is set: " + mJustifyContent);
            }
            spaceBetweenItem = Math.max(spaceBetweenItem, 0);

            for (int j = 0; j < flexLine.mItemCount; j++) {
                int index = flexLine.mFirstIndex + j;
                View child = getReorderedChildAt(index);
                if (child == null || child.getVisibility() == View.GONE) {
                    continue;
                }
                LayoutParams lp = ((LayoutParams) child.getLayoutParams());
                childTop += lp.topMargin;
                childBottom -= lp.bottomMargin;
                int beforeDividerLength = 0;
                int endDividerLength = 0;
                if (hasDividerBeforeChildAtAlongMainAxis(index, j)) {
                    beforeDividerLength = mDividerHorizontalHeight;
                    childTop += beforeDividerLength;
                    childBottom -= beforeDividerLength;
                }
                if (j == flexLine.mItemCount - 1
                        && (mShowDividerHorizontal & SHOW_DIVIDER_END) > 0) {
                    endDividerLength = mDividerHorizontalHeight;
                }
                if (isRtl) {
                    if (fromBottomToTop) {
                        mFlexboxHelper.layoutSingleChildVertical(child, flexLine, true,
                                childRight - child.getMeasuredWidth(),
                                Math.round(childBottom) - child.getMeasuredHeight(), childRight,
                                Math.round(childBottom));
                    } else {
                        mFlexboxHelper.layoutSingleChildVertical(child, flexLine, true,
                                childRight - child.getMeasuredWidth(), Math.round(childTop),
                                childRight, Math.round(childTop) + child.getMeasuredHeight());
                    }
                } else {
                    if (fromBottomToTop) {
                        mFlexboxHelper.layoutSingleChildVertical(child, flexLine, false,
                                childLeft, Math.round(childBottom) - child.getMeasuredHeight(),
                                childLeft + child.getMeasuredWidth(), Math.round(childBottom));
                    } else {
                        mFlexboxHelper.layoutSingleChildVertical(child, flexLine, false,
                                childLeft, Math.round(childTop),
                                childLeft + child.getMeasuredWidth(),
                                Math.round(childTop) + child.getMeasuredHeight());
                    }
                }
                childTop += child.getMeasuredHeight() + spaceBetweenItem + lp.bottomMargin;
                childBottom -= child.getMeasuredHeight() + spaceBetweenItem + lp.topMargin;

                if (fromBottomToTop) {
                    flexLine.updatePositionFromView(child, 0, /*topDecoration*/endDividerLength, 0,
                            /*bottomDecoration*/ beforeDividerLength);
                } else {
                    flexLine.updatePositionFromView(child, 0, /*topDecoration*/beforeDividerLength,
                            0, /*bottomDecoration*/endDividerLength);
                }
            }
            childLeft += flexLine.mCrossSize;
            childRight -= flexLine.mCrossSize;
        }
    }


    @Override
    protected void onDraw(Canvas canvas) {
        if (mDividerDrawableVertical == null && mDividerDrawableHorizontal == null) {
            return;
        }
        if (mShowDividerHorizontal == SHOW_DIVIDER_NONE
                && mShowDividerVertical == SHOW_DIVIDER_NONE) {
            return;
        }

        int layoutDirection = ViewCompat.getLayoutDirection(this);
        boolean isRtl;
        boolean fromBottomToTop = false;
        switch (mFlexDirection) {
            case FlexDirection.ROW:
                isRtl = layoutDirection == ViewCompat.LAYOUT_DIRECTION_RTL;
                if (mFlexWrap == FlexWrap.WRAP_REVERSE) {
                    fromBottomToTop = true;
                }
                drawDividersHorizontal(canvas, isRtl, fromBottomToTop);
                break;
            case FlexDirection.ROW_REVERSE:
                isRtl = layoutDirection != ViewCompat.LAYOUT_DIRECTION_RTL;
                if (mFlexWrap == FlexWrap.WRAP_REVERSE) {
                    fromBottomToTop = true;
                }
                drawDividersHorizontal(canvas, isRtl, fromBottomToTop);
                break;
            case FlexDirection.COLUMN:
                isRtl = layoutDirection == ViewCompat.LAYOUT_DIRECTION_RTL;
                if (mFlexWrap == FlexWrap.WRAP_REVERSE) {
                    isRtl = !isRtl;
                }
                drawDividersVertical(canvas, isRtl, false);
                break;
            case FlexDirection.COLUMN_REVERSE:
                isRtl = layoutDirection == ViewCompat.LAYOUT_DIRECTION_RTL;
                if (mFlexWrap == FlexWrap.WRAP_REVERSE) {
                    isRtl = !isRtl;
                }
                drawDividersVertical(canvas, isRtl, true);
                break;
        }
    }

    private void drawDividersHorizontal(Canvas canvas, boolean isRtl, boolean fromBottomToTop) {
        int currentViewIndex = 0;
        int paddingLeft = getPaddingLeft();
        int paddingRight = getPaddingRight();
        int horizontalDividerLength = Math.max(0, getWidth() - paddingRight - paddingLeft);
        for (int i = 0, size = mFlexLines.size(); i < size; i++) {
            FlexLine flexLine = mFlexLines.get(i);
            for (int j = 0; j < flexLine.mItemCount; j++) {
                View view = getReorderedChildAt(currentViewIndex);
                if (view == null || view.getVisibility() == View.GONE) {
                    continue;
                }
                LayoutParams lp = (LayoutParams) view.getLayoutParams();
                if (hasDividerBeforeChildAtAlongMainAxis(currentViewIndex, j)) {
                    int dividerLeft;
                    if (isRtl) {
                        dividerLeft = view.getRight() + lp.rightMargin;
                    } else {
                        dividerLeft = view.getLeft() - lp.leftMargin - mDividerVerticalWidth;
                    }

                    drawVerticalDivider(canvas, dividerLeft, flexLine.mTop, flexLine.mCrossSize);
                }
                if (j == flexLine.mItemCount - 1) {
                    if ((mShowDividerVertical & SHOW_DIVIDER_END) > 0) {
                        int dividerLeft;
                        if (isRtl) {
                            dividerLeft = view.getLeft() - lp.leftMargin - mDividerVerticalWidth;
                        } else {
                            dividerLeft = view.getRight() + lp.rightMargin;
                        }

                        drawVerticalDivider(canvas, dividerLeft, flexLine.mTop,
                                flexLine.mCrossSize);
                    }
                }
                currentViewIndex++;
            }
            if (hasDividerBeforeFlexLine(i)) {
                int horizontalDividerTop;
                if (fromBottomToTop) {
                    horizontalDividerTop = flexLine.mBottom;
                } else {
                    horizontalDividerTop = flexLine.mTop - mDividerHorizontalHeight;
                }
                drawHorizontalDivider(canvas, paddingLeft, horizontalDividerTop,
                        horizontalDividerLength);
            }
            if (hasEndDividerAfterFlexLine(i)) {
                if ((mShowDividerHorizontal & SHOW_DIVIDER_END) > 0) {
                    int horizontalDividerTop;
                    if (fromBottomToTop) {
                        horizontalDividerTop = flexLine.mTop - mDividerHorizontalHeight;
                    } else {
                        horizontalDividerTop = flexLine.mBottom;
                    }
                    drawHorizontalDivider(canvas, paddingLeft, horizontalDividerTop,
                            horizontalDividerLength);
                }
            }
        }
    }

    private void drawDividersVertical(Canvas canvas, boolean isRtl, boolean fromBottomToTop) {
        int currentViewIndex = 0;
        int paddingTop = getPaddingTop();
        int paddingBottom = getPaddingBottom();
        int verticalDividerLength = Math.max(0, getHeight() - paddingBottom - paddingTop);
        for (int i = 0, size = mFlexLines.size(); i < size; i++) {
            FlexLine flexLine = mFlexLines.get(i);
            for (int j = 0; j < flexLine.mItemCount; j++) {
                View view = getReorderedChildAt(currentViewIndex);
                if (view == null || view.getVisibility() == View.GONE) {
                    continue;
                }
                LayoutParams lp = (LayoutParams) view.getLayoutParams();
                if (hasDividerBeforeChildAtAlongMainAxis(currentViewIndex, j)) {
                    int dividerTop;
                    if (fromBottomToTop) {
                        dividerTop = view.getBottom() + lp.bottomMargin;
                    } else {
                        dividerTop = view.getTop() - lp.topMargin - mDividerHorizontalHeight;
                    }

                    drawHorizontalDivider(canvas, flexLine.mLeft, dividerTop, flexLine.mCrossSize);
                }
                if (j == flexLine.mItemCount - 1) {
                    if ((mShowDividerHorizontal & SHOW_DIVIDER_END) > 0) {
                        int dividerTop;
                        if (fromBottomToTop) {
                            dividerTop = view.getTop() - lp.topMargin - mDividerHorizontalHeight;
                        } else {
                            dividerTop = view.getBottom() + lp.bottomMargin;
                        }

                        drawHorizontalDivider(canvas, flexLine.mLeft, dividerTop,
                                flexLine.mCrossSize);
                    }
                }
                currentViewIndex++;
            }
            if (hasDividerBeforeFlexLine(i)) {
                int verticalDividerLeft;
                if (isRtl) {
                    verticalDividerLeft = flexLine.mRight;
                } else {
                    verticalDividerLeft = flexLine.mLeft - mDividerVerticalWidth;
                }
                drawVerticalDivider(canvas, verticalDividerLeft, paddingTop,
                        verticalDividerLength);
            }
            if (hasEndDividerAfterFlexLine(i)) {
                if ((mShowDividerVertical & SHOW_DIVIDER_END) > 0) {
                    int verticalDividerLeft;
                    if (isRtl) {
                        verticalDividerLeft = flexLine.mLeft - mDividerVerticalWidth;
                    } else {
                        verticalDividerLeft = flexLine.mRight;
                    }
                    drawVerticalDivider(canvas, verticalDividerLeft, paddingTop,
                            verticalDividerLength);
                }
            }
        }
    }

    private void drawVerticalDivider(Canvas canvas, int left, int top, int length) {
        if (mDividerDrawableVertical == null) {
            return;
        }
        mDividerDrawableVertical.setBounds(left, top, left + mDividerVerticalWidth, top + length);
        mDividerDrawableVertical.draw(canvas);
    }

    private void drawHorizontalDivider(Canvas canvas, int left, int top, int length) {
        if (mDividerDrawableHorizontal == null) {
            return;
        }
        mDividerDrawableHorizontal
                .setBounds(left, top, left + length, top + mDividerHorizontalHeight);
        mDividerDrawableHorizontal.draw(canvas);
    }

    @Override
    protected boolean checkLayoutParams(ViewGroup.LayoutParams p) {
        return p instanceof CustomeTagLayout.LayoutParams;
    }

    @Override
    public LayoutParams generateLayoutParams(AttributeSet attrs) {
        return new CustomeTagLayout.LayoutParams(getContext(), attrs);
    }

    @Override
    protected ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams p) {
        return new LayoutParams(p);
    }

    @FlexDirection
    @Override
    public int getFlexDirection() {
        return mFlexDirection;
    }

    @Override
    public void setFlexDirection(@FlexDirection int flexDirection) {
        if (mFlexDirection != flexDirection) {
            mFlexDirection = flexDirection;
            requestLayout();
        }
    }

    @FlexWrap
    @Override
    public int getFlexWrap() {
        return mFlexWrap;
    }

    @Override
    public void setFlexWrap(@FlexWrap int flexWrap) {
        if (mFlexWrap != flexWrap) {
            mFlexWrap = flexWrap;
            requestLayout();
        }
    }

    @JustifyContent
    @Override
    public int getJustifyContent() {
        return mJustifyContent;
    }

    @Override
    public void setJustifyContent(@JustifyContent int justifyContent) {
        if (mJustifyContent != justifyContent) {
            mJustifyContent = justifyContent;
            requestLayout();
        }
    }

    @AlignItems
    @Override
    public int getAlignItems() {
        return mAlignItems;
    }

    @Override
    public void setAlignItems(@AlignItems int alignItems) {
        if (mAlignItems != alignItems) {
            mAlignItems = alignItems;
            requestLayout();
        }
    }

    @AlignContent
    @Override
    public int getAlignContent() {
        return mAlignContent;
    }

    @Override
    public void setAlignContent(@AlignContent int alignContent) {
        if (mAlignContent != alignContent) {
            mAlignContent = alignContent;
            requestLayout();
        }
    }

    @Override
    public List<FlexLine> getFlexLines() {
        List<FlexLine> result = new ArrayList<>(mFlexLines.size());
        for (FlexLine flexLine : mFlexLines) {
            if (flexLine.getItemCountNotGone() == 0) {
                continue;
            }
            result.add(flexLine);
        }
        return result;
    }

    @Override
    public int getDecorationLengthMainAxis(View view, int index, int indexInFlexLine) {
        int decorationLength = 0;
        if (isMainAxisDirectionHorizontal()) {
            if (hasDividerBeforeChildAtAlongMainAxis(index, indexInFlexLine)) {
                decorationLength += mDividerVerticalWidth;
            }
            if ((mShowDividerVertical & SHOW_DIVIDER_END) > 0) {
                decorationLength += mDividerVerticalWidth;
            }
        } else {
            if (hasDividerBeforeChildAtAlongMainAxis(index, indexInFlexLine)) {
                decorationLength += mDividerHorizontalHeight;
            }
            if ((mShowDividerHorizontal & SHOW_DIVIDER_END) > 0) {
                decorationLength += mDividerHorizontalHeight;
            }
        }
        return decorationLength;
    }

    @Override
    public int getDecorationLengthCrossAxis(View view) {
        return 0;
    }

    @Override
    public void onNewFlexLineAdded(FlexLine flexLine) {
        if (isMainAxisDirectionHorizontal()) {
            if ((mShowDividerVertical & SHOW_DIVIDER_END) > 0) {
                flexLine.mMainSize += mDividerVerticalWidth;
                flexLine.mDividerLengthInMainSize += mDividerVerticalWidth;
            }
        } else {
            if ((mShowDividerHorizontal & SHOW_DIVIDER_END) > 0) {
                flexLine.mMainSize += mDividerHorizontalHeight;
                flexLine.mDividerLengthInMainSize += mDividerHorizontalHeight;
            }
        }
    }

    @Override
    public int getChildWidthMeasureSpec(int widthSpec, int padding, int childDimension) {
        return getChildMeasureSpec(widthSpec, padding, childDimension);
    }

    @Override
    public int getChildHeightMeasureSpec(int heightSpec, int padding, int childDimension) {
        return getChildMeasureSpec(heightSpec, padding, childDimension);
    }

    @Override
    public void onNewFlexItemAdded(View view, int index, int indexInFlexLine, FlexLine flexLine) {
        // Check if the beginning or middle divider is required for the flex item
        if (hasDividerBeforeChildAtAlongMainAxis(index, indexInFlexLine)) {
            if (isMainAxisDirectionHorizontal()) {
                flexLine.mMainSize += mDividerVerticalWidth;
                flexLine.mDividerLengthInMainSize += mDividerVerticalWidth;
            } else {
                flexLine.mMainSize += mDividerHorizontalHeight;
                flexLine.mDividerLengthInMainSize += mDividerHorizontalHeight;
            }
        }
    }

    @Override
    public void setFlexLines(List<FlexLine> flexLines) {
        mFlexLines = flexLines;
    }

    @Override
    public List<FlexLine> getFlexLinesInternal() {
        return mFlexLines;
    }

    @Override
    public void updateViewCache(int position, View view) {
    }

    @Nullable
    @SuppressWarnings("UnusedDeclaration")
    public Drawable getDividerDrawableHorizontal() {
        return mDividerDrawableHorizontal;
    }

    @Nullable
    @SuppressWarnings("UnusedDeclaration")
    public Drawable getDividerDrawableVertical() {
        return mDividerDrawableVertical;
    }

    public void setDividerDrawable(Drawable divider) {
        setDividerDrawableHorizontal(divider);
        setDividerDrawableVertical(divider);
    }

    public void setDividerDrawableHorizontal(@Nullable Drawable divider) {
        if (divider == mDividerDrawableHorizontal) {
            return;
        }
        mDividerDrawableHorizontal = divider;
        if (divider != null) {
            mDividerHorizontalHeight = divider.getIntrinsicHeight();
        } else {
            mDividerHorizontalHeight = 0;
        }
        setWillNotDrawFlag();
        requestLayout();
    }

    public void setDividerDrawableVertical(@Nullable Drawable divider) {
        if (divider == mDividerDrawableVertical) {
            return;
        }
        mDividerDrawableVertical = divider;
        if (divider != null) {
            mDividerVerticalWidth = divider.getIntrinsicWidth();
        } else {
            mDividerVerticalWidth = 0;
        }
        setWillNotDrawFlag();
        requestLayout();
    }

    @CustomeTagLayout.DividerMode
    public int getShowDividerVertical() {
        return mShowDividerVertical;
    }

    @CustomeTagLayout.DividerMode
    public int getShowDividerHorizontal() {
        return mShowDividerHorizontal;
    }

    public void setShowDivider(@DividerMode int dividerMode) {
        setShowDividerVertical(dividerMode);
        setShowDividerHorizontal(dividerMode);
    }

    public void setShowDividerVertical(@DividerMode int dividerMode) {
        if (dividerMode != mShowDividerVertical) {
            mShowDividerVertical = dividerMode;
            requestLayout();
        }
    }

    public void setShowDividerHorizontal(@DividerMode int dividerMode) {
        if (dividerMode != mShowDividerHorizontal) {
            mShowDividerHorizontal = dividerMode;
            requestLayout();
        }
    }

    private void setWillNotDrawFlag() {
        if (mDividerDrawableHorizontal == null && mDividerDrawableVertical == null) {
            setWillNotDraw(true);
        } else {
            setWillNotDraw(false);
        }
    }

    private boolean hasDividerBeforeChildAtAlongMainAxis(int index, int indexInFlexLine) {
        if (allViewsAreGoneBefore(index, indexInFlexLine)) {
            if (isMainAxisDirectionHorizontal()) {
                return (mShowDividerVertical & SHOW_DIVIDER_BEGINNING) != 0;
            } else {
                return (mShowDividerHorizontal & SHOW_DIVIDER_BEGINNING) != 0;
            }
        } else {
            if (isMainAxisDirectionHorizontal()) {
                return (mShowDividerVertical & SHOW_DIVIDER_MIDDLE) != 0;
            } else {
                return (mShowDividerHorizontal & SHOW_DIVIDER_MIDDLE) != 0;
            }
        }
    }

    private boolean allViewsAreGoneBefore(int index, int indexInFlexLine) {
        for (int i = 1; i <= indexInFlexLine; i++) {
            View view = getReorderedChildAt(index - i);
            if (view != null && view.getVisibility() != View.GONE) {
                return false;
            }
        }
        return true;
    }

    private boolean hasDividerBeforeFlexLine(int flexLineIndex) {
        if (flexLineIndex < 0 || flexLineIndex >= mFlexLines.size()) {
            return false;
        }
        if (allFlexLinesAreDummyBefore(flexLineIndex)) {
            if (isMainAxisDirectionHorizontal()) {
                return (mShowDividerHorizontal & SHOW_DIVIDER_BEGINNING) != 0;
            } else {
                return (mShowDividerVertical & SHOW_DIVIDER_BEGINNING) != 0;
            }
        } else {
            if (isMainAxisDirectionHorizontal()) {
                return (mShowDividerHorizontal & SHOW_DIVIDER_MIDDLE) != 0;
            } else {
                return (mShowDividerVertical & SHOW_DIVIDER_MIDDLE) != 0;
            }
        }
    }

    private boolean allFlexLinesAreDummyBefore(int flexLineIndex) {
        for (int i = 0; i < flexLineIndex; i++) {
            if (mFlexLines.get(i).getItemCountNotGone() > 0) {
                return false;
            }
        }
        return true;
    }

    private boolean hasEndDividerAfterFlexLine(int flexLineIndex) {
        if (flexLineIndex < 0 || flexLineIndex >= mFlexLines.size()) {
            return false;
        }

        for (int i = flexLineIndex + 1; i < mFlexLines.size(); i++) {
            if (mFlexLines.get(i).getItemCountNotGone() > 0) {
                return false;
            }
        }
        if (isMainAxisDirectionHorizontal()) {
            return (mShowDividerHorizontal & SHOW_DIVIDER_END) != 0;
        } else {
            return (mShowDividerVertical & SHOW_DIVIDER_END) != 0;
        }

    }

    public static class LayoutParams extends ViewGroup.MarginLayoutParams implements FlexItem {
        private int mOrder = FlexItem.ORDER_DEFAULT;
        private float mFlexGrow = FlexItem.FLEX_GROW_DEFAULT;
        private float mFlexShrink = FlexItem.FLEX_SHRINK_DEFAULT;
        private int mAlignSelf = AlignSelf.AUTO;
        private float mFlexBasisPercent = FLEX_BASIS_PERCENT_DEFAULT;
        private int mMinWidth;
        private int mMinHeight;
        private int mMaxWidth = MAX_SIZE;
        private int mMaxHeight = MAX_SIZE;
        private boolean mWrapBefore;

        public LayoutParams(Context context, AttributeSet attrs) {
            super(context, attrs);

            TypedArray a = context
                    .obtainStyledAttributes(attrs, R.styleable.FlexboxLayout_Layout);
            mOrder = a.getInt(R.styleable.FlexboxLayout_Layout_layout_order, ORDER_DEFAULT);
            mFlexGrow = a
                    .getFloat(R.styleable.FlexboxLayout_Layout_layout_flexGrow, FLEX_GROW_DEFAULT);
            mFlexShrink = a.getFloat(R.styleable.FlexboxLayout_Layout_layout_flexShrink,
                    FLEX_SHRINK_DEFAULT);
            mAlignSelf = a
                    .getInt(R.styleable.FlexboxLayout_Layout_layout_alignSelf, AlignSelf.AUTO);
            mFlexBasisPercent = a
                    .getFraction(R.styleable.FlexboxLayout_Layout_layout_flexBasisPercent, 1, 1,
                            FLEX_BASIS_PERCENT_DEFAULT);
            mMinWidth = a
                    .getDimensionPixelSize(R.styleable.FlexboxLayout_Layout_layout_minWidth, 0);
            mMinHeight = a
                    .getDimensionPixelSize(R.styleable.FlexboxLayout_Layout_layout_minHeight, 0);
            mMaxWidth = a.getDimensionPixelSize(R.styleable.FlexboxLayout_Layout_layout_maxWidth,
                    MAX_SIZE);
            mMaxHeight = a.getDimensionPixelSize(R.styleable.FlexboxLayout_Layout_layout_maxHeight,
                    MAX_SIZE);
            mWrapBefore = a.getBoolean(R.styleable.FlexboxLayout_Layout_layout_wrapBefore, false);
            a.recycle();
        }

        public LayoutParams(LayoutParams source) {
            super(source);

            mOrder = source.mOrder;
            mFlexGrow = source.mFlexGrow;
            mFlexShrink = source.mFlexShrink;
            mAlignSelf = source.mAlignSelf;
            mFlexBasisPercent = source.mFlexBasisPercent;
            mMinWidth = source.mMinWidth;
            mMinHeight = source.mMinHeight;
            mMaxWidth = source.mMaxWidth;
            mMaxHeight = source.mMaxHeight;
            mWrapBefore = source.mWrapBefore;
        }

        public LayoutParams(ViewGroup.LayoutParams source) {
            super(source);
        }

        public LayoutParams(int width, int height) {
            super(new ViewGroup.LayoutParams(width, height));
        }

        public LayoutParams(MarginLayoutParams source) {
            super(source);
        }

        @Override
        public int getWidth() {
            return width;
        }

        @Override
        public void setWidth(int width) {
            this.width = width;
        }

        @Override
        public int getHeight() {
            return height;
        }

        @Override
        public void setHeight(int height) {
            this.height = height;
        }

        @Override
        public int getOrder() {
            return mOrder;
        }

        @Override
        public void setOrder(int order) {
            mOrder = order;
        }

        @Override
        public float getFlexGrow() {
            return mFlexGrow;
        }

        @Override
        public void setFlexGrow(float flexGrow) {
            this.mFlexGrow = flexGrow;
        }

        @Override
        public float getFlexShrink() {
            return mFlexShrink;
        }

        @Override
        public void setFlexShrink(float flexShrink) {
            this.mFlexShrink = flexShrink;
        }

        @AlignSelf
        @Override
        public int getAlignSelf() {
            return mAlignSelf;
        }

        @Override
        public void setAlignSelf(@AlignSelf int alignSelf) {
            this.mAlignSelf = alignSelf;
        }

        @Override
        public int getMinWidth() {
            return mMinWidth;
        }

        @Override
        public void setMinWidth(int minWidth) {
            this.mMinWidth = minWidth;
        }

        @Override
        public int getMinHeight() {
            return mMinHeight;
        }

        @Override
        public void setMinHeight(int minHeight) {
            this.mMinHeight = minHeight;
        }

        @Override
        public int getMaxWidth() {
            return mMaxWidth;
        }

        @Override
        public void setMaxWidth(int maxWidth) {
            this.mMaxWidth = maxWidth;
        }

        @Override
        public int getMaxHeight() {
            return mMaxHeight;
        }

        @Override
        public void setMaxHeight(int maxHeight) {
            this.mMaxHeight = maxHeight;
        }

        @Override
        public boolean isWrapBefore() {
            return mWrapBefore;
        }

        @Override
        public void setWrapBefore(boolean wrapBefore) {
            this.mWrapBefore = wrapBefore;
        }

        @Override
        public float getFlexBasisPercent() {
            return mFlexBasisPercent;
        }

        @Override
        public void setFlexBasisPercent(float flexBasisPercent) {
            this.mFlexBasisPercent = flexBasisPercent;
        }

        @Override
        public int getMarginLeft() {
            return leftMargin;
        }

        @Override
        public int getMarginTop() {
            return topMargin;
        }

        @Override
        public int getMarginRight() {
            return rightMargin;
        }

        @Override
        public int getMarginBottom() {
            return bottomMargin;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(this.mOrder);
            dest.writeFloat(this.mFlexGrow);
            dest.writeFloat(this.mFlexShrink);
            dest.writeInt(this.mAlignSelf);
            dest.writeFloat(this.mFlexBasisPercent);
            dest.writeInt(this.mMinWidth);
            dest.writeInt(this.mMinHeight);
            dest.writeInt(this.mMaxWidth);
            dest.writeInt(this.mMaxHeight);
            dest.writeByte(this.mWrapBefore ? (byte) 1 : (byte) 0);
            dest.writeInt(this.bottomMargin);
            dest.writeInt(this.leftMargin);
            dest.writeInt(this.rightMargin);
            dest.writeInt(this.topMargin);
            dest.writeInt(this.height);
            dest.writeInt(this.width);
        }

        protected LayoutParams(Parcel in) {
           super(0, 0);
            this.mOrder = in.readInt();
            this.mFlexGrow = in.readFloat();
            this.mFlexShrink = in.readFloat();
            this.mAlignSelf = in.readInt();
            this.mFlexBasisPercent = in.readFloat();
            this.mMinWidth = in.readInt();
            this.mMinHeight = in.readInt();
            this.mMaxWidth = in.readInt();
            this.mMaxHeight = in.readInt();
            this.mWrapBefore = in.readByte() != 0;
            this.bottomMargin = in.readInt();
            this.leftMargin = in.readInt();
            this.rightMargin = in.readInt();
            this.topMargin = in.readInt();
            this.height = in.readInt();
            this.width = in.readInt();
        }

        public static final Parcelable.Creator<LayoutParams> CREATOR
                = new Parcelable.Creator<LayoutParams>() {
            @Override
            public LayoutParams createFromParcel(Parcel source) {
                return new LayoutParams(source);
            }

            @Override
            public LayoutParams[] newArray(int size) {
                return new LayoutParams[size];
            }
        };
    }

}


interface TagContainer {
    int getFlexItemCount();

    View getFlexItemAt(int index);

    View getReorderedFlexItemAt(int index);

    void addView(View view);

    void addView(View view, int index);

    void removeAllViews();

    void removeViewAt(int index);

    int getFlexDirection();

    void setFlexDirection(@FlexDirection int flexDirection);

    int getFlexWrap();

    void setFlexWrap(@FlexWrap int flexWrap);

    int getJustifyContent();

    void setJustifyContent(@JustifyContent int justifyContent);

    int getAlignContent();

    void setAlignContent(@AlignContent int alignContent);

    int getAlignItems();

    void setAlignItems(@AlignItems int alignItems);

    List<FlexLine> getFlexLines();

    boolean isMainAxisDirectionHorizontal();

    int getDecorationLengthMainAxis(View view, int index, int indexInFlexLine);

    int getDecorationLengthCrossAxis(View view);

    int getPaddingTop();

    int getPaddingLeft();

    int getPaddingRight();

    int getPaddingBottom();

    int getPaddingStart();

    int getPaddingEnd();

    int getChildWidthMeasureSpec(int widthSpec, int padding, int childDimension);

    int getChildHeightMeasureSpec(int heightSpec, int padding, int childDimension);

    int getLargestMainSize();

    int getSumOfCrossSize();

    void onNewFlexItemAdded(View view, int index, int indexInFlexLine, FlexLine flexLine);

    void onNewFlexLineAdded(FlexLine flexLine);

    void setFlexLines(List<FlexLine> flexLines);

    List<FlexLine> getFlexLinesInternal();

    void updateViewCache(int position, View view);
}

@interface AlignSelf {
    int AUTO = -1;
    int FLEX_START = AlignItems.FLEX_START;
    int FLEX_END = AlignItems.FLEX_END;
    int CENTER = AlignItems.CENTER;
    int BASELINE = AlignItems.BASELINE;
    int STRETCH = AlignItems.STRETCH;
}

@interface AlignContent {
    int FLEX_START = 0;
    int FLEX_END = 1;
    int CENTER = 2;
    int SPACE_BETWEEN = 3;
    int SPACE_AROUND = 4;
    int STRETCH = 5;
}
@interface FlexDirection {
    int ROW = 0;
    int ROW_REVERSE = 1;
    int COLUMN = 2;
    int COLUMN_REVERSE = 3;
}

@interface FlexWrap {
    int NOWRAP = 0;
    int WRAP = 1;
    int WRAP_REVERSE = 2;
}

@interface JustifyContent {
    int FLEX_START = 0;
    int FLEX_END = 1;
    int CENTER = 2;
    int SPACE_BETWEEN = 3;
    int SPACE_AROUND = 4;
}

@interface AlignItems {
    int FLEX_START = 0;
    int FLEX_END = 1;
    int CENTER = 2;
    int BASELINE = 3;
    int STRETCH = 4;
}

class FlexLine {

    FlexLine() {
    }

    int mLeft = Integer.MAX_VALUE;

    int mTop = Integer.MAX_VALUE;

    int mRight = Integer.MIN_VALUE;

    int mBottom = Integer.MIN_VALUE;
    int mMainSize;
    int mDividerLengthInMainSize;
    int mCrossSize;
    int mItemCount;
    int mGoneItemCount;
    float mTotalFlexGrow;
    float mTotalFlexShrink;
    int mMaxBaseline;
    int mSumCrossSizeBefore;
    List<Integer> mIndicesAlignSelfStretch = new ArrayList<>();

    int mFirstIndex;

    int mLastIndex;

    public int getMainSize() {
        return mMainSize;
    }

    @SuppressWarnings("WeakerAccess")
    public int getCrossSize() {
        return mCrossSize;
    }

    @SuppressWarnings("WeakerAccess")
    public int getItemCount() {
        return mItemCount;
    }

    @SuppressWarnings("WeakerAccess")
    public int getItemCountNotGone() {
        return mItemCount - mGoneItemCount;
    }

    @SuppressWarnings("WeakerAccess")
    public float getTotalFlexGrow() {
        return mTotalFlexGrow;
    }

    @SuppressWarnings("WeakerAccess")
    public float getTotalFlexShrink() {
        return mTotalFlexShrink;
    }

    public int getFirstIndex() {
        return mFirstIndex;
    }

    void updatePositionFromView(View view, int leftDecoration, int topDecoration,
                                int rightDecoration, int bottomDecoration) {
        FlexItem flexItem = (FlexItem) view.getLayoutParams();
        mLeft = Math.min(mLeft, view.getLeft() - flexItem.getMarginLeft() - leftDecoration);
        mTop = Math.min(mTop, view.getTop() - flexItem.getMarginTop() - topDecoration);
        mRight = Math.max(mRight, view.getRight() + flexItem.getMarginRight() + rightDecoration);
        mBottom = Math
                .max(mBottom, view.getBottom() + flexItem.getMarginBottom() + bottomDecoration);
    }
}

interface FlexItem extends Parcelable {
    int ORDER_DEFAULT = 1;
    float FLEX_GROW_DEFAULT = 0f;
    float FLEX_SHRINK_DEFAULT = 1f;
    float FLEX_BASIS_PERCENT_DEFAULT = -1f;
    int MAX_SIZE = Integer.MAX_VALUE & ViewCompat.MEASURED_SIZE_MASK;

    int getWidth();

    void setWidth(int width);

    int getHeight();

    void setHeight(int height);

    int getOrder();

    void setOrder(int order);

    float getFlexGrow();

    void setFlexGrow(float flexGrow);

    float getFlexShrink();

    void setFlexShrink(float flexShrink);

    @AlignSelf
    int getAlignSelf();

    void setAlignSelf(@AlignSelf int alignSelf);

    int getMinWidth();

    void setMinWidth(int minWidth);

    int getMinHeight();

    void setMinHeight(int minHeight);

    int getMaxWidth();

    void setMaxWidth(int maxWidth);

    int getMaxHeight();

    void setMaxHeight(int maxHeight);

    boolean isWrapBefore();

    void setWrapBefore(boolean wrapBefore);

    float getFlexBasisPercent();

    void setFlexBasisPercent(float flexBasisPercent);

    int getMarginLeft();

    int getMarginTop();

    int getMarginRight();

    int getMarginBottom();

    int getMarginStart();

    int getMarginEnd();
}

class FlexboxHelper {

    private static final int INITIAL_CAPACITY = 10;

    private static final long MEASURE_SPEC_WIDTH_MASK = 0xffffffffL;

    private final TagContainer mFlexContainer;
    private boolean[] mChildrenFrozen;
    @Nullable
    int[] mIndexToFlexLine;
    @Nullable
    long[] mMeasureSpecCache;
    @Nullable
    private long[] mMeasuredSizeCache;

    FlexboxHelper(TagContainer flexContainer) {
        mFlexContainer = flexContainer;
    }

    int[] createReorderedIndices(View viewBeforeAdded, int indexForViewBeforeAdded,
                                 ViewGroup.LayoutParams paramsForViewBeforeAdded, SparseIntArray orderCache) {
        int childCount = mFlexContainer.getFlexItemCount();
        List<FlexboxHelper.Order> orders = createOrders(childCount);
        FlexboxHelper.Order orderForViewToBeAdded = new FlexboxHelper.Order();
        if (viewBeforeAdded != null
                && paramsForViewBeforeAdded instanceof FlexItem) {
            orderForViewToBeAdded.order = ((FlexItem)
                    paramsForViewBeforeAdded).getOrder();
        } else {
            orderForViewToBeAdded.order = FlexItem.ORDER_DEFAULT;
        }

        if (indexForViewBeforeAdded == -1 || indexForViewBeforeAdded == childCount) {
            orderForViewToBeAdded.index = childCount;
        } else if (indexForViewBeforeAdded < mFlexContainer.getFlexItemCount()) {
            orderForViewToBeAdded.index = indexForViewBeforeAdded;
            for (int i = indexForViewBeforeAdded; i < childCount; i++) {
                orders.get(i).index++;
            }
        } else {
            orderForViewToBeAdded.index = childCount;
        }
        orders.add(orderForViewToBeAdded);

        return sortOrdersIntoReorderedIndices(childCount + 1, orders, orderCache);
    }

    int[] createReorderedIndices(SparseIntArray orderCache) {
        int childCount = mFlexContainer.getFlexItemCount();
        List<FlexboxHelper.Order> orders = createOrders(childCount);
        return sortOrdersIntoReorderedIndices(childCount, orders, orderCache);
    }

    @NonNull
    private List<FlexboxHelper.Order> createOrders(int childCount) {
        List<FlexboxHelper.Order> orders = new ArrayList<>(childCount);
        for (int i = 0; i < childCount; i++) {
            View child = mFlexContainer.getFlexItemAt(i);
            FlexItem flexItem = (FlexItem) child.getLayoutParams();
            FlexboxHelper.Order order = new FlexboxHelper.Order();
            order.order = flexItem.getOrder();
            order.index = i;
            orders.add(order);
        }
        return orders;
    }

    boolean isOrderChangedFromLastMeasurement(SparseIntArray orderCache) {
        int childCount = mFlexContainer.getFlexItemCount();
        if (orderCache.size() != childCount) {
            return true;
        }
        for (int i = 0; i < childCount; i++) {
            View view = mFlexContainer.getFlexItemAt(i);
            if (view == null) {
                continue;
            }
            FlexItem flexItem = (FlexItem) view.getLayoutParams();
            if (flexItem.getOrder() != orderCache.get(i)) {
                return true;
            }
        }
        return false;
    }

    private int[] sortOrdersIntoReorderedIndices(int childCount, List<FlexboxHelper.Order> orders,
                                                 SparseIntArray orderCache) {
        Collections.sort(orders);
        orderCache.clear();
        int[] reorderedIndices = new int[childCount];
        int i = 0;
        for (FlexboxHelper.Order order : orders) {
            reorderedIndices[i] = order.index;
            orderCache.append(order.index, order.order);
            i++;
        }
        return reorderedIndices;
    }

    void calculateHorizontalFlexLines(FlexboxHelper.FlexLinesResult result, int widthMeasureSpec,
                                      int heightMeasureSpec) {
        calculateFlexLines(result, widthMeasureSpec, heightMeasureSpec, Integer.MAX_VALUE,
                0, NO_POSITION, null);
    }


    void calculateHorizontalFlexLines(FlexboxHelper.FlexLinesResult result, int widthMeasureSpec,
                                      int heightMeasureSpec, int needsCalcAmount, int fromIndex,
                                      @Nullable List<FlexLine> existingLines) {
        calculateFlexLines(result, widthMeasureSpec, heightMeasureSpec, needsCalcAmount,
                fromIndex, NO_POSITION, existingLines);
    }

    void calculateHorizontalFlexLinesToIndex(FlexboxHelper.FlexLinesResult result, int widthMeasureSpec,
                                             int heightMeasureSpec, int needsCalcAmount, int toIndex, List<FlexLine> existingLines) {
        calculateFlexLines(result, widthMeasureSpec, heightMeasureSpec, needsCalcAmount,
                0, toIndex, existingLines);
    }

    void calculateVerticalFlexLines(FlexboxHelper.FlexLinesResult result, int widthMeasureSpec, int heightMeasureSpec) {
        calculateFlexLines(result, heightMeasureSpec, widthMeasureSpec, Integer.MAX_VALUE,
                0, NO_POSITION, null);
    }
    void calculateVerticalFlexLines(FlexboxHelper.FlexLinesResult result, int widthMeasureSpec,
                                    int heightMeasureSpec, int needsCalcAmount, int fromIndex,
                                    @Nullable List<FlexLine> existingLines) {
        calculateFlexLines(result, heightMeasureSpec, widthMeasureSpec, needsCalcAmount,
                fromIndex, NO_POSITION, existingLines);
    }

    void calculateVerticalFlexLinesToIndex(FlexboxHelper.FlexLinesResult result, int widthMeasureSpec,
                                           int heightMeasureSpec, int needsCalcAmount, int toIndex, List<FlexLine> existingLines) {
        calculateFlexLines(result, heightMeasureSpec, widthMeasureSpec, needsCalcAmount,
                0, toIndex, existingLines);
    }

    void calculateFlexLines(FlexboxHelper.FlexLinesResult result, int mainMeasureSpec,
                            int crossMeasureSpec, int needsCalcAmount, int fromIndex, int toIndex,
                            @Nullable List<FlexLine> existingLines) {

        boolean isMainHorizontal = mFlexContainer.isMainAxisDirectionHorizontal();

        int mainMode = View.MeasureSpec.getMode(mainMeasureSpec);
        int mainSize = View.MeasureSpec.getSize(mainMeasureSpec);

        int childState = 0;

        List<FlexLine> flexLines;
        if (existingLines == null) {
            flexLines = new ArrayList<>();
        } else {
            flexLines = existingLines;
        }

        result.mFlexLines = flexLines;

        boolean reachedToIndex = toIndex == NO_POSITION;

        int mainPaddingStart = getPaddingStartMain(isMainHorizontal);
        int mainPaddingEnd = getPaddingEndMain(isMainHorizontal);
        int crossPaddingStart = getPaddingStartCross(isMainHorizontal);
        int crossPaddingEnd = getPaddingEndCross(isMainHorizontal);

        int largestSizeInCross = Integer.MIN_VALUE;
      int sumCrossSize = 0;

        int indexInFlexLine = 0;

        FlexLine flexLine = new FlexLine();
        flexLine.mFirstIndex = fromIndex;
        flexLine.mMainSize = mainPaddingStart + mainPaddingEnd;

        int childCount = mFlexContainer.getFlexItemCount();
        for (int i = fromIndex; i < childCount; i++) {
            View child = mFlexContainer.getReorderedFlexItemAt(i);

            if (child == null) {
                if (isLastFlexItem(i, childCount, flexLine)) {
                    addFlexLine(flexLines, flexLine, i, sumCrossSize);
                }
                continue;
            } else if (child.getVisibility() == View.GONE) {
                flexLine.mGoneItemCount++;
                flexLine.mItemCount++;
                if (isLastFlexItem(i, childCount, flexLine)) {
                    addFlexLine(flexLines, flexLine, i, sumCrossSize);
                }
                continue;
            }

            FlexItem flexItem = (FlexItem) child.getLayoutParams();

            if (flexItem.getAlignSelf() == AlignItems.STRETCH) {
                flexLine.mIndicesAlignSelfStretch.add(i);
            }

            int childMainSize = getFlexItemSizeMain(flexItem, isMainHorizontal);

            if (flexItem.getFlexBasisPercent() != FlexItem.FLEX_BASIS_PERCENT_DEFAULT
                    && mainMode == View.MeasureSpec.EXACTLY) {
                childMainSize = Math.round(mainSize * flexItem.getFlexBasisPercent());
            }

            int childMainMeasureSpec;
            int childCrossMeasureSpec;
            if (isMainHorizontal) {
                childMainMeasureSpec = mFlexContainer.getChildWidthMeasureSpec(mainMeasureSpec,
                        mainPaddingStart + mainPaddingEnd +
                                getFlexItemMarginStartMain(flexItem, true) +
                                getFlexItemMarginEndMain(flexItem, true),
                        childMainSize);
                childCrossMeasureSpec = mFlexContainer.getChildHeightMeasureSpec(crossMeasureSpec,
                        crossPaddingStart + crossPaddingEnd +
                                getFlexItemMarginStartCross(flexItem, true) +
                                getFlexItemMarginEndCross(flexItem, true)
                                + sumCrossSize,
                        getFlexItemSizeCross(flexItem, true));
                child.measure(childMainMeasureSpec, childCrossMeasureSpec);
                updateMeasureCache(i, childMainMeasureSpec, childCrossMeasureSpec, child);
            } else {
                childCrossMeasureSpec = mFlexContainer.getChildWidthMeasureSpec(crossMeasureSpec,
                        crossPaddingStart + crossPaddingEnd +
                                getFlexItemMarginStartCross(flexItem, false) +
                                getFlexItemMarginEndCross(flexItem, false) + sumCrossSize,
                        getFlexItemSizeCross(flexItem, false));
                childMainMeasureSpec = mFlexContainer.getChildHeightMeasureSpec(mainMeasureSpec,
                        mainPaddingStart + mainPaddingEnd +
                                getFlexItemMarginStartMain(flexItem, false) +
                                getFlexItemMarginEndMain(flexItem, false),
                        childMainSize);
                child.measure(childCrossMeasureSpec, childMainMeasureSpec);
                updateMeasureCache(i, childCrossMeasureSpec, childMainMeasureSpec, child);
            }
            mFlexContainer.updateViewCache(i, child);
            checkSizeConstraints(child, i);

            childState = ViewCompat.combineMeasuredStates(
                    childState, ViewCompat.getMeasuredState(child));

            if (isWrapRequired(child, mainMode, mainSize, flexLine.mMainSize,
                    getViewMeasuredSizeMain(child, isMainHorizontal)
                            + getFlexItemMarginStartMain(flexItem, isMainHorizontal) +
                            getFlexItemMarginEndMain(flexItem, isMainHorizontal),
                    flexItem, i, indexInFlexLine)) {
                if (flexLine.getItemCountNotGone() > 0) {
                    addFlexLine(flexLines, flexLine, i > 0 ? i - 1 : 0, sumCrossSize);
                    sumCrossSize += flexLine.mCrossSize;
                }

                if (isMainHorizontal) {
                    if (flexItem.getHeight() == ViewGroup.LayoutParams.MATCH_PARENT) {
                        childCrossMeasureSpec = mFlexContainer.getChildHeightMeasureSpec(
                                crossMeasureSpec,
                                mFlexContainer.getPaddingTop() + mFlexContainer.getPaddingBottom()
                                        + flexItem.getMarginTop()
                                        + flexItem.getMarginBottom() + sumCrossSize,
                                flexItem.getHeight());
                        child.measure(childMainMeasureSpec, childCrossMeasureSpec);
                        checkSizeConstraints(child, i);
                    }
                } else {
                    if (flexItem.getWidth() == ViewGroup.LayoutParams.MATCH_PARENT) {
                        childCrossMeasureSpec = mFlexContainer.getChildWidthMeasureSpec(
                                crossMeasureSpec,
                                mFlexContainer.getPaddingLeft() + mFlexContainer.getPaddingRight()
                                        + flexItem.getMarginLeft()
                                        + flexItem.getMarginRight() + sumCrossSize,
                                flexItem.getWidth());
                        child.measure(childCrossMeasureSpec, childMainMeasureSpec);
                        checkSizeConstraints(child, i);
                    }
                }

                flexLine = new FlexLine();
                flexLine.mItemCount = 1;
                flexLine.mMainSize = mainPaddingStart + mainPaddingEnd;
                flexLine.mFirstIndex = i;
                indexInFlexLine = 0;
                largestSizeInCross = Integer.MIN_VALUE;
            } else {
                flexLine.mItemCount++;
                indexInFlexLine++;
            }
            if (mIndexToFlexLine != null) {
                mIndexToFlexLine[i] = flexLines.size();
            }
            flexLine.mMainSize += getViewMeasuredSizeMain(child, isMainHorizontal)
                    + getFlexItemMarginStartMain(flexItem, isMainHorizontal) +
                    getFlexItemMarginEndMain(flexItem, isMainHorizontal);
            flexLine.mTotalFlexGrow += flexItem.getFlexGrow();
            flexLine.mTotalFlexShrink += flexItem.getFlexShrink();

            mFlexContainer.onNewFlexItemAdded(child, i, indexInFlexLine, flexLine);

            largestSizeInCross = Math.max(largestSizeInCross,
                    getViewMeasuredSizeCross(child, isMainHorizontal) +
                            getFlexItemMarginStartCross(flexItem, isMainHorizontal) +
                            getFlexItemMarginEndCross(flexItem, isMainHorizontal) +
                            mFlexContainer.getDecorationLengthCrossAxis(child));

            flexLine.mCrossSize = Math.max(flexLine.mCrossSize, largestSizeInCross);

            if (isMainHorizontal) {
                if (mFlexContainer.getFlexWrap() != FlexWrap.WRAP_REVERSE) {
                    flexLine.mMaxBaseline = Math.max(flexLine.mMaxBaseline,
                            child.getBaseline() + flexItem.getMarginTop());
                } else {
                    flexLine.mMaxBaseline = Math.max(flexLine.mMaxBaseline,
                            child.getMeasuredHeight() - child.getBaseline()
                                    + flexItem.getMarginBottom());
                }
            }

            if (isLastFlexItem(i, childCount, flexLine)) {
                addFlexLine(flexLines, flexLine, i, sumCrossSize);
                sumCrossSize += flexLine.mCrossSize;
            }

            if (toIndex != NO_POSITION
                    && flexLines.size() > 0
                    && flexLines.get(flexLines.size() - 1).mLastIndex >= toIndex
                    && i >= toIndex
                    && !reachedToIndex) {
                  sumCrossSize = -flexLine.getCrossSize();
                reachedToIndex = true;
            }
            if (sumCrossSize > needsCalcAmount && reachedToIndex) {
                    break;
            }
        }

        result.mChildState = childState;
    }
    private int getPaddingStartMain(boolean isMainHorizontal) {
        if (isMainHorizontal) {
            return mFlexContainer.getPaddingStart();
        }

        return mFlexContainer.getPaddingTop();
    }
    private int getPaddingEndMain(boolean isMainHorizontal) {
        if (isMainHorizontal) {
            return mFlexContainer.getPaddingEnd();
        }

        return mFlexContainer.getPaddingBottom();
    }
    private int getPaddingStartCross(boolean isMainHorizontal) {
        if (isMainHorizontal) {
            return mFlexContainer.getPaddingTop();
        }

        return mFlexContainer.getPaddingStart();
    }

    private int getPaddingEndCross(boolean isMainHorizontal) {
        if (isMainHorizontal) {
            return mFlexContainer.getPaddingBottom();
        }

        return mFlexContainer.getPaddingEnd();
    }
    private int getViewMeasuredSizeMain(View view, boolean isMainHorizontal) {
        if (isMainHorizontal) {
            return view.getMeasuredWidth();
        }

        return view.getMeasuredHeight();
    }
    private int getViewMeasuredSizeCross(View view, boolean isMainHorizontal) {
        if (isMainHorizontal) {
            return view.getMeasuredHeight();
        }

        return view.getMeasuredWidth();
    }
    private int getFlexItemSizeMain(FlexItem flexItem, boolean isMainHorizontal) {
        if (isMainHorizontal) {
            return flexItem.getWidth();
        }

        return flexItem.getHeight();
    }

    private int getFlexItemSizeCross(FlexItem flexItem, boolean isMainHorizontal) {
        if (isMainHorizontal) {
            return flexItem.getHeight();
        }

        return flexItem.getWidth();
    }
    private int getFlexItemMarginStartMain(FlexItem flexItem, boolean isMainHorizontal) {
        if (isMainHorizontal) {
            return flexItem.getMarginStart();
        }

        return flexItem.getMarginTop();
    }

    private int getFlexItemMarginEndMain(FlexItem flexItem, boolean isMainHorizontal) {
        if (isMainHorizontal) {
            return flexItem.getMarginEnd();
        }

        return flexItem.getMarginBottom();
    }

    private int getFlexItemMarginStartCross(FlexItem flexItem, boolean isMainHorizontal) {
        if (isMainHorizontal) {
            return flexItem.getMarginTop();
        }

        return flexItem.getMarginStart();
    }

    private int getFlexItemMarginEndCross(FlexItem flexItem, boolean isMainHorizontal) {
        if (isMainHorizontal) {
            return flexItem.getMarginBottom();
        }

        return flexItem.getMarginEnd();
    }

    private boolean isWrapRequired(View view, int mode, int maxSize, int currentLength,
                                   int childLength, FlexItem flexItem, int index, int indexInFlexLine) {
        if (mFlexContainer.getFlexWrap() == FlexWrap.NOWRAP) {
            return false;
        }
        if (flexItem.isWrapBefore()) {
            return true;
        }
        if (mode == View.MeasureSpec.UNSPECIFIED) {
            return false;
        }
        int decorationLength =
                mFlexContainer.getDecorationLengthMainAxis(view, index, indexInFlexLine);
        if (decorationLength > 0) {
            childLength += decorationLength;
        }
        return maxSize < currentLength + childLength;
    }

    private boolean isLastFlexItem(int childIndex, int childCount,
                                   FlexLine flexLine) {
        return childIndex == childCount - 1 && flexLine.getItemCountNotGone() != 0;
    }

    private void addFlexLine(List<FlexLine> flexLines, FlexLine flexLine, int viewIndex,
                             int usedCrossSizeSoFar) {
        flexLine.mSumCrossSizeBefore = usedCrossSizeSoFar;
        mFlexContainer.onNewFlexLineAdded(flexLine);
        flexLine.mLastIndex = viewIndex;
        flexLines.add(flexLine);
    }

    private void checkSizeConstraints(View view, int index) {
        boolean needsMeasure = false;
        FlexItem flexItem = (FlexItem) view.getLayoutParams();
        int childWidth = view.getMeasuredWidth();
        int childHeight = view.getMeasuredHeight();

        if (childWidth < flexItem.getMinWidth()) {
            needsMeasure = true;
            childWidth = flexItem.getMinWidth();
        } else if (childWidth > flexItem.getMaxWidth()) {
            needsMeasure = true;
            childWidth = flexItem.getMaxWidth();
        }

        if (childHeight < flexItem.getMinHeight()) {
            needsMeasure = true;
            childHeight = flexItem.getMinHeight();
        } else if (childHeight > flexItem.getMaxHeight()) {
            needsMeasure = true;
            childHeight = flexItem.getMaxHeight();
        }
        if (needsMeasure) {
            int widthSpec = View.MeasureSpec.makeMeasureSpec(childWidth, View.MeasureSpec.EXACTLY);
            int heightSpec = View.MeasureSpec
                    .makeMeasureSpec(childHeight, View.MeasureSpec.EXACTLY);
            view.measure(widthSpec, heightSpec);
            updateMeasureCache(index, widthSpec, heightSpec, view);
            mFlexContainer.updateViewCache(index, view);
        }
    }
  void determineMainSize(int widthMeasureSpec, int heightMeasureSpec) {
        determineMainSize(widthMeasureSpec, heightMeasureSpec, 0);
    }

     void determineMainSize(int widthMeasureSpec, int heightMeasureSpec, int fromIndex) {
        ensureChildrenFrozen(mFlexContainer.getFlexItemCount());
        if (fromIndex >= mFlexContainer.getFlexItemCount()) {
            return;
        }
        int mainSize;
        int paddingAlongMainAxis;
        int flexDirection = mFlexContainer.getFlexDirection();
        switch (mFlexContainer.getFlexDirection()) {
            case FlexDirection.ROW: // Intentional fall through
            case FlexDirection.ROW_REVERSE:
                int widthMode = View.MeasureSpec.getMode(widthMeasureSpec);
                int widthSize = View.MeasureSpec.getSize(widthMeasureSpec);
                if (widthMode == View.MeasureSpec.EXACTLY) {
                    mainSize = widthSize;
                } else {
                    mainSize = mFlexContainer.getLargestMainSize();
                }
                paddingAlongMainAxis = mFlexContainer.getPaddingLeft()
                        + mFlexContainer.getPaddingRight();
                break;
            case FlexDirection.COLUMN: // Intentional fall through
            case FlexDirection.COLUMN_REVERSE:
                int heightMode = View.MeasureSpec.getMode(heightMeasureSpec);
                int heightSize = View.MeasureSpec.getSize(heightMeasureSpec);
                if (heightMode == View.MeasureSpec.EXACTLY) {
                    mainSize = heightSize;
                } else {
                    mainSize = mFlexContainer.getLargestMainSize();
                }
                paddingAlongMainAxis = mFlexContainer.getPaddingTop()
                        + mFlexContainer.getPaddingBottom();
                break;
            default:
                throw new IllegalArgumentException("Invalid flex direction: " + flexDirection);
        }

        int flexLineIndex = 0;
        if (mIndexToFlexLine != null) {
            flexLineIndex = mIndexToFlexLine[fromIndex];
        }
        List<FlexLine> flexLines = mFlexContainer.getFlexLinesInternal();
        for (int i = flexLineIndex, size = flexLines.size(); i < size; i++) {
            FlexLine flexLine = flexLines.get(i);
            if (flexLine.mMainSize < mainSize) {
                expandFlexItems(widthMeasureSpec, heightMeasureSpec, flexLine,
                        mainSize, paddingAlongMainAxis, false);
            } else {
                shrinkFlexItems(widthMeasureSpec, heightMeasureSpec, flexLine,
                        mainSize, paddingAlongMainAxis, false);
            }
        }
    }

    private void ensureChildrenFrozen(int size) {
        if (mChildrenFrozen == null) {
            mChildrenFrozen = new boolean[size < INITIAL_CAPACITY ? INITIAL_CAPACITY : size];
        } else if (mChildrenFrozen.length < size) {
            int newCapacity = mChildrenFrozen.length * 2;
            mChildrenFrozen = new boolean[newCapacity >= size ? newCapacity : size];
        } else {
            Arrays.fill(mChildrenFrozen, false);
        }
    }

     private void expandFlexItems(int widthMeasureSpec, int heightMeasureSpec, FlexLine flexLine,
                                 int maxMainSize, int paddingAlongMainAxis, boolean calledRecursively) {
        if (flexLine.mTotalFlexGrow <= 0 || maxMainSize < flexLine.mMainSize) {
            return;
        }
        int sizeBeforeExpand = flexLine.mMainSize;
        boolean needsReexpand = false;
        float unitSpace = (maxMainSize - flexLine.mMainSize) / flexLine.mTotalFlexGrow;
        flexLine.mMainSize = paddingAlongMainAxis + flexLine.mDividerLengthInMainSize;
      int largestCrossSize = 0;
        if (!calledRecursively) {
            flexLine.mCrossSize = Integer.MIN_VALUE;
        }
        float accumulatedRoundError = 0;
        for (int i = 0; i < flexLine.mItemCount; i++) {
            int index = flexLine.mFirstIndex + i;
            View child = mFlexContainer.getReorderedFlexItemAt(index);
            if (child == null || child.getVisibility() == View.GONE) {
                continue;
            }
            FlexItem flexItem = (FlexItem) child.getLayoutParams();
            int flexDirection = mFlexContainer.getFlexDirection();
            if (flexDirection == FlexDirection.ROW || flexDirection == FlexDirection.ROW_REVERSE) {

                int childMeasuredWidth = child.getMeasuredWidth();
                if (mMeasuredSizeCache != null) {
                    childMeasuredWidth = extractLowerInt(mMeasuredSizeCache[index]);
                }
                int childMeasuredHeight = child.getMeasuredHeight();
                if (mMeasuredSizeCache != null) {
                    childMeasuredHeight = extractHigherInt(mMeasuredSizeCache[index]);
                }
                if (!mChildrenFrozen[index] && flexItem.getFlexGrow() > 0f) {
                    float rawCalculatedWidth = childMeasuredWidth
                            + unitSpace * flexItem.getFlexGrow();
                    if (i == flexLine.mItemCount - 1) {
                        rawCalculatedWidth += accumulatedRoundError;
                        accumulatedRoundError = 0;
                    }
                    int newWidth = Math.round(rawCalculatedWidth);
                    if (newWidth > flexItem.getMaxWidth()) {
                        needsReexpand = true;
                        newWidth = flexItem.getMaxWidth();
                        mChildrenFrozen[index] = true;
                        flexLine.mTotalFlexGrow -= flexItem.getFlexGrow();
                    } else {
                        accumulatedRoundError += (rawCalculatedWidth - newWidth);
                        if (accumulatedRoundError > 1.0) {
                            newWidth += 1;
                            accumulatedRoundError -= 1.0;
                        } else if (accumulatedRoundError < -1.0) {
                            newWidth -= 1;
                            accumulatedRoundError += 1.0;
                        }
                    }
                    int childHeightMeasureSpec = getChildHeightMeasureSpecInternal(
                            heightMeasureSpec, flexItem, flexLine.mSumCrossSizeBefore);
                    int childWidthMeasureSpec = View.MeasureSpec.makeMeasureSpec(newWidth,
                            View.MeasureSpec.EXACTLY);
                    child.measure(childWidthMeasureSpec, childHeightMeasureSpec);
                    childMeasuredWidth = child.getMeasuredWidth();
                    childMeasuredHeight = child.getMeasuredHeight();
                    updateMeasureCache(index, childWidthMeasureSpec, childHeightMeasureSpec,
                            child);
                    mFlexContainer.updateViewCache(index, child);
                }
                largestCrossSize = Math.max(largestCrossSize, childMeasuredHeight
                        + flexItem.getMarginTop() + flexItem.getMarginBottom()
                        + mFlexContainer.getDecorationLengthCrossAxis(child));
                flexLine.mMainSize += childMeasuredWidth + flexItem.getMarginLeft()
                        + flexItem.getMarginRight();
            } else {

                int childMeasuredHeight = child.getMeasuredHeight();
                if (mMeasuredSizeCache != null) {
                    childMeasuredHeight =
                            extractHigherInt(mMeasuredSizeCache[index]);
                }
                int childMeasuredWidth = child.getMeasuredWidth();
                if (mMeasuredSizeCache != null) {
                    childMeasuredWidth =
                            extractLowerInt(mMeasuredSizeCache[index]);
                }
                if (!mChildrenFrozen[index] && flexItem.getFlexGrow() > 0f) {
                    float rawCalculatedHeight = childMeasuredHeight
                            + unitSpace * flexItem.getFlexGrow();
                    if (i == flexLine.mItemCount - 1) {
                        rawCalculatedHeight += accumulatedRoundError;
                        accumulatedRoundError = 0;
                    }
                    int newHeight = Math.round(rawCalculatedHeight);
                    if (newHeight > flexItem.getMaxHeight()) {
                        needsReexpand = true;
                        newHeight = flexItem.getMaxHeight();
                        mChildrenFrozen[index] = true;
                        flexLine.mTotalFlexGrow -= flexItem.getFlexGrow();
                    } else {
                        accumulatedRoundError += (rawCalculatedHeight - newHeight);
                        if (accumulatedRoundError > 1.0) {
                            newHeight += 1;
                            accumulatedRoundError -= 1.0;
                        } else if (accumulatedRoundError < -1.0) {
                            newHeight -= 1;
                            accumulatedRoundError += 1.0;
                        }
                    }
                    int childWidthMeasureSpec = getChildWidthMeasureSpecInternal(widthMeasureSpec,
                            flexItem, flexLine.mSumCrossSizeBefore);
                    int childHeightMeasureSpec = View.MeasureSpec.makeMeasureSpec(newHeight,
                            View.MeasureSpec.EXACTLY);
                    child.measure(childWidthMeasureSpec, childHeightMeasureSpec);
                    childMeasuredWidth = child.getMeasuredWidth();
                    childMeasuredHeight = child.getMeasuredHeight();
                    updateMeasureCache(index, childWidthMeasureSpec, childHeightMeasureSpec,
                            child);
                    mFlexContainer.updateViewCache(index, child);
                }
                largestCrossSize = Math.max(largestCrossSize, childMeasuredWidth
                        + flexItem.getMarginLeft() + flexItem.getMarginRight()
                        + mFlexContainer.getDecorationLengthCrossAxis(child));
                flexLine.mMainSize += childMeasuredHeight + flexItem.getMarginTop()
                        + flexItem.getMarginBottom();
            }
            flexLine.mCrossSize = Math.max(flexLine.mCrossSize, largestCrossSize);
        }

        if (needsReexpand && sizeBeforeExpand != flexLine.mMainSize) {
            expandFlexItems(widthMeasureSpec, heightMeasureSpec, flexLine, maxMainSize,
                    paddingAlongMainAxis, true);
        }
    }
    private void shrinkFlexItems(int widthMeasureSpec, int heightMeasureSpec, FlexLine flexLine,
                                 int maxMainSize, int paddingAlongMainAxis, boolean calledRecursively) {
        int sizeBeforeShrink = flexLine.mMainSize;
        if (flexLine.mTotalFlexShrink <= 0 || maxMainSize > flexLine.mMainSize) {
            return;
        }
        boolean needsReshrink = false;
        float unitShrink = (flexLine.mMainSize - maxMainSize) / flexLine.mTotalFlexShrink;
        float accumulatedRoundError = 0;
        flexLine.mMainSize = paddingAlongMainAxis + flexLine.mDividerLengthInMainSize;
        int largestCrossSize = 0;
        if (!calledRecursively) {
            flexLine.mCrossSize = Integer.MIN_VALUE;
        }
        for (int i = 0; i < flexLine.mItemCount; i++) {
            int index = flexLine.mFirstIndex + i;
            View child = mFlexContainer.getReorderedFlexItemAt(index);
            if (child == null || child.getVisibility() == View.GONE) {
                continue;
            }
            FlexItem flexItem = (FlexItem) child.getLayoutParams();
            int flexDirection = mFlexContainer.getFlexDirection();
            if (flexDirection == FlexDirection.ROW || flexDirection == FlexDirection.ROW_REVERSE) {
                int childMeasuredWidth = child.getMeasuredWidth();
                if (mMeasuredSizeCache != null) {
                    childMeasuredWidth = extractLowerInt(mMeasuredSizeCache[index]);
                }
                int childMeasuredHeight = child.getMeasuredHeight();
                if (mMeasuredSizeCache != null) {
                    childMeasuredHeight = extractHigherInt(mMeasuredSizeCache[index]);
                }
                if (!mChildrenFrozen[index] && flexItem.getFlexShrink() > 0f) {
                    float rawCalculatedWidth = childMeasuredWidth
                            - unitShrink * flexItem.getFlexShrink();
                    if (i == flexLine.mItemCount - 1) {
                        rawCalculatedWidth += accumulatedRoundError;
                        accumulatedRoundError = 0;
                    }
                    int newWidth = Math.round(rawCalculatedWidth);
                    if (newWidth < flexItem.getMinWidth()) {
                        needsReshrink = true;
                        newWidth = flexItem.getMinWidth();
                        mChildrenFrozen[index] = true;
                        flexLine.mTotalFlexShrink -= flexItem.getFlexShrink();
                    } else {
                        accumulatedRoundError += (rawCalculatedWidth - newWidth);
                        if (accumulatedRoundError > 1.0) {
                            newWidth += 1;
                            accumulatedRoundError -= 1;
                        } else if (accumulatedRoundError < -1.0) {
                            newWidth -= 1;
                            accumulatedRoundError += 1;
                        }
                    }
                    int childHeightMeasureSpec = getChildHeightMeasureSpecInternal(
                            heightMeasureSpec, flexItem, flexLine.mSumCrossSizeBefore);
                    int childWidthMeasureSpec =
                            View.MeasureSpec.makeMeasureSpec(newWidth, View.MeasureSpec.EXACTLY);
                    child.measure(childWidthMeasureSpec, childHeightMeasureSpec);

                    childMeasuredWidth = child.getMeasuredWidth();
                    childMeasuredHeight = child.getMeasuredHeight();
                    updateMeasureCache(index, childWidthMeasureSpec, childHeightMeasureSpec,
                            child);
                    mFlexContainer.updateViewCache(index, child);
                }
                largestCrossSize = Math.max(largestCrossSize, childMeasuredHeight +
                        flexItem.getMarginTop() + flexItem.getMarginBottom() +
                        mFlexContainer.getDecorationLengthCrossAxis(child));
                flexLine.mMainSize += childMeasuredWidth + flexItem.getMarginLeft()
                        + flexItem.getMarginRight();
            } else {
                int childMeasuredHeight = child.getMeasuredHeight();
                if (mMeasuredSizeCache != null) {
                    childMeasuredHeight =
                            extractHigherInt(mMeasuredSizeCache[index]);
                }
                int childMeasuredWidth = child.getMeasuredWidth();
                if (mMeasuredSizeCache != null) {
                    childMeasuredWidth =
                            extractLowerInt(mMeasuredSizeCache[index]);
                }
                if (!mChildrenFrozen[index] && flexItem.getFlexShrink() > 0f) {
                    float rawCalculatedHeight = childMeasuredHeight
                            - unitShrink * flexItem.getFlexShrink();
                    if (i == flexLine.mItemCount - 1) {
                        rawCalculatedHeight += accumulatedRoundError;
                        accumulatedRoundError = 0;
                    }
                    int newHeight = Math.round(rawCalculatedHeight);
                    if (newHeight < flexItem.getMinHeight()) {
                        needsReshrink = true;
                        newHeight = flexItem.getMinHeight();
                        mChildrenFrozen[index] = true;
                        flexLine.mTotalFlexShrink -= flexItem.getFlexShrink();
                    } else {
                        accumulatedRoundError += (rawCalculatedHeight - newHeight);
                        if (accumulatedRoundError > 1.0) {
                            newHeight += 1;
                            accumulatedRoundError -= 1;
                        } else if (accumulatedRoundError < -1.0) {
                            newHeight -= 1;
                            accumulatedRoundError += 1;
                        }
                    }
                    int childWidthMeasureSpec = getChildWidthMeasureSpecInternal(widthMeasureSpec,
                            flexItem, flexLine.mSumCrossSizeBefore);
                    int childHeightMeasureSpec =
                            View.MeasureSpec.makeMeasureSpec(newHeight, View.MeasureSpec.EXACTLY);
                    child.measure(childWidthMeasureSpec, childHeightMeasureSpec);

                    childMeasuredWidth = child.getMeasuredWidth();
                    childMeasuredHeight = child.getMeasuredHeight();
                    updateMeasureCache(index, childWidthMeasureSpec, childHeightMeasureSpec,
                            child);
                    mFlexContainer.updateViewCache(index, child);
                }
                largestCrossSize = Math.max(largestCrossSize, childMeasuredWidth +
                        flexItem.getMarginLeft() + flexItem.getMarginRight() +
                        mFlexContainer.getDecorationLengthCrossAxis(child));
                flexLine.mMainSize += childMeasuredHeight + flexItem.getMarginTop()
                        + flexItem.getMarginBottom();
            }
            flexLine.mCrossSize = Math.max(flexLine.mCrossSize, largestCrossSize);
        }

        if (needsReshrink && sizeBeforeShrink != flexLine.mMainSize) {
            shrinkFlexItems(widthMeasureSpec, heightMeasureSpec, flexLine,
                    maxMainSize, paddingAlongMainAxis, true);
        }
    }

    private int getChildWidthMeasureSpecInternal(int widthMeasureSpec, FlexItem flexItem,
                                                 int padding) {
        int childWidthMeasureSpec = mFlexContainer.getChildWidthMeasureSpec(widthMeasureSpec,
                mFlexContainer.getPaddingLeft() + mFlexContainer.getPaddingRight() +
                        flexItem.getMarginLeft() + flexItem.getMarginRight() + padding,
                flexItem.getWidth());
        int childWidth = View.MeasureSpec.getSize(childWidthMeasureSpec);
        if (childWidth > flexItem.getMaxWidth()) {
            childWidthMeasureSpec = View.MeasureSpec.makeMeasureSpec(flexItem.getMaxWidth(),
                    View.MeasureSpec.getMode(childWidthMeasureSpec));
        } else if (childWidth < flexItem.getMinWidth()) {
            childWidthMeasureSpec = View.MeasureSpec.makeMeasureSpec(flexItem.getMinWidth(),
                    View.MeasureSpec.getMode(childWidthMeasureSpec));
        }
        return childWidthMeasureSpec;
    }

    private int getChildHeightMeasureSpecInternal(int heightMeasureSpec, FlexItem flexItem,
                                                  int padding) {
        int childHeightMeasureSpec = mFlexContainer.getChildHeightMeasureSpec(heightMeasureSpec,
                mFlexContainer.getPaddingTop() + mFlexContainer.getPaddingBottom()
                        + flexItem.getMarginTop() + flexItem.getMarginBottom() + padding,
                flexItem.getHeight());
        int childHeight = View.MeasureSpec.getSize(childHeightMeasureSpec);
        if (childHeight > flexItem.getMaxHeight()) {
            childHeightMeasureSpec = View.MeasureSpec.makeMeasureSpec(flexItem.getMaxHeight(),
                    View.MeasureSpec.getMode(childHeightMeasureSpec));
        } else if (childHeight < flexItem.getMinHeight()) {
            childHeightMeasureSpec = View.MeasureSpec.makeMeasureSpec(flexItem.getMinHeight(),
                    View.MeasureSpec.getMode(childHeightMeasureSpec));
        }
        return childHeightMeasureSpec;
    }

    void determineCrossSize(int widthMeasureSpec, int heightMeasureSpec,
                            int paddingAlongCrossAxis) {
        int mode;
        int size;
        int flexDirection = mFlexContainer.getFlexDirection();
        switch (flexDirection) {
            case FlexDirection.ROW: // Intentional fall through
            case FlexDirection.ROW_REVERSE:
                mode = View.MeasureSpec.getMode(heightMeasureSpec);
                size = View.MeasureSpec.getSize(heightMeasureSpec);
                break;
            case FlexDirection.COLUMN: // Intentional fall through
            case FlexDirection.COLUMN_REVERSE:
                mode = View.MeasureSpec.getMode(widthMeasureSpec);
                size = View.MeasureSpec.getSize(widthMeasureSpec);
                break;
            default:
                throw new IllegalArgumentException("Invalid flex direction: " + flexDirection);
        }
        List<FlexLine> flexLines = mFlexContainer.getFlexLinesInternal();
        if (mode == View.MeasureSpec.EXACTLY) {
            int totalCrossSize = mFlexContainer.getSumOfCrossSize() + paddingAlongCrossAxis;
            if (flexLines.size() == 1) {
                flexLines.get(0).mCrossSize = size - paddingAlongCrossAxis;
            } else if (flexLines.size() >= 2) {
                switch (mFlexContainer.getAlignContent()) {
                    case AlignContent.STRETCH: {
                        if (totalCrossSize >= size) {
                            break;
                        }
                        float freeSpaceUnit = (size - totalCrossSize) / (float) flexLines.size();
                        float accumulatedError = 0;
                        for (int i = 0, flexLinesSize = flexLines.size(); i < flexLinesSize; i++) {
                            FlexLine flexLine = flexLines.get(i);
                            float newCrossSizeAsFloat = flexLine.mCrossSize + freeSpaceUnit;
                            if (i == flexLines.size() - 1) {
                                newCrossSizeAsFloat += accumulatedError;
                                accumulatedError = 0;
                            }
                            int newCrossSize = Math.round(newCrossSizeAsFloat);
                            accumulatedError += (newCrossSizeAsFloat - newCrossSize);
                            if (accumulatedError > 1) {
                                newCrossSize += 1;
                                accumulatedError -= 1;
                            } else if (accumulatedError < -1) {
                                newCrossSize -= 1;
                                accumulatedError += 1;
                            }
                            flexLine.mCrossSize = newCrossSize;
                        }
                        break;
                    }
                    case AlignContent.SPACE_AROUND: {
                        if (totalCrossSize >= size) {
                            mFlexContainer.setFlexLines(
                                    constructFlexLinesForAlignContentCenter(flexLines, size,
                                            totalCrossSize));
                            break;
                        }
                        int spaceTopAndBottom = size - totalCrossSize;
                        int numberOfSpaces = flexLines.size() * 2;
                        spaceTopAndBottom = spaceTopAndBottom / numberOfSpaces;
                        List<FlexLine> newFlexLines = new ArrayList<>();
                        FlexLine dummySpaceFlexLine = new FlexLine();
                        dummySpaceFlexLine.mCrossSize = spaceTopAndBottom;
                        for (FlexLine flexLine : flexLines) {
                            newFlexLines.add(dummySpaceFlexLine);
                            newFlexLines.add(flexLine);
                            newFlexLines.add(dummySpaceFlexLine);
                        }
                        mFlexContainer.setFlexLines(newFlexLines);
                        break;
                    }
                    case AlignContent.SPACE_BETWEEN: {
                        if (totalCrossSize >= size) {
                            break;
                        }
                        float spaceBetweenFlexLine = size - totalCrossSize;
                        int numberOfSpaces = flexLines.size() - 1;
                        spaceBetweenFlexLine = spaceBetweenFlexLine / (float) numberOfSpaces;
                        float accumulatedError = 0;
                        List<FlexLine> newFlexLines = new ArrayList<>();
                        for (int i = 0, flexLineSize = flexLines.size(); i < flexLineSize; i++) {
                            FlexLine flexLine = flexLines.get(i);
                            newFlexLines.add(flexLine);

                            if (i != flexLines.size() - 1) {
                                FlexLine dummySpaceFlexLine = new FlexLine();
                                if (i == flexLines.size() - 2) {
                                    dummySpaceFlexLine.mCrossSize = Math
                                            .round(spaceBetweenFlexLine + accumulatedError);
                                    accumulatedError = 0;
                                } else {
                                    dummySpaceFlexLine.mCrossSize = Math
                                            .round(spaceBetweenFlexLine);
                                }
                                accumulatedError += (spaceBetweenFlexLine
                                        - dummySpaceFlexLine.mCrossSize);
                                if (accumulatedError > 1) {
                                    dummySpaceFlexLine.mCrossSize += 1;
                                    accumulatedError -= 1;
                                } else if (accumulatedError < -1) {
                                    dummySpaceFlexLine.mCrossSize -= 1;
                                    accumulatedError += 1;
                                }
                                newFlexLines.add(dummySpaceFlexLine);
                            }
                        }
                        mFlexContainer.setFlexLines(newFlexLines);
                        break;
                    }
                    case AlignContent.CENTER: {
                        mFlexContainer.setFlexLines(
                                constructFlexLinesForAlignContentCenter(flexLines, size,
                                        totalCrossSize));
                        break;
                    }
                    case AlignContent.FLEX_END: {
                        int spaceTop = size - totalCrossSize;
                        FlexLine dummySpaceFlexLine = new FlexLine();
                        dummySpaceFlexLine.mCrossSize = spaceTop;
                        flexLines.add(0, dummySpaceFlexLine);
                        break;
                    }
                    case AlignContent.FLEX_START:
                        break;
                }
            }
        }
    }

    private List<FlexLine> constructFlexLinesForAlignContentCenter(List<FlexLine> flexLines,
                                                                                              int size, int totalCrossSize) {
        int spaceAboveAndBottom = size - totalCrossSize;
        spaceAboveAndBottom = spaceAboveAndBottom / 2;
        List<FlexLine> newFlexLines = new ArrayList<>();
        FlexLine dummySpaceFlexLine = new FlexLine();
        dummySpaceFlexLine.mCrossSize = spaceAboveAndBottom;
        for (int i = 0, flexLineSize = flexLines.size(); i < flexLineSize; i++) {
            if (i == 0) {
                newFlexLines.add(dummySpaceFlexLine);
            }
            FlexLine flexLine = flexLines.get(i);
            newFlexLines.add(flexLine);
            if (i == flexLines.size() - 1) {
                newFlexLines.add(dummySpaceFlexLine);
            }
        }
        return newFlexLines;
    }

    void stretchViews() {
        stretchViews(0);
    }
    void stretchViews(int fromIndex) {
        if (fromIndex >= mFlexContainer.getFlexItemCount()) {
            return;
        }
        int flexDirection = mFlexContainer.getFlexDirection();
        if (mFlexContainer.getAlignItems() == AlignItems.STRETCH) {
            int flexLineIndex = 0;
            if (mIndexToFlexLine != null) {
                flexLineIndex = mIndexToFlexLine[fromIndex];
            }
            List<FlexLine> flexLines = mFlexContainer.getFlexLinesInternal();
            for (int i = flexLineIndex, size = flexLines.size(); i < size; i++) {
                FlexLine flexLine = flexLines.get(i);
                for (int j = 0, itemCount = flexLine.mItemCount; j < itemCount; j++) {
                    int viewIndex = flexLine.mFirstIndex + j;
                    if (j >= mFlexContainer.getFlexItemCount()) {
                        continue;
                    }
                    View view = mFlexContainer.getReorderedFlexItemAt(viewIndex);
                    if (view == null || view.getVisibility() == View.GONE) {
                        continue;
                    }
                    FlexItem flexItem = (FlexItem) view.getLayoutParams();
                    if (flexItem.getAlignSelf() != AlignSelf.AUTO &&
                            flexItem.getAlignSelf() != AlignItems.STRETCH) {
                        continue;
                    }
                    switch (flexDirection) {
                        case FlexDirection.ROW: // Intentional fall through
                        case FlexDirection.ROW_REVERSE:
                            stretchViewVertically(view, flexLine.mCrossSize, viewIndex);
                            break;
                        case FlexDirection.COLUMN:
                        case FlexDirection.COLUMN_REVERSE:
                            stretchViewHorizontally(view, flexLine.mCrossSize, viewIndex);
                            break;
                        default:
                            throw new IllegalArgumentException(
                                    "Invalid flex direction: " + flexDirection);
                    }
                }
            }
        } else {
            for (FlexLine flexLine : mFlexContainer.getFlexLinesInternal()) {
                for (Integer index : flexLine.mIndicesAlignSelfStretch) {
                    View view = mFlexContainer.getReorderedFlexItemAt(index);
                    switch (flexDirection) {
                        case FlexDirection.ROW: // Intentional fall through
                        case FlexDirection.ROW_REVERSE:
                            stretchViewVertically(view, flexLine.mCrossSize, index);
                            break;
                        case FlexDirection.COLUMN:
                        case FlexDirection.COLUMN_REVERSE:
                            stretchViewHorizontally(view, flexLine.mCrossSize, index);
                            break;
                        default:
                            throw new IllegalArgumentException(
                                    "Invalid flex direction: " + flexDirection);
                    }
                }
            }
        }
    }
    private void stretchViewVertically(View view, int crossSize, int index) {
        FlexItem flexItem = (FlexItem) view.getLayoutParams();
        int newHeight = crossSize - flexItem.getMarginTop() - flexItem.getMarginBottom() -
                mFlexContainer.getDecorationLengthCrossAxis(view);
        newHeight = Math.max(newHeight, flexItem.getMinHeight());
        newHeight = Math.min(newHeight, flexItem.getMaxHeight());
        int childWidthSpec;
        int measuredWidth;
        if (mMeasuredSizeCache != null) {
            measuredWidth = extractLowerInt(mMeasuredSizeCache[index]);
        } else {
            measuredWidth = view.getMeasuredWidth();
        }
        childWidthSpec = View.MeasureSpec.makeMeasureSpec(measuredWidth,
                View.MeasureSpec.EXACTLY);

        int childHeightSpec = View.MeasureSpec.makeMeasureSpec(newHeight, View.MeasureSpec.EXACTLY);
        view.measure(childWidthSpec, childHeightSpec);

        updateMeasureCache(index, childWidthSpec, childHeightSpec, view);
        mFlexContainer.updateViewCache(index, view);
    }

    private void stretchViewHorizontally(View view, int crossSize, int index) {
        FlexItem flexItem = (FlexItem) view.getLayoutParams();
        int newWidth = crossSize - flexItem.getMarginLeft() - flexItem.getMarginRight()
                - mFlexContainer.getDecorationLengthCrossAxis(view);
        newWidth = Math.max(newWidth, flexItem.getMinWidth());
        newWidth = Math.min(newWidth, flexItem.getMaxWidth());
        int childHeightSpec;
        int measuredHeight;
        if (mMeasuredSizeCache != null) {
            measuredHeight = extractHigherInt(mMeasuredSizeCache[index]);
        } else {
            measuredHeight = view.getMeasuredHeight();
        }
        childHeightSpec = View.MeasureSpec.makeMeasureSpec(measuredHeight,
                View.MeasureSpec.EXACTLY);
        int childWidthSpec = View.MeasureSpec.makeMeasureSpec(newWidth, View.MeasureSpec.EXACTLY);
        view.measure(childWidthSpec, childHeightSpec);

        updateMeasureCache(index, childWidthSpec, childHeightSpec, view);
        mFlexContainer.updateViewCache(index, view);
    }
    void layoutSingleChildHorizontal(View view, FlexLine flexLine, int left, int top, int right,
                                     int bottom) {
        FlexItem flexItem = (FlexItem) view.getLayoutParams();
        int alignItems = mFlexContainer.getAlignItems();
        if (flexItem.getAlignSelf() != AlignSelf.AUTO) {
            alignItems = flexItem.getAlignSelf();
        }
        int crossSize = flexLine.mCrossSize;
        switch (alignItems) {
            case AlignItems.FLEX_START: // Intentional fall through
            case AlignItems.STRETCH:
                if (mFlexContainer.getFlexWrap() != FlexWrap.WRAP_REVERSE) {
                    view.layout(left, top + flexItem.getMarginTop(), right,
                            bottom + flexItem.getMarginTop());
                } else {
                    view.layout(left, top - flexItem.getMarginBottom(), right,
                            bottom - flexItem.getMarginBottom());
                }
                break;
            case AlignItems.BASELINE:
                if (mFlexContainer.getFlexWrap() != FlexWrap.WRAP_REVERSE) {
                    int marginTop = flexLine.mMaxBaseline - view.getBaseline();
                    marginTop = Math.max(marginTop, flexItem.getMarginTop());
                    view.layout(left, top + marginTop, right, bottom + marginTop);
                } else {
                    int marginBottom = flexLine.mMaxBaseline - view.getMeasuredHeight() + view
                            .getBaseline();
                    marginBottom = Math.max(marginBottom, flexItem.getMarginBottom());
                    view.layout(left, top - marginBottom, right, bottom - marginBottom);
                }
                break;
            case AlignItems.FLEX_END:
                if (mFlexContainer.getFlexWrap() != FlexWrap.WRAP_REVERSE) {
                    view.layout(left,
                            top + crossSize - view.getMeasuredHeight() - flexItem.getMarginBottom(),
                            right, top + crossSize - flexItem.getMarginBottom());
                } else {
                    view.layout(left,
                            top - crossSize + view.getMeasuredHeight() + flexItem.getMarginTop(),
                            right, bottom - crossSize + view.getMeasuredHeight() + flexItem
                                    .getMarginTop());
                }
                break;
            case AlignItems.CENTER:
                int topFromCrossAxis = (crossSize - view.getMeasuredHeight()
                        + flexItem.getMarginTop() - flexItem.getMarginBottom()) / 2;
                if (mFlexContainer.getFlexWrap() != FlexWrap.WRAP_REVERSE) {
                    view.layout(left, top + topFromCrossAxis,
                            right, top + topFromCrossAxis + view.getMeasuredHeight());
                } else {
                    view.layout(left, top - topFromCrossAxis,
                            right, top - topFromCrossAxis + view.getMeasuredHeight());
                }
                break;
        }
    }
    void layoutSingleChildVertical(View view, FlexLine flexLine, boolean isRtl,
                                   int left, int top, int right, int bottom) {
        FlexItem flexItem = (FlexItem) view.getLayoutParams();
        int alignItems = mFlexContainer.getAlignItems();
        if (flexItem.getAlignSelf() != AlignSelf.AUTO) {
            alignItems = flexItem.getAlignSelf();
        }
        int crossSize = flexLine.mCrossSize;
        switch (alignItems) {
            case AlignItems.FLEX_START: // Intentional fall through
            case AlignItems.STRETCH: // Intentional fall through
            case AlignItems.BASELINE:
                if (!isRtl) {
                    view.layout(left + flexItem.getMarginLeft(), top,
                            right + flexItem.getMarginLeft(), bottom);
                } else {
                    view.layout(left - flexItem.getMarginRight(), top,
                            right - flexItem.getMarginRight(), bottom);
                }
                break;
            case AlignItems.FLEX_END:
                if (!isRtl) {
                    view.layout(
                            left + crossSize - view.getMeasuredWidth() - flexItem.getMarginRight(),
                            top,
                            right + crossSize - view.getMeasuredWidth() - flexItem.getMarginRight(),
                            bottom);
                } else {
                    view.layout(
                            left - crossSize + view.getMeasuredWidth() + flexItem.getMarginLeft(),
                            top,
                            right - crossSize + view.getMeasuredWidth() + flexItem.getMarginLeft(),
                            bottom);
                }
                break;
            case AlignItems.CENTER:
                ViewGroup.MarginLayoutParams lp = (ViewGroup.MarginLayoutParams)
                        view.getLayoutParams();
                int leftFromCrossAxis = (crossSize - view.getMeasuredWidth()
                        + MarginLayoutParamsCompat.getMarginStart(lp)
                        - MarginLayoutParamsCompat.getMarginEnd(lp)) / 2;
                if (!isRtl) {
                    view.layout(left + leftFromCrossAxis, top, right + leftFromCrossAxis, bottom);
                } else {
                    view.layout(left - leftFromCrossAxis, top, right - leftFromCrossAxis, bottom);
                }
                break;
        }
    }

    void ensureMeasuredSizeCache(int size) {
        if (mMeasuredSizeCache == null) {
            mMeasuredSizeCache = new long[size < INITIAL_CAPACITY ? INITIAL_CAPACITY : size];
        } else if (mMeasuredSizeCache.length < size) {
            int newCapacity = mMeasuredSizeCache.length * 2;
            newCapacity = newCapacity >= size ? newCapacity : size;
            mMeasuredSizeCache = Arrays.copyOf(mMeasuredSizeCache, newCapacity);
        }
    }

    void ensureMeasureSpecCache(int size) {
        if (mMeasureSpecCache == null) {
            mMeasureSpecCache = new long[size < INITIAL_CAPACITY ? INITIAL_CAPACITY : size];
        } else if (mMeasureSpecCache.length < size) {
            int newCapacity = mMeasureSpecCache.length * 2;
            newCapacity = newCapacity >= size ? newCapacity : size;
            mMeasureSpecCache = Arrays.copyOf(mMeasureSpecCache, newCapacity);
        }
    }
    int extractLowerInt(long longValue) {
        return (int) longValue;
    }

    int extractHigherInt(long longValue) {
        return (int) (longValue >> 32);
    }
    @VisibleForTesting
    long makeCombinedLong(int widthMeasureSpec, int heightMeasureSpec) {
        return (long) heightMeasureSpec << 32 | (long) widthMeasureSpec & MEASURE_SPEC_WIDTH_MASK;
    }

    private void updateMeasureCache(int index, int widthMeasureSpec, int heightMeasureSpec,
                                    View view) {
        if (mMeasureSpecCache != null) {
            mMeasureSpecCache[index] = makeCombinedLong(
                    widthMeasureSpec,
                    heightMeasureSpec);
        }
        if (mMeasuredSizeCache != null) {
            mMeasuredSizeCache[index] = makeCombinedLong(
                    view.getMeasuredWidth(),
                    view.getMeasuredHeight());
        }
    }

    void ensureIndexToFlexLine(int size) {
        if (mIndexToFlexLine == null) {
            mIndexToFlexLine = new int[size < INITIAL_CAPACITY ? INITIAL_CAPACITY : size];
        } else if (mIndexToFlexLine.length < size) {
            int newCapacity = mIndexToFlexLine.length * 2;
            newCapacity = newCapacity >= size ? newCapacity : size;
            mIndexToFlexLine = Arrays.copyOf(mIndexToFlexLine, newCapacity);
        }
    }
    void clearFlexLines(List<FlexLine> flexLines, int fromFlexItem) {
        assert mIndexToFlexLine != null;
        assert mMeasureSpecCache != null;

        int fromFlexLine = mIndexToFlexLine[fromFlexItem];
        if (fromFlexLine == NO_POSITION) {
            fromFlexLine = 0;
        }
        for (int i = flexLines.size() - 1; i >= fromFlexLine; i--) {
            flexLines.remove(i);
        }

        int fillTo = mIndexToFlexLine.length - 1;
        if (fromFlexItem > fillTo) {
            Arrays.fill(mIndexToFlexLine, NO_POSITION);
        } else {
            Arrays.fill(mIndexToFlexLine, fromFlexItem, fillTo, NO_POSITION);
        }

        fillTo = mMeasureSpecCache.length - 1;
        if (fromFlexItem > fillTo) {
            Arrays.fill(mMeasureSpecCache, 0);
        } else {
            Arrays.fill(mMeasureSpecCache, fromFlexItem, fillTo, 0);
        }
    }
    private static class Order implements Comparable<FlexboxHelper.Order> {
        int index;
        int order;

        @Override
        public int compareTo(@NonNull FlexboxHelper.Order another) {
            if (order != another.order) {
                return order - another.order;
            }
            return index - another.index;
        }

        @Override
        public String toString() {
            return "Order{" +
                    "order=" + order +
                    ", index=" + index +
                    '}';
        }
    }

    static class FlexLinesResult {

        List<FlexLine> mFlexLines;

        int mChildState;

        void reset() {
            mFlexLines = null;
            mChildState = 0;
        }
    }
}
