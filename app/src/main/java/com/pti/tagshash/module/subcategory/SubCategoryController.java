package com.pti.tagshash.module.subcategory;

import android.content.Context;

import com.pti.tagshash.model.category.CategoryContainer;
import com.pti.tagshash.model.sub_category.SubCategoryContainer;
import com.pti.tagshash.network.ApiClient;
import com.pti.tagshash.network.ApiInterface;
import com.pti.tagshash.utils.Constant;
import com.pti.tagshash.utils.Utility;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SubCategoryController {
    private Presenter presenter;
    Context mContext;
    ApiInterface apiInterface;

    public SubCategoryController(Context context, Presenter pr) {
        presenter = pr;
        mContext = context;
        apiInterface = ApiClient.createService(ApiInterface.class);
    }

    public void getHashTag(String cat_id,String sub_cat_id,String keyword) {
        Map<String, String> data = new HashMap<>();

        data.put(Constant.cat_id, cat_id);
        data.put(Constant.sub_cat_id, sub_cat_id);
        data.put(Constant.keyword, keyword);
        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json"), (new JSONObject(data)).toString());


        Call<SubCategoryContainer> call = apiInterface.getCategory_hashtag(body);
        call.enqueue(new Callback<SubCategoryContainer>() {
            @Override
            public void onResponse(Call<SubCategoryContainer> call, Response<SubCategoryContainer> response) {
                if (response.code() == 200) {
                    if (response.body().getStatus()==null){
                        presenter.onError(response.body().getMessage());
                    }else {
                        if (response.body().getStatus().equals(Constant.code201)) {
                            presenter.getSuccessSubCategory(response.body());
                        } else {
                            presenter.onError(response.body().getMessage());
                        }
                    }
                }else {
                    presenter.onError(Utility.getError(response.errorBody()));
                }
            }

            @Override
            public void onFailure(Call<SubCategoryContainer> call, Throwable t) {
                presenter.onError(t.toString());
            }
        });
    }

    interface Presenter {
        void onError(String toString);

        void getSuccessSubCategory(SubCategoryContainer body);

    }
}
