package com.pti.tagshash.utils;

import android.Manifest;

public class Constant {
    public static final String NOTIFICATION_CHANNEL_ID = "10001" ;
    private final static String default_notification_channel_id = "default" ;

    public static String[] PERMITION_GENERAL = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE};
    public static final int PERMISSIONS_GENERAL = 2106;
    public static String[] PERMISSIONS_STORAGE = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.MANAGE_DOCUMENTS};
    public static String[] PERMISSIONS_WRITE_CAMERA_EXTERNAL = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE};
    public static final int PERMISSIONS_REQUEST_STORAGE = 13;
    public static final int PERMISSIONS_REQUEST_CAMERA = 12;
    public static final int CAMERA_IMAGE_REQUEST = 20010;
    public static final int GALLERY_IMAGE_REQUEST = 2002;
   public static final String IMAGE_DIRECTORY_NAME = "Quuikfoodee";



    public static final String ENGLISH = "en";
    public static final String BAHASA = "BS";
    public static final String CHINESE = "zh";


    public static final int verticalSpace18 = 18;
    public static final int verticalSpace5 = 5;
    public static final int verticalSpace10 = 10;
    public static final int adsTime = 30;//second

    public static String code201="201";
    public static String Category_ImagePath="";
    public static String categoryData="categoryData";
    public static String cat_id="cat_id";
    public static String sub_cat_id="sub_cat_id";
    public static String keyword="keyword";
}
