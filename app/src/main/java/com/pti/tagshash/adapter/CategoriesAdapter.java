package com.pti.tagshash.adapter;

import android.content.Context;
import android.content.Intent;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.RequestConfiguration;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.pti.tagshash.R;
import com.pti.tagshash.activity.TagsListActivity;
import com.pti.tagshash.model.category.CatData;
import com.pti.tagshash.utils.Constant;
import com.pti.tagshash.utils.Log;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CategoriesAdapter extends RecyclerView.Adapter<CategoriesAdapter.GridViewHolder> implements Filterable {


    private List<CatData> mList;
    private List<CatData> mSearchList;
    private Context mContext;


    public CategoriesAdapter(Context context, List<CatData> list) {
        mList = list;
        mSearchList = list;
        mContext = context;
    }

    @Override
    public GridViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        GridViewHolder holder;
        View mainview = LayoutInflater.from(mContext).inflate(R.layout.categories_row, null);
        holder = new GridViewHolder(mainview);
        return holder;
    }

    @Override
    public void onBindViewHolder(GridViewHolder holder, int position) {
        holder.setIsRecyclable(false);
        Log.e("onBindViewHolder", mList.size() + "");
        CatData data = mList.get(position);
        holder.txt_tag.setText(data.getCatName());
        /*if ((position % 3) == 0) {
            MobileAds.initialize(mContext,mContext.getResources().getString(R.string.app_id));
            List<String> testDevices = new ArrayList<>();
            testDevices.add(AdRequest.DEVICE_ID_EMULATOR);

            String android_id = Settings.Secure.getString(mContext.getContentResolver(),
                    Settings.Secure.ANDROID_ID);
            List<String> testDevices1 = new ArrayList<>();
            testDevices1.add(android_id);
            RequestConfiguration requestConfiguration
                    = new RequestConfiguration.Builder()
                    .setTestDeviceIds(testDevices)
                    .setTestDeviceIds(testDevices1)
                    .build();
            MobileAds.setRequestConfiguration(requestConfiguration);
            holder.adView.loadAd(new AdRequest.Builder().build());




//            String android_id = Settings.Secure.getString(mContext.getContentResolver(),
//                    Settings.Secure.ANDROID_ID);
//            AdRequest request = new AdRequest.Builder()
//                    .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
//                    .addTestDevice(android_id)
//                    .build();
//            holder.adView.loadAd(request);


            holder.adView.setAdListener(new AdListener() {
                @Override
                public void onAdLoaded() {
                    Log.e("onAdLoaded", "onAdLoaded");
                    holder.ads_cardview.setVisibility(View.VISIBLE);
                }

                @Override
                public void onAdClosed() {
                    super.onAdClosed();
                    Log.e("onAdClosed", "onAdClosed");
                }

                @Override
                public void onAdFailedToLoad(int errorCode) {
                    Log.e("onAdFailedToLoad", errorCode + "");
                }

                @Override
                public void onAdLeftApplication() {
                    Log.e("onAdLeftApplication", "onAdLeftApplication");
                }

                @Override
                public void onAdOpened() {
                    super.onAdOpened();
                    Log.e("onAdOpened", "onAdOpened");
                }
            });
        }*/


        Glide.with(mContext).load(Constant.Category_ImagePath + data.getCatImage()).into(holder.image);
        holder.layout_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, TagsListActivity.class);
                intent.putExtra(Constant.categoryData, data);
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class GridViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.layout_main)
        LinearLayout layout_main;
        @BindView(R.id.txt_tag)
        TextView txt_tag;
        @BindView(R.id.image)
        ImageView image;
        @BindView(R.id.ads_cardview)
        CardView ads_cardview;
        @BindView(R.id.adView)
        AdView adView;

        public GridViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    mList = mSearchList;
                } else {
                    List<CatData> filteredList = new ArrayList<>();
                    for (CatData row : mSearchList) {
                        if (row.getCatName().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }
                    mList = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = mList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                mList = (ArrayList<CatData>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }
}
