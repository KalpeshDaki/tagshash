package com.pti.tagshash.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.pti.tagshash.R;
import com.pti.tagshash.adapter.CategoriesAdapter;
import com.pti.tagshash.model.category.CatData;
import com.pti.tagshash.model.category.CategoryContainer;
import com.pti.tagshash.model.sub_category.SubCategoryContainer;
import com.pti.tagshash.model.sub_category.SubCategoryList;
import com.pti.tagshash.module.category.CategoryPresenterImpl;
import com.pti.tagshash.module.category.CategoryView;
import com.pti.tagshash.module.subcategory.SubCategoryPresenterImpl;
import com.pti.tagshash.module.subcategory.SubCategoryView;
import com.pti.tagshash.preference.AppPreference;
import com.pti.tagshash.utils.Constant;
import com.pti.tagshash.utils.CustomeTagLayout;
import com.pti.tagshash.utils.Log;
import com.pti.tagshash.utils.Utility;
import com.pti.tagshash.utils.VerticalSpaceItemDecoration;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Dashboard extends AppCompatActivity implements SubCategoryView,CategoryView, View.OnClickListener {
    Context mContext;
    @BindView(R.id.edt_search)
    EditText edt_search;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.search_click_layout)
    LinearLayout search_click_layout;
    @BindView(R.id.hashtag_click_layout)
    LinearLayout hashtag_click_layout;


    //for search tag
    @BindView(R.id.search_view)
    LinearLayout search_view;
    @BindView(R.id.hashtag_view)
    LinearLayout hashtag_view;
    @BindView(R.id.hash_search)
    EditText hash_search;
    @BindView(R.id.taglayout)
    CustomeTagLayout taglayout;

    CategoriesAdapter mAdapter;
    List<CatData> mList;
    CategoryPresenterImpl presenter;
    SubCategoryPresenterImpl subPresenter;
    List<SubCategoryList> taglist;
    LayoutInflater layoutInflater;

    ProgressDialog progressDialog;
    private InterstitialAd interstitial;

    boolean isOpenHashView=true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        ButterKnife.bind(this);
        mContext = Dashboard.this;
        AppPreference.setFirstTimeLaunch(mContext, true);
        setUpLayoutManager();
        presenter = new CategoryPresenterImpl(mContext, this);
        subPresenter = new SubCategoryPresenterImpl(mContext, this);
        getTagCategory();

        /*ScheduledExecutorService scheduler =
                Executors.newSingleThreadScheduledExecutor();
        scheduler.scheduleAtFixedRate(new Runnable() {
            public void run() {
                runOnUiThread(new Runnable() {
                    public void run() {
                        if (interstitial == null) {
                            showAdsView();
                        } else {
                            if (interstitial.isLoaded()) {
                                interstitial.show();
                                Log.e("isLoaded", "isLoaded");
                            } else {
                                Log.e("isLoaded", "else");
                            }
                            showAdsView();
                        }
                    }
                });
            }
        }, Constant.adsTime, Constant.adsTime, TimeUnit.SECONDS);*/
        showAdsView();
    }

    private void showAdsView() {
        MobileAds.initialize(this, getString(R.string.app_id));
        AdRequest adIRequest = new AdRequest.Builder().build();
        interstitial = new InterstitialAd(Dashboard.this);
        interstitial.setAdUnitId(getString(R.string.full_page));
        interstitial.loadAd(adIRequest);
        interstitial.setAdListener(new AdListener() {
            public void onAdLoaded() {
                displayInterstitial();
            }
        });
    }

    public void displayInterstitial() {
        if (interstitial.isLoaded()) {
            interstitial.show();
        }
    }

    private void getTagCategory() {
        if (Utility.isNetworkConnected(mContext)) {
            presenter.getCategory();
        } else {
            Utility.showToast(mContext, mContext.getResources().getString(R.string.no_internet));
        }
    }

    private void setUpLayoutManager() {
        mList = new ArrayList<>();
        taglist = new ArrayList<>();
        recyclerView.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new VerticalSpaceItemDecoration(Constant.verticalSpace5));

        edt_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!s.equals("")) {
                    if (s != null) {
                        if (mAdapter != null) {
                            mAdapter.getFilter().filter(s);
                        }
                    }
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        hash_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length()>0) {
                    if (!s.equals("")) {
                        if (s != null) {
                            getHashTag("", "", s.toString());
                        }
                    }
                }else {
                    removeAllTags();
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    private void removeAllTags() {
        if (taglist != null) {
            taglist.clear();
        }
        if (taglayout.getChildCount() > 0) {
            taglayout.removeAllViews();
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.search_click_layout:
                if (isOpenHashView){
                    isOpenHashView=false;
                    loadview();
                }
                break;
            case R.id.hashtag_click_layout:
                if (isOpenHashView!=true){
                    isOpenHashView=true;
                    loadview();
                }
                break;
        }
    }

    @Override
    public void onBackPressed() {

        if (isOpenHashView){
            finish();
        }else {
            isOpenHashView=true;
            loadview();
        }
    }

    private void loadview() {
        if (isOpenHashView){
            hashtag_view.setVisibility(View.VISIBLE);
            search_view.setVisibility(View.GONE);
            if (mList!=null){
                mList.clear();
            }
            getTagCategory();
        }else {
            hashtag_view.setVisibility(View.GONE);
            search_view.setVisibility(View.VISIBLE);
        }
        hash_search.setText("");
        edt_search.setText("");
    }
    @Override
    public void showDialog() {
        if (progressDialog == null) {
            progressDialog = Utility.createProgressDialog(mContext);
            progressDialog.show();
        } else {
            progressDialog.show();
        }
    }
    @Override
    public void hideDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }
    @Override
    public void onError(String message) {
        Utility.showToast(mContext, message);
    }
    @Override
    public void getSuccessCategory(CategoryContainer body) {
        if (body.getData().size() > 0) {
            if (mList != null) {
                mList.clear();
            }
            mList.addAll(body.getData());
            mAdapter = new CategoriesAdapter(mContext, mList);
            recyclerView.setAdapter(mAdapter);
        }
    }

    private void getHashTag(String catid, String subcatid, String keyword) {
        if (Utility.isNetworkConnected(mContext)) {
            subPresenter.getHashTag(catid, subcatid, keyword);
        } else {
            Utility.showToast(mContext, mContext.getResources().getString(R.string.no_internet));
        }
    }
    @Override
    public void getSuccessSubCategory(SubCategoryContainer body) {
        if (body.getData().size() > 0) {
            if (taglist != null) {
                taglist.clear();
            }
            taglist.addAll(body.getData());
            setTagsView();
        }
    }

    private void setTagsView() {
        layoutInflater = getLayoutInflater();
        if (taglayout.getChildCount() > 0) {
            taglayout.removeAllViews();
        }
        for (int i = 0; i < taglist.size(); i++) {
            View tagView = layoutInflater.inflate(R.layout.tags_raw, null, false);
            final LinearLayout child_view = (LinearLayout) tagView.findViewById(R.id.child_view);
            final TextView tagTextView = (TextView) tagView.findViewById(R.id.tags);
            if (taglist.get(i).isEnable()) {
                child_view.setBackgroundResource(R.drawable.tagsback_select);
                tagTextView.setTextColor(getResources().getColor(R.color.white));
            } else {
                child_view.setBackgroundResource(R.drawable.tagsback);
                tagTextView.setTextColor(getResources().getColor(R.color.blue_light));
            }
            tagTextView.setText(taglist.get(i).getTag_name());
            final int finalI = i;
            child_view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
//                    if (taglist.get(finalI).isEnable()) {
//                        child_view.setBackgroundResource(R.drawable.tagsback);
//                        tagTextView.setTextColor(getResources().getColor(R.color.blue_light));
//                        taglist.get(finalI).setEnable(false);
//                    } else {
//                        child_view.setBackgroundResource(R.drawable.tagsback_select);
//                        tagTextView.setTextColor(getResources().getColor(R.color.white));
//                        taglist.get(finalI).setEnable(true);
//                    }

                }
            });
            taglayout.addView(tagView);

        }
    }
}
