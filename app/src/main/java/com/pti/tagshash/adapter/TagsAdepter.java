package com.pti.tagshash.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.pti.tagshash.R;
import com.pti.tagshash.model.category.SubCategory;
import com.pti.tagshash.utils.Log;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class TagsAdepter extends RecyclerView.Adapter<TagsAdepter.MyViewHolder> {

    private Context context;
    private List<SubCategory> tagsModels;
    int defaulSelection = 0;
    onClickTag onClickTag;
    public TagsAdepter(Context context, List<SubCategory> tagsModels,onClickTag onClick) {
        this.context = context;
        this.tagsModels = tagsModels;
        this.onClickTag = onClick;
        defaulSelection=tagsModels.size()-1;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.tagsraw, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, int position) {

        final SubCategory data = tagsModels.get(position);

        if (position == defaulSelection) {
            if ((position%3)==0){
                holder.child_view.setBackgroundResource(R.drawable.select_a);
            }else if ((position%2)==0){
                holder.child_view.setBackgroundResource(R.drawable.select_b);
            }else {
                holder.child_view.setBackgroundResource(R.drawable.select_c);
            }

            holder.tags.setTextColor(context.getResources().getColor(R.color.white));
        } else {
            holder.child_view.setBackgroundResource(R.drawable.tagsback);
            holder.tags.setTextColor(context.getResources().getColor(R.color.black));
        }


        holder.tags.setText(data.getName().replace("#", ""));

        holder.tags.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                defaulSelection = position;
                notifyDataSetChanged();
                onClickTag.clickTag(data,position);

            }
        });
    }

    @Override
    public int getItemCount() {
        return tagsModels.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tags)
        TextView tags;
        @BindView(R.id.child_view)
        LinearLayout child_view;

        public MyViewHolder(View convertView) {
            super(convertView);
            tags = convertView.findViewById(R.id.tags);
            ButterKnife.bind(this, itemView);
        }
    }
    public interface onClickTag {
        void clickTag(SubCategory data,int pos);
    }
}
