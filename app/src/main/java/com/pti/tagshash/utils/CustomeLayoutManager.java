package com.pti.tagshash.utils;

import android.content.Context;
import android.graphics.PointF;
import android.graphics.Rect;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;

import androidx.core.view.ViewCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.OrientationHelper;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import static androidx.recyclerview.widget.LinearLayoutManager.INVALID_OFFSET;
import static androidx.recyclerview.widget.RecyclerView.NO_POSITION;

public class CustomeLayoutManager extends RecyclerView.LayoutManager implements TagContainer,
        RecyclerView.SmoothScroller.ScrollVectorProvider {

    private static final String TAG = "CustomeLayoutManager";
    private static final Rect TEMP_RECT = new Rect();

    private static final boolean DEBUG = false;

    private int mFlexDirection;
   private int mFlexWrap;
    private int mJustifyContent;
private int mAlignItems;
    private boolean mIsRtl;
    private boolean mFromBottomToTop;

    private List<FlexLine> mFlexLines = new ArrayList<>();

    private final FlexboxHelper mFlexboxHelper = new FlexboxHelper(this);
    private RecyclerView.Recycler mRecycler;
    private RecyclerView.State mState;

    private CustomeLayoutManager.LayoutState mLayoutState;

    private CustomeLayoutManager.AnchorInfo mAnchorInfo = new CustomeLayoutManager.AnchorInfo();

    private OrientationHelper mOrientationHelper;
    private OrientationHelper mSubOrientationHelper;

    private CustomeLayoutManager.SavedState mPendingSavedState;
    private int mPendingScrollPosition = NO_POSITION;
    private int mPendingScrollPositionOffset = INVALID_OFFSET;
    private int mLastWidth = Integer.MIN_VALUE;
    private int mLastHeight = Integer.MIN_VALUE;
    private boolean mRecycleChildrenOnDetach;
    private SparseArray<View> mViewCache = new SparseArray<>();
    private Context mContext;
 private View mParent;
    private int mDirtyPosition = NO_POSITION;
    private FlexboxHelper.FlexLinesResult mFlexLinesResult = new FlexboxHelper.FlexLinesResult();
    public CustomeLayoutManager(Context context) {
        this(context, FlexDirection.ROW, FlexWrap.WRAP);
    }

  public CustomeLayoutManager(Context context, @FlexDirection int flexDirection) {
        this(context, flexDirection, FlexWrap.WRAP);
    }
    public CustomeLayoutManager(Context context, @FlexDirection int flexDirection,
                                @FlexWrap int flexWrap) {
        setFlexDirection(flexDirection);
        setFlexWrap(flexWrap);
        setAlignItems(AlignItems.STRETCH);
        setAutoMeasureEnabled(true);
        mContext = context;
    }

    public CustomeLayoutManager(Context context, AttributeSet attrs, int defStyleAttr,
                                int defStyleRes) {
        Properties properties = getProperties(context, attrs, defStyleAttr, defStyleRes);
        switch (properties.orientation) {
            case LinearLayoutManager.HORIZONTAL:
                if (properties.reverseLayout) {
                    setFlexDirection(FlexDirection.ROW_REVERSE);
                } else {
                    setFlexDirection(FlexDirection.ROW);
                }
                break;
            case LinearLayoutManager.VERTICAL:
                if (properties.reverseLayout) {
                    setFlexDirection(FlexDirection.COLUMN_REVERSE);
                } else {
                    setFlexDirection(FlexDirection.COLUMN);
                }
                break;
        }
        setFlexWrap(FlexWrap.WRAP);
        setAlignItems(AlignItems.STRETCH);
        setAutoMeasureEnabled(true);
        mContext = context;
    }
   @FlexDirection
    @Override
    public int getFlexDirection() {
        return mFlexDirection;
    }

    @Override
    public void setFlexDirection(@FlexDirection int flexDirection) {
        if (mFlexDirection != flexDirection) {
            removeAllViews();
            mFlexDirection = flexDirection;
            mOrientationHelper = null;
            mSubOrientationHelper = null;
            clearFlexLines();
            requestLayout();
        }
    }

    @Override
    @FlexWrap
    public int getFlexWrap() {
        return mFlexWrap;
    }

    @Override
    public void setFlexWrap(@FlexWrap int flexWrap) {
        if (flexWrap == FlexWrap.WRAP_REVERSE) {
            throw new UnsupportedOperationException("wrap_reverse is not supported in "
                    + "CustomeLayoutManager");
        }
        if (mFlexWrap != flexWrap) {
            if (mFlexWrap == FlexWrap.NOWRAP || flexWrap == FlexWrap.NOWRAP) {
                removeAllViews();
                clearFlexLines();
            }
            mFlexWrap = flexWrap;
            mOrientationHelper = null;
            mSubOrientationHelper = null;
            requestLayout();
        }
    }

    @JustifyContent
    @Override
    public int getJustifyContent() {
        return mJustifyContent;
    }

    @Override
    public void setJustifyContent(@JustifyContent int justifyContent) {
        if (mJustifyContent != justifyContent) {
            mJustifyContent = justifyContent;
            requestLayout();
        }
    }

    @AlignItems
    @Override
    public int getAlignItems() {
        return mAlignItems;
    }

    @Override
    public void setAlignItems(@AlignItems int alignItems) {
        if (mAlignItems != alignItems) {
            if (mAlignItems == AlignItems.STRETCH || alignItems == AlignItems.STRETCH) {
                removeAllViews();
                clearFlexLines();
            }
            mAlignItems = alignItems;
            requestLayout();
        }
    }

    @AlignContent
    @Override
    public int getAlignContent() {
        return AlignContent.STRETCH;
    }

    @Override
    public void setAlignContent(@AlignContent int alignContent) {
        throw new UnsupportedOperationException("Setting the alignContent in the "
                + "CustomeLayoutManager is not supported. Use FlexboxLayout "
                + "if you need to use this attribute.");
    }

    @Override
    public List<FlexLine> getFlexLines() {
        List<FlexLine> result = new ArrayList<>(mFlexLines.size());
        for (int i = 0, size = mFlexLines.size(); i < size; i++) {
            FlexLine flexLine = mFlexLines.get(i);
            if (flexLine.getItemCount() == 0) {
                continue;
            }
            result.add(flexLine);
        }
        return result;
    }

    @Override
    public int getDecorationLengthMainAxis(View view, int index, int indexInFlexLine) {
        if (isMainAxisDirectionHorizontal()) {
            return getLeftDecorationWidth(view) + getRightDecorationWidth(view);
        } else {
            return getTopDecorationHeight(view) + getBottomDecorationHeight(view);
        }
    }

    @Override
    public int getDecorationLengthCrossAxis(View view) {
        if (isMainAxisDirectionHorizontal()) {
            return getTopDecorationHeight(view) + getBottomDecorationHeight(view);
        } else {
            return getLeftDecorationWidth(view) + getRightDecorationWidth(view);
        }
    }

    @Override
    public void onNewFlexItemAdded(View view, int index, int indexInFlexLine, FlexLine flexLine) {
        calculateItemDecorationsForChild(view, TEMP_RECT);
        if (isMainAxisDirectionHorizontal()) {
            int decorationWidth = getLeftDecorationWidth(view) + getRightDecorationWidth(view);
            flexLine.mMainSize += decorationWidth;
            flexLine.mDividerLengthInMainSize += decorationWidth;
        } else {
            int decorationHeight = getTopDecorationHeight(view) + getBottomDecorationHeight(view);
            flexLine.mMainSize += decorationHeight;
            flexLine.mDividerLengthInMainSize += decorationHeight;
        }
    }
  @Override
    public int getFlexItemCount() {
        return mState.getItemCount();
    }
    @Override
    public View getFlexItemAt(int index) {
        View cachedView = mViewCache.get(index);
        if (cachedView != null) {
            return cachedView;
        }
        return mRecycler.getViewForPosition(index);
    }
    @Override
    public View getReorderedFlexItemAt(int index) {
        return getFlexItemAt(index);
    }

    @Override
    public void onNewFlexLineAdded(FlexLine flexLine) {
    }

    @Override
    public int getChildWidthMeasureSpec(int widthSpec, int padding, int childDimension) {
        return getChildMeasureSpec(getWidth(), getWidthMode(), padding, childDimension,
                canScrollHorizontally());
    }

    @Override
    public int getChildHeightMeasureSpec(int heightSpec, int padding, int childDimension) {
        return getChildMeasureSpec(getHeight(), getHeightMode(), padding, childDimension,
                canScrollVertically());
    }

    @Override
    public int getLargestMainSize() {
        if (mFlexLines.size() == 0) {
            return 0;
        }
        int largestSize = Integer.MIN_VALUE;
        for (int i = 0, size = mFlexLines.size(); i < size; i++) {
            FlexLine flexLine = mFlexLines.get(i);
            largestSize = Math.max(largestSize, flexLine.mMainSize);
        }
        return largestSize;
    }

    @Override
    public int getSumOfCrossSize() {
        int sum = 0;
        for (int i = 0, size = mFlexLines.size(); i < size; i++) {
            FlexLine flexLine = mFlexLines.get(i);
            sum += flexLine.mCrossSize;
        }
        return sum;
    }

    @Override
    public void setFlexLines(List<FlexLine> flexLines) {
        mFlexLines = flexLines;
    }

    @Override
    public List<FlexLine> getFlexLinesInternal() {
        return mFlexLines;
    }

    @Override
    public void updateViewCache(int position, View view) {
        mViewCache.put(position, view);
    }
    @Override
    public PointF computeScrollVectorForPosition(int targetPosition) {
        if (getChildCount() == 0) {
            return null;
        }
        int firstChildPos = getPosition(getChildAt(0));
        int direction = targetPosition < firstChildPos ? -1 : 1;
        if (isMainAxisDirectionHorizontal()) {
            return new PointF(0, direction);
        } else {
            return new PointF(direction, 0);
        }
    }

    @Override
    public RecyclerView.LayoutParams generateDefaultLayoutParams() {
        return new CustomeLayoutManager.LayoutParams(CustomeLayoutManager.LayoutParams.WRAP_CONTENT, CustomeLayoutManager.LayoutParams.WRAP_CONTENT);
    }

    @Override
    public RecyclerView.LayoutParams generateLayoutParams(Context c, AttributeSet attrs) {
        return new CustomeLayoutManager.LayoutParams(c, attrs);
    }

    @Override
    public boolean checkLayoutParams(RecyclerView.LayoutParams lp) {
        return lp instanceof CustomeLayoutManager.LayoutParams;
    }

    @Override
    public void onAdapterChanged(RecyclerView.Adapter oldAdapter, RecyclerView.Adapter newAdapter) {
        removeAllViews();
    }

    @Override
    public Parcelable onSaveInstanceState() {
        if (mPendingSavedState != null) {
            return new CustomeLayoutManager.SavedState(mPendingSavedState);
        }
        CustomeLayoutManager.SavedState savedState = new CustomeLayoutManager.SavedState();
        if (getChildCount() > 0) {
            View firstView = getChildClosestToStart();
            savedState.mAnchorPosition = getPosition(firstView);
            savedState.mAnchorOffset = mOrientationHelper.getDecoratedStart(firstView) -
                    mOrientationHelper.getStartAfterPadding();
        } else {
            savedState.invalidateAnchor();
        }
        return savedState;
    }

    @Override
    public void onRestoreInstanceState(Parcelable state) {
        if (state instanceof CustomeLayoutManager.SavedState) {
            mPendingSavedState = (CustomeLayoutManager.SavedState) state;
            requestLayout();
            if (DEBUG) {
                Log.d(TAG, "Loaded saved state. " + mPendingSavedState);
            }
        } else {
            if (DEBUG) {
                Log.w(TAG, "Invalid state was trying to be restored. " + state);
            }
        }
    }

    @Override
    public void onItemsAdded(RecyclerView recyclerView, int positionStart, int itemCount) {
        super.onItemsAdded(recyclerView, positionStart, itemCount);
        updateDirtyPosition(positionStart);
    }

    @Override
    public void onItemsUpdated(RecyclerView recyclerView, int positionStart, int itemCount,
                               Object payload) {
        super.onItemsUpdated(recyclerView, positionStart, itemCount, payload);
        updateDirtyPosition(positionStart);
    }

    @Override
    public void onItemsUpdated(RecyclerView recyclerView, int positionStart, int itemCount) {
        super.onItemsUpdated(recyclerView, positionStart, itemCount);
        updateDirtyPosition(positionStart);
    }

    @Override
    public void onItemsRemoved(RecyclerView recyclerView, int positionStart, int itemCount) {
        super.onItemsRemoved(recyclerView, positionStart, itemCount);
        updateDirtyPosition(positionStart);
    }

    @Override
    public void onItemsMoved(RecyclerView recyclerView, int from, int to, int itemCount) {
        super.onItemsMoved(recyclerView, from, to, itemCount);
        updateDirtyPosition(Math.min(from, to));
    }

    private void updateDirtyPosition(int positionStart) {
        int firstVisiblePosition = findFirstVisibleItemPosition();
        int lastVisiblePosition = findLastVisibleItemPosition();
        if (positionStart >= lastVisiblePosition) {
            return;
        }
        int childCount = getChildCount();
        mFlexboxHelper.ensureMeasureSpecCache(childCount);
        mFlexboxHelper.ensureMeasuredSizeCache(childCount);
        mFlexboxHelper.ensureIndexToFlexLine(childCount);
        assert mFlexboxHelper.mIndexToFlexLine != null;

        if (positionStart >= mFlexboxHelper.mIndexToFlexLine.length) {
            return;
        }

        mDirtyPosition = positionStart;

        View firstView = getChildClosestToStart();
        if (firstView == null) {
            return;
        }
        if (firstVisiblePosition <= positionStart && positionStart <= lastVisiblePosition) {
            return;
        }
      mPendingScrollPosition = getPosition(firstView);

        if (!isMainAxisDirectionHorizontal() && mIsRtl) {
            mPendingScrollPositionOffset = mOrientationHelper.getDecoratedEnd(firstView) +
                    mOrientationHelper.getEndPadding();
        } else {
            mPendingScrollPositionOffset = mOrientationHelper.getDecoratedStart(firstView) -
                    mOrientationHelper.getStartAfterPadding();
        }
    }

    @Override
    public void onLayoutChildren(RecyclerView.Recycler recycler, RecyclerView.State state) {
       if (DEBUG) {
            Log.d(TAG, "onLayoutChildren started");
            Log.d(TAG, "getChildCount: " + getChildCount());
           Log.d(TAG, "State: " + state);
            Log.d(TAG, "PendingSavedState: " + mPendingSavedState);
            Log.d(TAG, "PendingScrollPosition: " + mPendingScrollPosition);
            Log.d(TAG, "PendingScrollOffset: " + mPendingScrollPositionOffset);
        }
       mRecycler = recycler;
        mState = state;
        int childCount = state.getItemCount();
        if (childCount == 0 && state.isPreLayout()) {
            return;
        }
        resolveLayoutDirection();
        ensureOrientationHelper();
        ensureLayoutState();
        mFlexboxHelper.ensureMeasureSpecCache(childCount);
        mFlexboxHelper.ensureMeasuredSizeCache(childCount);

        mFlexboxHelper.ensureIndexToFlexLine(childCount);

        mLayoutState.mShouldRecycle = false;

        if (mPendingSavedState != null && mPendingSavedState.hasValidAnchor(childCount)) {
            mPendingScrollPosition = mPendingSavedState.mAnchorPosition;
        }

        if (!mAnchorInfo.mValid || mPendingScrollPosition != NO_POSITION ||
                mPendingSavedState != null) {
            mAnchorInfo.reset();
            updateAnchorInfoForLayout(state, mAnchorInfo);
            mAnchorInfo.mValid = true;
        }
        detachAndScrapAttachedViews(recycler);

        if (mAnchorInfo.mLayoutFromEnd) {
            updateLayoutStateToFillStart(mAnchorInfo, false, true);
        } else {
            updateLayoutStateToFillEnd(mAnchorInfo, false, true);
        }
        if (DEBUG) {
            Log.d(TAG,
                    String.format("onLayoutChildren. recycler.getScrapList.size(): %s, state: %s",
                            recycler.getScrapList().size(), state));
        }

        updateFlexLines(childCount);
        if (DEBUG) {
            for (int i = 0, size = mFlexLines.size(); i < size; i++) {
                FlexLine flexLine = mFlexLines.get(i);
                Log.d(TAG, String.format("%d flex line. MainSize: %d, CrossSize: %d, itemCount: %d",
                        i, flexLine.getMainSize(), flexLine.getCrossSize(),
                        flexLine.getItemCount()));
            }
        }

        int startOffset;
        int endOffset;
        if (mAnchorInfo.mLayoutFromEnd) {
            int filledToEnd = fill(recycler, state, mLayoutState);
            if (DEBUG) {
                Log.d(TAG, String.format("filled: %d toward start", filledToEnd));
            }
            startOffset = mLayoutState.mOffset;
            updateLayoutStateToFillEnd(mAnchorInfo, true, false);
            int filledToStart = fill(recycler, state, mLayoutState);
            if (DEBUG) {
                Log.d(TAG, String.format("filled: %d toward end", filledToStart));
            }
            endOffset = mLayoutState.mOffset;
        } else {
            int filledToEnd = fill(recycler, state, mLayoutState);
            if (DEBUG) {
                Log.d(TAG, String.format("filled: %d toward end", filledToEnd));
            }
            endOffset = mLayoutState.mOffset;
            updateLayoutStateToFillStart(mAnchorInfo, true, false);
            int filledToStart = fill(recycler, state, mLayoutState);
            if (DEBUG) {
                Log.d(TAG, String.format("filled: %d toward start", filledToStart));
            }
            startOffset = mLayoutState.mOffset;
        }

        if (getChildCount() > 0) {
            if (mAnchorInfo.mLayoutFromEnd) {
                int fixOffset = fixLayoutEndGap(endOffset, recycler, state, true);
                startOffset += fixOffset;
                fixLayoutStartGap(startOffset, recycler, state, false);
            } else {
                int fixOffset = fixLayoutStartGap(startOffset, recycler, state, true);
                endOffset += fixOffset;
                fixLayoutEndGap(endOffset, recycler, state, false);
            }
        }
    }

    /**
     * Fill the gap the toward the start position if the gap to be filled is made.
     * Large part is copied from LinearLayoutManager#fixLayoutStartGap.
     */
    private int fixLayoutStartGap(int startOffset, RecyclerView.Recycler recycler,
                                  RecyclerView.State state, boolean canOffsetChildren) {
        int gap;
        int fixOffset;
        if (!isMainAxisDirectionHorizontal() && mIsRtl) {
            gap = mOrientationHelper.getEndAfterPadding() - startOffset;
            if (gap > 0) {
                // check if we should fix this gap.
                fixOffset = handleScrollingCrossAxis(-gap, recycler, state);
            } else {
                return 0; // nothing to fix
            }
        } else {
            gap = startOffset - mOrientationHelper.getStartAfterPadding();
            if (gap > 0) {
                fixOffset = -handleScrollingCrossAxis(gap, recycler, state);
            } else {
                return 0; // nothing to fix
            }
        }
        startOffset += fixOffset;
        if (canOffsetChildren) {
            gap = startOffset - mOrientationHelper.getStartAfterPadding();
            if (gap > 0) {
                mOrientationHelper.offsetChildren(-gap);
                return fixOffset - gap;
            }
        }
        return fixOffset;
    }
    private int fixLayoutEndGap(int endOffset, RecyclerView.Recycler recycler,
                                RecyclerView.State state, boolean canOffsetChildren) {
        int gap;
        boolean columnAndRtl = !isMainAxisDirectionHorizontal() && mIsRtl;
        int fixOffset;
        if (columnAndRtl) {
            gap = endOffset - mOrientationHelper.getStartAfterPadding();
            if (gap > 0) {
                fixOffset = handleScrollingCrossAxis(gap, recycler, state);
            } else {
                return 0; // nothing to fix
            }
        } else {
            gap = mOrientationHelper.getEndAfterPadding() - endOffset;
            if (gap > 0) {
                fixOffset = -handleScrollingCrossAxis(-gap, recycler, state);
            } else {
                return 0; // nothing to fix
            }
        }
        endOffset += fixOffset;
        if (canOffsetChildren) {
            gap = mOrientationHelper.getEndAfterPadding() - endOffset;
            if (gap > 0) {
                mOrientationHelper.offsetChildren(gap);
                return gap + fixOffset;
            }
        }
        return fixOffset;
    }

    private void updateFlexLines(int childCount) {
        int widthMeasureSpec = View.MeasureSpec.makeMeasureSpec(getWidth(), getWidthMode());
        int heightMeasureSpec = View.MeasureSpec.makeMeasureSpec(getHeight(), getHeightMode());
        int width = getWidth();
        int height = getHeight();
        boolean isMainSizeChanged;
        int needsToFill;
        if (isMainAxisDirectionHorizontal()) {
            isMainSizeChanged = mLastWidth != Integer.MIN_VALUE && mLastWidth != width;
            needsToFill = mLayoutState.mInfinite ?
                    mContext.getResources().getDisplayMetrics().heightPixels
                    : mLayoutState.mAvailable;
        } else {
            isMainSizeChanged = mLastHeight != Integer.MIN_VALUE && mLastHeight != height;
            needsToFill = mLayoutState.mInfinite ?
                    mContext.getResources().getDisplayMetrics().widthPixels
                    : mLayoutState.mAvailable;
        }

        mLastWidth = width;
        mLastHeight = height;

        if (mDirtyPosition == NO_POSITION &&
                (mPendingScrollPosition != NO_POSITION || isMainSizeChanged)) {
            if (mAnchorInfo.mLayoutFromEnd) {
                return;
            }
            mFlexLines.clear();
            assert mFlexboxHelper.mIndexToFlexLine != null;
            mFlexLinesResult.reset();
            if (isMainAxisDirectionHorizontal()) {
                mFlexboxHelper
                        .calculateHorizontalFlexLinesToIndex(mFlexLinesResult,
                                widthMeasureSpec, heightMeasureSpec,
                                needsToFill, mAnchorInfo.mPosition, mFlexLines);
            } else {
                mFlexboxHelper
                        .calculateVerticalFlexLinesToIndex(mFlexLinesResult,
                                widthMeasureSpec, heightMeasureSpec,
                                needsToFill, mAnchorInfo.mPosition, mFlexLines);
            }
            mFlexLines = mFlexLinesResult.mFlexLines;
            mFlexboxHelper.determineMainSize(widthMeasureSpec, heightMeasureSpec);
            mFlexboxHelper.stretchViews();
            mAnchorInfo.mFlexLinePosition =
                    mFlexboxHelper.mIndexToFlexLine[mAnchorInfo.mPosition];
            mLayoutState.mFlexLinePosition = mAnchorInfo.mFlexLinePosition;
        } else {
            int fromIndex = mDirtyPosition != NO_POSITION ?
                    Math.min(mDirtyPosition, mAnchorInfo.mPosition) : mAnchorInfo.mPosition;

            mFlexLinesResult.reset();
            if (isMainAxisDirectionHorizontal()) {
                if (mFlexLines.size() > 0) {
                    mFlexboxHelper.clearFlexLines(mFlexLines, fromIndex);
                    mFlexboxHelper.calculateFlexLines(mFlexLinesResult, widthMeasureSpec,
                            heightMeasureSpec, needsToFill, fromIndex, mAnchorInfo.mPosition,
                            mFlexLines);
                } else {
                    mFlexboxHelper.ensureIndexToFlexLine(childCount);
                    mFlexboxHelper
                            .calculateHorizontalFlexLines(mFlexLinesResult,
                                    widthMeasureSpec, heightMeasureSpec,
                                    needsToFill, 0, mFlexLines);
                }
            } else {
                if (mFlexLines.size() > 0) {
                    mFlexboxHelper.clearFlexLines(mFlexLines, fromIndex);
                    mFlexboxHelper.calculateFlexLines(mFlexLinesResult, heightMeasureSpec,
                            widthMeasureSpec, needsToFill, fromIndex, mAnchorInfo.mPosition,
                            mFlexLines);
                } else {
                    mFlexboxHelper.ensureIndexToFlexLine(childCount);
                    mFlexboxHelper
                            .calculateVerticalFlexLines(mFlexLinesResult, widthMeasureSpec,
                                    heightMeasureSpec, needsToFill, 0, mFlexLines);
                }
            }
            mFlexLines = mFlexLinesResult.mFlexLines;
            mFlexboxHelper.determineMainSize(widthMeasureSpec, heightMeasureSpec,
                    fromIndex);
            mFlexboxHelper.stretchViews(fromIndex);
        }
    }

    @Override
    public void onLayoutCompleted(RecyclerView.State state) {
        super.onLayoutCompleted(state);
        if (DEBUG) {
            Log.d(TAG, "onLayoutCompleted. " + state);
        }
        mPendingSavedState = null;
        mPendingScrollPosition = NO_POSITION;
        mPendingScrollPositionOffset = INVALID_OFFSET;
        mDirtyPosition = NO_POSITION;
        mAnchorInfo.reset();
        mViewCache.clear();
    }

    boolean isLayoutRtl() {
        return mIsRtl;
    }

    private void resolveLayoutDirection() {
        int layoutDirection = getLayoutDirection();
        switch (mFlexDirection) {
            case FlexDirection.ROW:
                mIsRtl = layoutDirection == ViewCompat.LAYOUT_DIRECTION_RTL;
                mFromBottomToTop = mFlexWrap == FlexWrap.WRAP_REVERSE;
                break;
            case FlexDirection.ROW_REVERSE:
                mIsRtl = layoutDirection != ViewCompat.LAYOUT_DIRECTION_RTL;
                mFromBottomToTop = mFlexWrap == FlexWrap.WRAP_REVERSE;
                break;
            case FlexDirection.COLUMN:
                mIsRtl = layoutDirection == ViewCompat.LAYOUT_DIRECTION_RTL;
                if (mFlexWrap == FlexWrap.WRAP_REVERSE) {
                    mIsRtl = !mIsRtl;
                }
                mFromBottomToTop = false;
                break;
            case FlexDirection.COLUMN_REVERSE:
                mIsRtl = layoutDirection == ViewCompat.LAYOUT_DIRECTION_RTL;
                if (mFlexWrap == FlexWrap.WRAP_REVERSE) {
                    mIsRtl = !mIsRtl;
                }
                mFromBottomToTop = true;
                break;
            default:
                mIsRtl = false;
                mFromBottomToTop = false;
        }
    }

    private void updateAnchorInfoForLayout(RecyclerView.State state, CustomeLayoutManager.AnchorInfo anchorInfo) {
        if (updateAnchorFromPendingState(state, anchorInfo, mPendingSavedState)) {
            if (DEBUG) {
                Log.d(TAG, "updated anchor from the pending state");
            }
            return;
        }
        if (updateAnchorFromChildren(state, anchorInfo)) {
            if (DEBUG) {
                Log.d(TAG,
                        String.format("updated anchor info from existing children. AnchorInfo: %s",
                                anchorInfo));
            }
            return;
        }
        if (DEBUG) {
            Log.d(TAG, "deciding anchor info for fresh state");
        }
        anchorInfo.assignCoordinateFromPadding();
        anchorInfo.mPosition = 0;
        anchorInfo.mFlexLinePosition = 0;
    }

    private boolean updateAnchorFromPendingState(RecyclerView.State state, CustomeLayoutManager.AnchorInfo anchorInfo,
                                                 CustomeLayoutManager.SavedState savedState) {
        assert mFlexboxHelper.mIndexToFlexLine != null;
        if (state.isPreLayout() || mPendingScrollPosition == NO_POSITION) {
            return false;
        }
        if (mPendingScrollPosition < 0 || mPendingScrollPosition >= state.getItemCount()) {
            mPendingScrollPosition = NO_POSITION;
            mPendingScrollPositionOffset = INVALID_OFFSET;
            if (DEBUG) {
                Log.e(TAG, "ignoring invalid scroll position " + mPendingScrollPosition);
            }
            return false;
        }

        anchorInfo.mPosition = mPendingScrollPosition;
        anchorInfo.mFlexLinePosition = mFlexboxHelper.mIndexToFlexLine[anchorInfo.mPosition];
        if (mPendingSavedState != null && mPendingSavedState.hasValidAnchor(state.getItemCount())) {
            anchorInfo.mCoordinate = mOrientationHelper.getStartAfterPadding() +
                    savedState.mAnchorOffset;
            anchorInfo.mAssignedFromSavedState = true;
            anchorInfo.mFlexLinePosition = NO_POSITION;
            return true;
        }

        if (mPendingScrollPositionOffset == INVALID_OFFSET) {
            View anchorView = findViewByPosition(mPendingScrollPosition);
            if (anchorView != null) {
                if (mOrientationHelper.getDecoratedMeasurement(anchorView) >
                        mOrientationHelper.getTotalSpace()) {
                    anchorInfo.assignCoordinateFromPadding();
                    return true;
                }
                int startGap = mOrientationHelper.getDecoratedStart(anchorView)
                        - mOrientationHelper.getStartAfterPadding();
                if (startGap < 0) {
                    anchorInfo.mCoordinate = mOrientationHelper.getStartAfterPadding();
                    anchorInfo.mLayoutFromEnd = false;
                    return true;
                }

                int endGap = mOrientationHelper.getEndAfterPadding() -
                        mOrientationHelper.getDecoratedEnd(anchorView);
                if (endGap < 0) {
                    anchorInfo.mCoordinate = mOrientationHelper.getEndAfterPadding();
                    anchorInfo.mLayoutFromEnd = true;
                    return true;
                }
                anchorInfo.mCoordinate = anchorInfo.mLayoutFromEnd ?
                        (mOrientationHelper.getDecoratedEnd(anchorView) +
                                mOrientationHelper.getTotalSpaceChange())
                        : mOrientationHelper.getDecoratedStart(anchorView);
            } else {
                if (getChildCount() > 0) {
                    int position = getPosition(getChildAt(0));
                    anchorInfo.mLayoutFromEnd = mPendingScrollPosition < position;
                }
                anchorInfo.assignCoordinateFromPadding();
            }
            return true;
        }

        if (!isMainAxisDirectionHorizontal() && mIsRtl) {
            anchorInfo.mCoordinate = mPendingScrollPositionOffset
                    - mOrientationHelper.getEndPadding();
        } else {
            anchorInfo.mCoordinate = mOrientationHelper.getStartAfterPadding()
                    + mPendingScrollPositionOffset;
        }
        return true;
    }

    private boolean updateAnchorFromChildren(RecyclerView.State state, CustomeLayoutManager.AnchorInfo anchorInfo) {
        if (getChildCount() == 0) {
            return false;
        }

        View referenceChild = anchorInfo.mLayoutFromEnd
                ? findLastReferenceChild(state.getItemCount())
                : findFirstReferenceChild(state.getItemCount());
        if (referenceChild != null) {
            anchorInfo.assignFromView(referenceChild);
            if (!state.isPreLayout() && supportsPredictiveItemAnimations()) {
                final boolean notVisible =
                        mOrientationHelper.getDecoratedStart(referenceChild) >= mOrientationHelper
                                .getEndAfterPadding()
                                || mOrientationHelper.getDecoratedEnd(referenceChild)
                                < mOrientationHelper.getStartAfterPadding();
                if (notVisible) {
                    anchorInfo.mCoordinate = anchorInfo.mLayoutFromEnd
                            ? mOrientationHelper.getEndAfterPadding()
                            : mOrientationHelper.getStartAfterPadding();
                }
            }
            return true;
        }
        return false;
    }
    private View findFirstReferenceChild(int itemCount) {
        assert mFlexboxHelper.mIndexToFlexLine != null;
        View firstFound = findReferenceChild(0, getChildCount(), itemCount);
        if (firstFound == null) {
            return null;
        }
        int firstFoundPosition = getPosition(firstFound);
        int firstFoundLinePosition = mFlexboxHelper.mIndexToFlexLine[firstFoundPosition];
        if (firstFoundLinePosition == NO_POSITION) {
            return null;
        }
        FlexLine firstFoundLine = mFlexLines.get(firstFoundLinePosition);
        return findFirstReferenceViewInLine(firstFound, firstFoundLine);
    }
    private View findLastReferenceChild(int itemCount) {
        assert mFlexboxHelper.mIndexToFlexLine != null;
        View lastFound = findReferenceChild(getChildCount() - 1, -1, itemCount);
        if (lastFound == null) {
            return null;
        }
        int lastFoundPosition = getPosition(lastFound);
        int lastFoundLinePosition = mFlexboxHelper.mIndexToFlexLine[lastFoundPosition];
        FlexLine lastFoundLine = mFlexLines.get(lastFoundLinePosition);
        return findLastReferenceViewInLine(lastFound, lastFoundLine);
    }
    private View findReferenceChild(int start, int end, int itemCount) {
        ensureOrientationHelper();
        ensureLayoutState();
        View invalidMatch = null;
        View outOfBoundsMatch = null;
        int boundStart = mOrientationHelper.getStartAfterPadding();
        int boundEnd = mOrientationHelper.getEndAfterPadding();
        int diff = end > start ? 1 : -1;
        for (int i = start; i != end; i += diff) {
            View view = getChildAt(i);
            int position = getPosition(view);
            if (position >= 0 && position < itemCount) {
                if (((RecyclerView.LayoutParams) view.getLayoutParams()).isItemRemoved()) {
                    if (invalidMatch == null) {
                        invalidMatch = view;
                    }
                } else if (mOrientationHelper.getDecoratedStart(view) < boundStart ||
                        mOrientationHelper.getDecoratedEnd(view) > boundEnd) {
                    if (outOfBoundsMatch == null) {
                        outOfBoundsMatch = view;
                    }
                } else {
                    return view;
                }
            }
        }
        return outOfBoundsMatch != null ? outOfBoundsMatch : invalidMatch;
    }

    private View getChildClosestToStart() {
        return getChildAt(0);
    }
    private int fill(RecyclerView.Recycler recycler, RecyclerView.State state,
                     CustomeLayoutManager.LayoutState layoutState) {
        if (layoutState.mScrollingOffset != CustomeLayoutManager.LayoutState.SCROLLING_OFFSET_NaN) {
            if (layoutState.mAvailable < 0) {
                layoutState.mScrollingOffset += layoutState.mAvailable;
            }
            recycleByLayoutState(recycler, layoutState);
        }
        int start = layoutState.mAvailable;
        int remainingSpace = layoutState.mAvailable;
        int consumed = 0;
        boolean mainAxisHorizontal = isMainAxisDirectionHorizontal();
        while ((remainingSpace > 0 || mLayoutState.mInfinite) &&
                layoutState.hasMore(state, mFlexLines)) {
            FlexLine flexLine = mFlexLines.get(layoutState.mFlexLinePosition);
            layoutState.mPosition = flexLine.mFirstIndex;
            consumed += layoutFlexLine(flexLine, layoutState);

            if (!mainAxisHorizontal && mIsRtl) {
                layoutState.mOffset -= flexLine.getCrossSize() * layoutState.mLayoutDirection;
            } else {
                layoutState.mOffset += flexLine.getCrossSize() * layoutState.mLayoutDirection;
            }

            remainingSpace -= flexLine.getCrossSize();
        }
        layoutState.mAvailable -= consumed;
        if (layoutState.mScrollingOffset != CustomeLayoutManager.LayoutState.SCROLLING_OFFSET_NaN) {
            layoutState.mScrollingOffset += consumed;
            if (layoutState.mAvailable < 0) {
                layoutState.mScrollingOffset += layoutState.mAvailable;
            }
            recycleByLayoutState(recycler, layoutState);
        }
        return start - layoutState.mAvailable;
    }

    private void recycleByLayoutState(RecyclerView.Recycler recycler, CustomeLayoutManager.LayoutState layoutState) {
        if (!layoutState.mShouldRecycle) {
            return;
        }
        if (layoutState.mLayoutDirection == CustomeLayoutManager.LayoutState.LAYOUT_START) {
            recycleFlexLinesFromEnd(recycler, layoutState);
        } else {
            recycleFlexLinesFromStart(recycler, layoutState);
        }
    }

    private void recycleFlexLinesFromStart(RecyclerView.Recycler recycler,
                                           CustomeLayoutManager.LayoutState layoutState) {
        if (layoutState.mScrollingOffset < 0) {
            return;
        }
        assert mFlexboxHelper.mIndexToFlexLine != null;
        int childCount = getChildCount();
        if (childCount == 0) {
            return;
        }
        View firstView = getChildAt(0);

        int currentLineIndex = mFlexboxHelper.mIndexToFlexLine[getPosition(firstView)];
        if (currentLineIndex == NO_POSITION) {
            return;
        }
        FlexLine flexLine = mFlexLines.get(currentLineIndex);
        int recycleTo = -1;
        for (int i = 0; i < childCount; i++) {
            View view = getChildAt(i);
            if (canViewBeRecycledFromStart(view, layoutState.mScrollingOffset)) {
                if (flexLine.mLastIndex == getPosition(view)) {
                    recycleTo = i;
                    if (currentLineIndex >= mFlexLines.size() - 1) {
                        break;
                    } else {
                        currentLineIndex += layoutState.mLayoutDirection;
                        flexLine = mFlexLines.get(currentLineIndex);
                    }
                }
            } else {
                break;
            }
        }
        recycleChildren(recycler, 0, recycleTo);
    }

    private boolean canViewBeRecycledFromStart(View view, int scrollingOffset) {
        if (!isMainAxisDirectionHorizontal() && mIsRtl) {
            return mOrientationHelper.getEnd() -
                    mOrientationHelper.getDecoratedStart(view) <= scrollingOffset;
        } else {
            return mOrientationHelper.getDecoratedEnd(view) <= scrollingOffset;
        }
    }

    private void recycleFlexLinesFromEnd(RecyclerView.Recycler recycler, CustomeLayoutManager.LayoutState layoutState) {
        if (layoutState.mScrollingOffset < 0) {
            return;
        }
        assert mFlexboxHelper.mIndexToFlexLine != null;
        int limit = mOrientationHelper.getEnd() - layoutState.mScrollingOffset;
        int childCount = getChildCount();
        if (childCount == 0) {
            return;
        }

        View lastView = getChildAt(childCount - 1);
        int currentLineIndex = mFlexboxHelper.mIndexToFlexLine[getPosition(lastView)];
        if (currentLineIndex == NO_POSITION) {
            return;
        }
        int recycleTo = childCount - 1;
        int recycleFrom = childCount;
        FlexLine flexLine = mFlexLines.get(currentLineIndex);
        for (int i = childCount - 1; i >= 0; i--) {
            View view = getChildAt(i);
            if (canViewBeRecycledFromEnd(view, layoutState.mScrollingOffset)) {
                if (flexLine.mFirstIndex == getPosition(view)) {
                    recycleFrom = i;
                    if (currentLineIndex <= 0) {
                        break;
                    } else {
                        currentLineIndex += layoutState.mLayoutDirection;
                        flexLine = mFlexLines.get(currentLineIndex);
                    }
                }
            } else {
                break;
            }
        }
        recycleChildren(recycler, recycleFrom, recycleTo);
    }

    private boolean canViewBeRecycledFromEnd(View view, int scrollingOffset) {
        if (!isMainAxisDirectionHorizontal() && mIsRtl) {
            return mOrientationHelper.getDecoratedEnd(view) <= scrollingOffset;
        } else {
            return mOrientationHelper.getDecoratedStart(view) >=
                    mOrientationHelper.getEnd() - scrollingOffset;
        }
    }
    private void recycleChildren(RecyclerView.Recycler recycler, int startIndex, int endIndex) {
        for (int i = endIndex; i >= startIndex; i--) {
            removeAndRecycleViewAt(i, recycler);
        }
    }

    private int layoutFlexLine(FlexLine flexLine, CustomeLayoutManager.LayoutState layoutState) {
        if (isMainAxisDirectionHorizontal()) {
            return layoutFlexLineMainAxisHorizontal(flexLine, layoutState);
        } else {
            return layoutFlexLineMainAxisVertical(flexLine, layoutState);
        }
    }

    private int layoutFlexLineMainAxisHorizontal(FlexLine flexLine, CustomeLayoutManager.LayoutState layoutState) {
        assert mFlexboxHelper.mMeasureSpecCache != null;

        int paddingLeft = getPaddingLeft();
        int paddingRight = getPaddingRight();
        int parentWidth = getWidth();

        int childTop = layoutState.mOffset;
        if (layoutState.mLayoutDirection == CustomeLayoutManager.LayoutState.LAYOUT_START) {
            childTop = childTop - flexLine.mCrossSize;
        }
        int startPosition = layoutState.mPosition;

        float childLeft;

        // Only used when mIsRtl is true
        float childRight;
        float spaceBetweenItem = 0f;
        switch (mJustifyContent) {
            case JustifyContent.FLEX_START:
                childLeft = paddingLeft;
                childRight = parentWidth - paddingRight;
                break;
            case JustifyContent.FLEX_END:
                childLeft = parentWidth - flexLine.mMainSize + paddingRight;
                childRight = flexLine.mMainSize - paddingLeft;
                break;
            case JustifyContent.CENTER:
                childLeft = paddingLeft + (parentWidth - flexLine.mMainSize) / 2f;
                childRight = parentWidth - paddingRight - (parentWidth - flexLine.mMainSize) / 2f;
                break;
            case JustifyContent.SPACE_AROUND:
                if (flexLine.mItemCount != 0) {
                    spaceBetweenItem = (parentWidth - flexLine.mMainSize)
                            / (float) flexLine.mItemCount;
                }
                childLeft = paddingLeft + spaceBetweenItem / 2f;
                childRight = parentWidth - paddingRight - spaceBetweenItem / 2f;
                break;
            case JustifyContent.SPACE_BETWEEN:
                childLeft = paddingLeft;
                float denominator = flexLine.mItemCount != 1 ? flexLine.mItemCount - 1 : 1f;
                spaceBetweenItem = (parentWidth - flexLine.mMainSize) / denominator;
                childRight = parentWidth - paddingRight;
                break;
            default:
                throw new IllegalStateException(
                        "Invalid justifyContent is set: " + mJustifyContent);
        }
        childLeft -= mAnchorInfo.mPerpendicularCoordinate;
        childRight -= mAnchorInfo.mPerpendicularCoordinate;
        spaceBetweenItem = Math.max(spaceBetweenItem, 0);

        int indexInFlexLine = 0;
        for (int i = startPosition, itemCount = flexLine.getItemCount();
             i < startPosition + itemCount; i++) {
            View view = getFlexItemAt(i);
            if (view == null) {
                continue;
            }

            if (layoutState.mLayoutDirection == CustomeLayoutManager.LayoutState.LAYOUT_END) {
                calculateItemDecorationsForChild(view, TEMP_RECT);
                addView(view);
            } else {
                calculateItemDecorationsForChild(view, TEMP_RECT);
                addView(view, indexInFlexLine);
                indexInFlexLine++;
            }

            long measureSpec = mFlexboxHelper.mMeasureSpecCache[i];
            int widthSpec = mFlexboxHelper.extractLowerInt(measureSpec);
            int heightSpec = mFlexboxHelper.extractHigherInt(measureSpec);
            CustomeLayoutManager.LayoutParams lp = (CustomeLayoutManager.LayoutParams) view.getLayoutParams();
            if (shouldMeasureChild(view, widthSpec, heightSpec, lp)) {
                view.measure(widthSpec, heightSpec);
            }

            childLeft += (lp.leftMargin + getLeftDecorationWidth(view));
            childRight -= (lp.rightMargin + getRightDecorationWidth(view));

            int topWithDecoration = childTop + getTopDecorationHeight(view);
            if (mIsRtl) {
                mFlexboxHelper.layoutSingleChildHorizontal(view, flexLine,
                        Math.round(childRight) - view.getMeasuredWidth(),
                        topWithDecoration, Math.round(childRight),
                        topWithDecoration + view.getMeasuredHeight());
            } else {
                mFlexboxHelper.layoutSingleChildHorizontal(view, flexLine,
                        Math.round(childLeft), topWithDecoration,
                        Math.round(childLeft) + view.getMeasuredWidth(),
                        topWithDecoration + view.getMeasuredHeight());
            }
            childLeft += (view.getMeasuredWidth() + lp.rightMargin + getRightDecorationWidth(view)
                    + spaceBetweenItem);
            childRight -= (view.getMeasuredWidth() + lp.leftMargin + getLeftDecorationWidth(view)
                    + spaceBetweenItem);
        }
        layoutState.mFlexLinePosition += mLayoutState.mLayoutDirection;
        return flexLine.getCrossSize();
    }

    private int layoutFlexLineMainAxisVertical(FlexLine flexLine, CustomeLayoutManager.LayoutState layoutState) {
        assert mFlexboxHelper.mMeasureSpecCache != null;

        int paddingTop = getPaddingTop();
        int paddingBottom = getPaddingBottom();
        int parentHeight = getHeight();

        int childLeft = layoutState.mOffset;
        int childRight = layoutState.mOffset;
        if (layoutState.mLayoutDirection == CustomeLayoutManager.LayoutState.LAYOUT_START) {
            childLeft = childLeft - flexLine.mCrossSize;
            childRight = childRight + flexLine.mCrossSize;
        }
        int startPosition = layoutState.mPosition;

        float childTop;

        float childBottom;
        float spaceBetweenItem = 0f;
        switch (mJustifyContent) {
            case JustifyContent.FLEX_START:
                childTop = paddingTop;
                childBottom = parentHeight - paddingBottom;
                break;
            case JustifyContent.FLEX_END:
                childTop = parentHeight - flexLine.mMainSize + paddingBottom;
                childBottom = flexLine.mMainSize - paddingTop;
                break;
            case JustifyContent.CENTER:
                childTop = paddingTop + (parentHeight - flexLine.mMainSize) / 2f;
                childBottom = parentHeight - paddingBottom
                        - (parentHeight - flexLine.mMainSize) / 2f;
                break;
            case JustifyContent.SPACE_AROUND:
                if (flexLine.mItemCount != 0) {
                    spaceBetweenItem = (parentHeight - flexLine.mMainSize)
                            / (float) flexLine.mItemCount;
                }
                childTop = paddingTop + spaceBetweenItem / 2f;
                childBottom = parentHeight - paddingBottom - spaceBetweenItem / 2f;
                break;
            case JustifyContent.SPACE_BETWEEN:
                childTop = paddingTop;
                float denominator = flexLine.mItemCount != 1 ? flexLine.mItemCount - 1 : 1f;
                spaceBetweenItem = (parentHeight - flexLine.mMainSize) / denominator;
                childBottom = parentHeight - paddingBottom;
                break;
            default:
                throw new IllegalStateException(
                        "Invalid justifyContent is set: " + mJustifyContent);
        }
        childTop -= mAnchorInfo.mPerpendicularCoordinate;
        childBottom -= mAnchorInfo.mPerpendicularCoordinate;
        spaceBetweenItem = Math.max(spaceBetweenItem, 0);

        int indexInFlexLine = 0;
        for (int i = startPosition, itemCount = flexLine.getItemCount();
             i < startPosition + itemCount; i++) {
            View view = getFlexItemAt(i);
            if (view == null) {
                continue;
            }

            long measureSpec = mFlexboxHelper.mMeasureSpecCache[i];
            int widthSpec = mFlexboxHelper.extractLowerInt(measureSpec);
            int heightSpec = mFlexboxHelper.extractHigherInt(measureSpec);
            CustomeLayoutManager.LayoutParams lp = (CustomeLayoutManager.LayoutParams) view.getLayoutParams();
            if (shouldMeasureChild(view, widthSpec, heightSpec, lp)) {
                view.measure(widthSpec, heightSpec);
            }

            childTop += (lp.topMargin + getTopDecorationHeight(view));
            childBottom -= (lp.rightMargin + getBottomDecorationHeight(view));

            if (layoutState.mLayoutDirection == CustomeLayoutManager.LayoutState.LAYOUT_END) {
                calculateItemDecorationsForChild(view, TEMP_RECT);
                addView(view);
            } else {
                calculateItemDecorationsForChild(view, TEMP_RECT);
                addView(view, indexInFlexLine);
                indexInFlexLine++;
            }

            int leftWithDecoration = childLeft + getLeftDecorationWidth(view);
            int rightWithDecoration = childRight - getRightDecorationWidth(view);
            if (mIsRtl) {
                if (mFromBottomToTop) {
                    mFlexboxHelper.layoutSingleChildVertical(view, flexLine, mIsRtl,
                            rightWithDecoration - view.getMeasuredWidth(),
                            Math.round(childBottom) - view.getMeasuredHeight(),
                            rightWithDecoration, Math.round(childBottom));
                } else {
                    mFlexboxHelper.layoutSingleChildVertical(view, flexLine, mIsRtl,
                            rightWithDecoration - view.getMeasuredWidth(),
                            Math.round(childTop), rightWithDecoration,
                            Math.round(childTop) + view.getMeasuredHeight());
                }
            } else {
                if (mFromBottomToTop) {
                    mFlexboxHelper.layoutSingleChildVertical(view, flexLine, mIsRtl,
                            leftWithDecoration, Math.round(childBottom) - view.getMeasuredHeight(),
                            leftWithDecoration + view.getMeasuredWidth(), Math.round(childBottom));
                } else {
                    mFlexboxHelper.layoutSingleChildVertical(view, flexLine, mIsRtl,
                            leftWithDecoration, Math.round(childTop),
                            leftWithDecoration + view.getMeasuredWidth(),
                            Math.round(childTop) + view.getMeasuredHeight());
                }
            }
            childTop += (view.getMeasuredHeight() + lp.topMargin + getBottomDecorationHeight(view)
                    + spaceBetweenItem);
            childBottom -= (view.getMeasuredHeight() + lp.bottomMargin +
                    getTopDecorationHeight(view) + spaceBetweenItem);
        }
        layoutState.mFlexLinePosition += mLayoutState.mLayoutDirection;
        return flexLine.getCrossSize();
    }

    @Override
    public boolean isMainAxisDirectionHorizontal() {
        return mFlexDirection == FlexDirection.ROW || mFlexDirection == FlexDirection.ROW_REVERSE;
    }

    private void updateLayoutStateToFillEnd(CustomeLayoutManager.AnchorInfo anchorInfo, boolean fromNextLine,
                                            boolean considerInfinite) {
        if (considerInfinite) {
            resolveInfiniteAmount();
        } else {
            mLayoutState.mInfinite = false;
        }
        if (!isMainAxisDirectionHorizontal() && mIsRtl) {
            mLayoutState.mAvailable = anchorInfo.mCoordinate - getPaddingRight();
        } else {
            mLayoutState.mAvailable =
                    mOrientationHelper.getEndAfterPadding() - anchorInfo.mCoordinate;
        }
        mLayoutState.mPosition = anchorInfo.mPosition;
        mLayoutState.mItemDirection = CustomeLayoutManager.LayoutState.ITEM_DIRECTION_TAIL;
        mLayoutState.mLayoutDirection = CustomeLayoutManager.LayoutState.LAYOUT_END;
        mLayoutState.mOffset = anchorInfo.mCoordinate;
        mLayoutState.mScrollingOffset = CustomeLayoutManager.LayoutState.SCROLLING_OFFSET_NaN;
        mLayoutState.mFlexLinePosition = anchorInfo.mFlexLinePosition;

        if (fromNextLine
                && mFlexLines.size() > 1
                && anchorInfo.mFlexLinePosition >= 0
                && anchorInfo.mFlexLinePosition < mFlexLines.size() - 1) {
            FlexLine currentLine = mFlexLines.get(anchorInfo.mFlexLinePosition);
            mLayoutState.mFlexLinePosition++;
            mLayoutState.mPosition += currentLine.getItemCount();
        }
    }

    private void updateLayoutStateToFillStart(CustomeLayoutManager.AnchorInfo anchorInfo, boolean fromPreviousLine,
                                              boolean considerInfinite) {
        if (considerInfinite) {
            resolveInfiniteAmount();
        } else {
            mLayoutState.mInfinite = false;
        }
        if (!isMainAxisDirectionHorizontal() && mIsRtl) {
            mLayoutState.mAvailable = mParent.getWidth() - anchorInfo.mCoordinate
                    - mOrientationHelper.getStartAfterPadding();
        } else {
            mLayoutState.mAvailable = anchorInfo.mCoordinate - mOrientationHelper
                    .getStartAfterPadding();
        }
        mLayoutState.mPosition = anchorInfo.mPosition;
        mLayoutState.mItemDirection = CustomeLayoutManager.LayoutState.ITEM_DIRECTION_TAIL;
        mLayoutState.mLayoutDirection = CustomeLayoutManager.LayoutState.LAYOUT_START;
        mLayoutState.mOffset = anchorInfo.mCoordinate;
        mLayoutState.mScrollingOffset = CustomeLayoutManager.LayoutState.SCROLLING_OFFSET_NaN;
        mLayoutState.mFlexLinePosition = anchorInfo.mFlexLinePosition;

        if (fromPreviousLine && anchorInfo.mFlexLinePosition > 0
                && mFlexLines.size() > anchorInfo.mFlexLinePosition) {
            FlexLine currentLine = mFlexLines.get(anchorInfo.mFlexLinePosition);
            mLayoutState.mFlexLinePosition--;
            mLayoutState.mPosition -= currentLine.getItemCount();
        }
    }

    private void resolveInfiniteAmount() {
        int crossMode;
        if (isMainAxisDirectionHorizontal()) {
            crossMode = getHeightMode();
        } else {
            crossMode = getWidthMode();
        }
        mLayoutState.mInfinite =
                crossMode == View.MeasureSpec.UNSPECIFIED || crossMode == View.MeasureSpec.AT_MOST;
    }

    private void ensureOrientationHelper() {
        if (mOrientationHelper != null) {
            return;
        }
        if (isMainAxisDirectionHorizontal()) {
            if (mFlexWrap == FlexWrap.NOWRAP) {
                mOrientationHelper = OrientationHelper.createHorizontalHelper(this);
                mSubOrientationHelper = OrientationHelper.createVerticalHelper(this);
            } else {
                mOrientationHelper = OrientationHelper.createVerticalHelper(this);
                mSubOrientationHelper = OrientationHelper.createHorizontalHelper(this);
            }
        } else {
            if (mFlexWrap == FlexWrap.NOWRAP) {
                mOrientationHelper = OrientationHelper.createVerticalHelper(this);
                mSubOrientationHelper = OrientationHelper.createHorizontalHelper(this);
            } else {
                mOrientationHelper = OrientationHelper.createHorizontalHelper(this);
                mSubOrientationHelper = OrientationHelper.createVerticalHelper(this);
            }
        }
    }

    private void ensureLayoutState() {
        if (mLayoutState == null) {
            mLayoutState = new CustomeLayoutManager.LayoutState();
        }
    }

    @Override
    public void scrollToPosition(int position) {
        mPendingScrollPosition = position;
        mPendingScrollPositionOffset = INVALID_OFFSET;
        if (mPendingSavedState != null) {
            mPendingSavedState.invalidateAnchor();
        }
        requestLayout();
    }

    @Override
    public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state,
                                       int position) {
        LinearSmoothScroller smoothScroller =
                new LinearSmoothScroller(recyclerView.getContext());
        smoothScroller.setTargetPosition(position);
        startSmoothScroll(smoothScroller);
    }
    @SuppressWarnings("UnusedDeclaration")
    public boolean getRecycleChildrenOnDetach() {
        return mRecycleChildrenOnDetach;
    }

    @SuppressWarnings("UnusedDeclaration")
    public void setRecycleChildrenOnDetach(boolean recycleChildrenOnDetach) {
        mRecycleChildrenOnDetach = recycleChildrenOnDetach;
    }

    @Override
    public void onAttachedToWindow(RecyclerView recyclerView) {
        super.onAttachedToWindow(recyclerView);
        mParent = (View) recyclerView.getParent();
    }

    @Override
    public void onDetachedFromWindow(RecyclerView view, RecyclerView.Recycler recycler) {
        super.onDetachedFromWindow(view, recycler);
        if (mRecycleChildrenOnDetach) {
            if (DEBUG) {
                Log.d(TAG, "onDetachedFromWindow. Recycling children in the recycler");
            }
            removeAndRecycleAllViews(recycler);
            recycler.clear();
        }
    }

    @Override
    public boolean canScrollHorizontally() {
        return !isMainAxisDirectionHorizontal() || getWidth() > mParent.getWidth();
    }

    @Override
    public boolean canScrollVertically() {
        return isMainAxisDirectionHorizontal() || getHeight() > mParent.getHeight();
    }

    @Override
    public int scrollHorizontallyBy(int dx, RecyclerView.Recycler recycler,
                                    RecyclerView.State state) {
        if (!isMainAxisDirectionHorizontal()) {
            int scrolled = handleScrollingCrossAxis(dx, recycler, state);
            mViewCache.clear();
            return scrolled;
        } else {
            int scrolled = handleScrollingMainAxis(dx);
            mAnchorInfo.mPerpendicularCoordinate += scrolled;
            mSubOrientationHelper.offsetChildren(-scrolled);
            return scrolled;
        }
    }

    @Override
    public int scrollVerticallyBy(int dy, RecyclerView.Recycler recycler,
                                  RecyclerView.State state) {
        if (isMainAxisDirectionHorizontal()) {
            int scrolled = handleScrollingCrossAxis(dy, recycler, state);
            mViewCache.clear();
            return scrolled;
        } else {
            int scrolled = handleScrollingMainAxis(dy);
            mAnchorInfo.mPerpendicularCoordinate += scrolled;
            mSubOrientationHelper.offsetChildren(-scrolled);
            return scrolled;
        }
    }
    private int handleScrollingCrossAxis(int delta, RecyclerView.Recycler recycler,
                                         RecyclerView.State state) {
        if (getChildCount() == 0 || delta == 0) {
            return 0;
        }
        ensureOrientationHelper();
        mLayoutState.mShouldRecycle = true;
        int layoutDirection;
        boolean columnAndRtl = !isMainAxisDirectionHorizontal() && mIsRtl;
        if (columnAndRtl) {
            layoutDirection = delta < 0 ? CustomeLayoutManager.LayoutState.LAYOUT_END : CustomeLayoutManager.LayoutState.LAYOUT_START;
        } else {
            layoutDirection = delta > 0 ? CustomeLayoutManager.LayoutState.LAYOUT_END : CustomeLayoutManager.LayoutState.LAYOUT_START;
        }
        int absDelta = Math.abs(delta);

        updateLayoutState(layoutDirection, absDelta);

        int freeScroll = mLayoutState.mScrollingOffset;
        int consumed = freeScroll + fill(recycler, state, mLayoutState);
        if (consumed < 0) {
            return 0;
        }
        int scrolled;
        if (columnAndRtl) {
            scrolled = absDelta > consumed ? -layoutDirection * consumed : delta;
        } else {
            scrolled = absDelta > consumed ? layoutDirection * consumed : delta;
        }
        mOrientationHelper.offsetChildren(-scrolled);
        mLayoutState.mLastScrollDelta = scrolled;
        return scrolled;
    }

    private int handleScrollingMainAxis(int delta) {
        if (getChildCount() == 0 || delta == 0) {
            return 0;
        }
        ensureOrientationHelper();
        boolean isMainAxisHorizontal = isMainAxisDirectionHorizontal();
        int parentLength = isMainAxisHorizontal ? mParent.getWidth() : mParent.getHeight();
        int mainAxisLength = isMainAxisHorizontal ? getWidth() : getHeight();

        boolean layoutRtl  = getLayoutDirection() == ViewCompat.LAYOUT_DIRECTION_RTL;
        if (layoutRtl) {
            int absDelta = Math.abs(delta);
            if (delta < 0) {
                delta = Math.min(mainAxisLength
                        + mAnchorInfo.mPerpendicularCoordinate - parentLength, absDelta);
                delta = -delta;
            } else {
                delta = mAnchorInfo.mPerpendicularCoordinate + delta > 0
                        ? -mAnchorInfo.mPerpendicularCoordinate
                        : delta;
            }
        } else {
            if (delta > 0) {
                delta = Math.min(mainAxisLength
                        - mAnchorInfo.mPerpendicularCoordinate - parentLength, delta);
            } else {
                delta = mAnchorInfo.mPerpendicularCoordinate + delta >= 0 ? delta :
                        -mAnchorInfo.mPerpendicularCoordinate;
            }
        }
        return delta;
    }

    private void updateLayoutState(int layoutDirection, int absDelta) {
        assert mFlexboxHelper.mIndexToFlexLine != null;
        mLayoutState.mLayoutDirection = layoutDirection;
        boolean mainAxisHorizontal = isMainAxisDirectionHorizontal();

        int widthMeasureSpec = View.MeasureSpec.makeMeasureSpec(getWidth(), getWidthMode());
        int heightMeasureSpec = View.MeasureSpec.makeMeasureSpec(getHeight(), getHeightMode());
        boolean columnAndRtl = !mainAxisHorizontal && mIsRtl;
        if (layoutDirection == CustomeLayoutManager.LayoutState.LAYOUT_END) {
            View lastVisible = getChildAt(getChildCount() - 1);
            mLayoutState.mOffset = mOrientationHelper.getDecoratedEnd(lastVisible);
            int lastVisiblePosition = getPosition(lastVisible);
            int lastVisibleLinePosition = mFlexboxHelper.mIndexToFlexLine[lastVisiblePosition];
            FlexLine lastVisibleLine = mFlexLines.get(lastVisibleLinePosition);

            View referenceView = findLastReferenceViewInLine(lastVisible, lastVisibleLine);
            mLayoutState.mItemDirection = CustomeLayoutManager.LayoutState.ITEM_DIRECTION_TAIL;
            mLayoutState.mPosition = lastVisiblePosition + mLayoutState.mItemDirection;
            if (mFlexboxHelper.mIndexToFlexLine.length <= mLayoutState.mPosition) {
                mLayoutState.mFlexLinePosition = NO_POSITION;
            } else {
                mLayoutState.mFlexLinePosition
                        = mFlexboxHelper.mIndexToFlexLine[mLayoutState.mPosition];
            }

            if (columnAndRtl) {
                mLayoutState.mOffset = mOrientationHelper.getDecoratedStart(referenceView);
                mLayoutState.mScrollingOffset = -mOrientationHelper.getDecoratedStart(referenceView)
                        + mOrientationHelper.getStartAfterPadding();
                mLayoutState.mScrollingOffset = mLayoutState.mScrollingOffset >= 0 ?
                        mLayoutState.mScrollingOffset : 0;
            } else {
                mLayoutState.mOffset = mOrientationHelper.getDecoratedEnd(referenceView);
                mLayoutState.mScrollingOffset = mOrientationHelper.getDecoratedEnd(referenceView)
                        - mOrientationHelper.getEndAfterPadding();
            }

            if ((mLayoutState.mFlexLinePosition == NO_POSITION
                    || mLayoutState.mFlexLinePosition > mFlexLines.size() - 1) &&
                    mLayoutState.mPosition <= getFlexItemCount()) {

                int needsToFill = absDelta - mLayoutState.mScrollingOffset;
                mFlexLinesResult.reset();
                if (needsToFill > 0) {
                    if (mainAxisHorizontal) {
                        mFlexboxHelper.calculateHorizontalFlexLines(mFlexLinesResult,
                                widthMeasureSpec, heightMeasureSpec, needsToFill,
                                mLayoutState.mPosition, mFlexLines);
                    } else {
                        mFlexboxHelper.calculateVerticalFlexLines(mFlexLinesResult,
                                widthMeasureSpec, heightMeasureSpec, needsToFill,
                                mLayoutState.mPosition, mFlexLines);
                    }
                    mFlexboxHelper.determineMainSize(widthMeasureSpec, heightMeasureSpec,
                            mLayoutState.mPosition);
                    mFlexboxHelper.stretchViews(mLayoutState.mPosition);
                }
            }
        } else {
            View firstVisible = getChildAt(0);

            mLayoutState.mOffset = mOrientationHelper.getDecoratedStart(firstVisible);
            int firstVisiblePosition = getPosition(firstVisible);
            int firstVisibleLinePosition = mFlexboxHelper.mIndexToFlexLine[firstVisiblePosition];
            FlexLine firstVisibleLine = mFlexLines.get(firstVisibleLinePosition);

            View referenceView = findFirstReferenceViewInLine(firstVisible, firstVisibleLine);

            mLayoutState.mItemDirection = CustomeLayoutManager.LayoutState.ITEM_DIRECTION_TAIL;
            int flexLinePosition = mFlexboxHelper.mIndexToFlexLine[firstVisiblePosition];
            if (flexLinePosition == NO_POSITION) {
                flexLinePosition = 0;
            }
            if (flexLinePosition > 0) {
                FlexLine previousLine = mFlexLines.get(flexLinePosition - 1);
                mLayoutState.mPosition = firstVisiblePosition - previousLine.getItemCount();
            } else {
                mLayoutState.mPosition = NO_POSITION;
            }
            mLayoutState.mFlexLinePosition = flexLinePosition > 0 ? flexLinePosition - 1 : 0;

            if (columnAndRtl) {
                mLayoutState.mOffset = mOrientationHelper.getDecoratedEnd(referenceView);
                mLayoutState.mScrollingOffset = mOrientationHelper.getDecoratedEnd(referenceView)
                        - mOrientationHelper.getEndAfterPadding();
                mLayoutState.mScrollingOffset = mLayoutState.mScrollingOffset >= 0 ?
                        mLayoutState.mScrollingOffset : 0;
            } else {
                mLayoutState.mOffset = mOrientationHelper.getDecoratedStart(referenceView);
                mLayoutState.mScrollingOffset = -mOrientationHelper.getDecoratedStart(referenceView)
                        + mOrientationHelper.getStartAfterPadding();
            }
        }
        mLayoutState.mAvailable = absDelta - mLayoutState.mScrollingOffset;
    }
    private View findFirstReferenceViewInLine(View firstView, FlexLine firstVisibleLine) {
        boolean mainAxisHorizontal = isMainAxisDirectionHorizontal();
        View referenceView = firstView;
        for (int i = 1, to = firstVisibleLine.mItemCount;
             i < to; i++) {
            View viewInSameLine = getChildAt(i);
            if (viewInSameLine == null || viewInSameLine.getVisibility() == View.GONE) {
                continue;
            }
            if (mIsRtl && !mainAxisHorizontal) {
                if (mOrientationHelper.getDecoratedEnd(referenceView)
                        < mOrientationHelper.getDecoratedEnd(viewInSameLine)) {
                    referenceView = viewInSameLine;
                }
            } else {
                if (mOrientationHelper.getDecoratedStart(referenceView)
                        > mOrientationHelper.getDecoratedStart(viewInSameLine)) {
                    referenceView = viewInSameLine;
                }
            }
        }
        return referenceView;
    }
    private View findLastReferenceViewInLine(View lastView, FlexLine lastVisibleLine) {
        boolean mainAxisHorizontal = isMainAxisDirectionHorizontal();
        View referenceView = lastView;
        for (int i = getChildCount() - 2, to = getChildCount() - lastVisibleLine.mItemCount - 1;
             i > to; i--) {
            View viewInSameLine = getChildAt(i);
            if (viewInSameLine == null || viewInSameLine.getVisibility() == View.GONE) {
                continue;
            }
            if (mIsRtl && !mainAxisHorizontal) {
                if (mOrientationHelper.getDecoratedStart(referenceView) >
                        mOrientationHelper.getDecoratedStart(viewInSameLine)) {
                    referenceView = viewInSameLine;
                }
            } else {
                if (mOrientationHelper.getDecoratedEnd(referenceView) <
                        mOrientationHelper.getDecoratedEnd(viewInSameLine)) {
                    referenceView = viewInSameLine;
                }
            }
        }
        return referenceView;
    }

    @Override
    public int computeHorizontalScrollExtent(RecyclerView.State state) {
        int scrollExtent = computeScrollExtent(state);
        if (DEBUG) {
            Log.d(TAG, "computeHorizontalScrollExtent: " + scrollExtent);
        }
        return scrollExtent;
    }

    @Override
    public int computeVerticalScrollExtent(RecyclerView.State state) {
        int scrollExtent = computeScrollExtent(state);
        if (DEBUG) {
            Log.d(TAG, "computeVerticalScrollExtent: " + scrollExtent);
        }
        return scrollExtent;
    }

    private int computeScrollExtent(RecyclerView.State state) {
        if (getChildCount() == 0) {
            return 0;
        }
        int allChildrenCount = state.getItemCount();
        ensureOrientationHelper();
        View firstReferenceView = findFirstReferenceChild(allChildrenCount);
        View lastReferenceView = findLastReferenceChild(allChildrenCount);
        if (state.getItemCount() == 0 || firstReferenceView == null || lastReferenceView == null) {
            return 0;
        }
        int extend = mOrientationHelper.getDecoratedEnd(lastReferenceView) -
                mOrientationHelper.getDecoratedStart(firstReferenceView);
        return Math.min(mOrientationHelper.getTotalSpace(), extend);
    }

    @Override
    public int computeHorizontalScrollOffset(RecyclerView.State state) {
        int scrollOffset = computeScrollOffset(state);
        if (DEBUG) {
            Log.d(TAG, "computeHorizontalScrollOffset: " + scrollOffset);
        }
        return computeScrollOffset(state);
    }

    @Override
    public int computeVerticalScrollOffset(RecyclerView.State state) {
        int scrollOffset = computeScrollOffset(state);
        if (DEBUG) {
            Log.d(TAG, "computeVerticalScrollOffset: " + scrollOffset);
        }
        return scrollOffset;
    }

    private int computeScrollOffset(RecyclerView.State state) {
        if (getChildCount() == 0) {
            return 0;
        }
        int allChildrenCount = state.getItemCount();
        View firstReferenceView = findFirstReferenceChild(allChildrenCount);
        View lastReferenceView = findLastReferenceChild(allChildrenCount);
        if (state.getItemCount() == 0 || firstReferenceView == null || lastReferenceView == null) {
            return 0;
        }
        assert mFlexboxHelper.mIndexToFlexLine != null;
        int minPosition = getPosition(firstReferenceView);
        int maxPosition = getPosition(lastReferenceView);
        int laidOutArea = Math.abs(mOrientationHelper.getDecoratedEnd(lastReferenceView) -
                mOrientationHelper.getDecoratedStart(firstReferenceView));
        int firstLinePosition = mFlexboxHelper.mIndexToFlexLine[minPosition];
        if (firstLinePosition == 0 || firstLinePosition == NO_POSITION) {
            return 0;
        }
        int lastLinePosition = mFlexboxHelper.mIndexToFlexLine[maxPosition];
        int lineRange = lastLinePosition - firstLinePosition + 1;
        float averageSizePerLine = (float) laidOutArea / lineRange;
        return Math.round(
                firstLinePosition * averageSizePerLine + (mOrientationHelper.getStartAfterPadding()
                        - mOrientationHelper.getDecoratedStart(firstReferenceView)));
    }

    @Override
    public int computeHorizontalScrollRange(RecyclerView.State state) {
        int scrollRange = computeScrollRange(state);
        if (DEBUG) {
            Log.d(TAG, "computeHorizontalScrollRange: " + scrollRange);
        }
        return scrollRange;
    }

    @Override
    public int computeVerticalScrollRange(RecyclerView.State state) {
        int scrollRange = computeScrollRange(state);
        if (DEBUG) {
            Log.d(TAG, "computeVerticalScrollRange: " + scrollRange);
        }
        return scrollRange;
    }
    private int computeScrollRange(RecyclerView.State state) {
        if (getChildCount() == 0) {
            return 0;
        }
        int allItemCount = state.getItemCount();
        View firstReferenceView = findFirstReferenceChild(allItemCount);
        View lastReferenceView = findLastReferenceChild(allItemCount);
        if (state.getItemCount() == 0 || firstReferenceView == null || lastReferenceView == null) {
            return 0;
        }
        assert mFlexboxHelper.mIndexToFlexLine != null;
        int firstVisiblePosition = findFirstVisibleItemPosition();
        int lastVisiblePosition = findLastVisibleItemPosition();
        int laidOutArea = Math.abs(mOrientationHelper.getDecoratedEnd(lastReferenceView) -
                mOrientationHelper.getDecoratedStart(firstReferenceView));
        int laidOutRange = lastVisiblePosition - firstVisiblePosition + 1;
        return (int) ((float) laidOutArea / laidOutRange * state.getItemCount());
    }

    private boolean shouldMeasureChild(View child, int widthSpec, int heightSpec,
                                       RecyclerView.LayoutParams lp) {
        return child.isLayoutRequested()
                || !isMeasurementCacheEnabled()
                || !isMeasurementUpToDate(child.getWidth(), widthSpec, lp.width)
                || !isMeasurementUpToDate(child.getHeight(), heightSpec, lp.height);
    }

    private static boolean isMeasurementUpToDate(int childSize, int spec, int dimension) {
        final int specMode = View.MeasureSpec.getMode(spec);
        final int specSize = View.MeasureSpec.getSize(spec);
        if (dimension > 0 && childSize != dimension) {
            return false;
        }
        switch (specMode) {
            case View.MeasureSpec.UNSPECIFIED:
                return true;
            case View.MeasureSpec.AT_MOST:
                return specSize >= childSize;
            case View.MeasureSpec.EXACTLY:
                return specSize == childSize;
        }
        return false;
    }

    private void clearFlexLines() {
        mFlexLines.clear();
        mAnchorInfo.reset();
        mAnchorInfo.mPerpendicularCoordinate = 0;
    }

    private int getChildLeft(View view) {
        RecyclerView.LayoutParams params = (RecyclerView.LayoutParams)
                view.getLayoutParams();
        return getDecoratedLeft(view) - params.leftMargin;
    }

    private int getChildRight(View view) {
        RecyclerView.LayoutParams params = (RecyclerView.LayoutParams)
                view.getLayoutParams();
        return getDecoratedRight(view) + params.rightMargin;
    }

    private int getChildTop(View view) {
        RecyclerView.LayoutParams params = (RecyclerView.LayoutParams)
                view.getLayoutParams();
        return getDecoratedTop(view) - params.topMargin;
    }

    private int getChildBottom(View view) {
        RecyclerView.LayoutParams params = (RecyclerView.LayoutParams)
                view.getLayoutParams();
        return getDecoratedBottom(view) + params.bottomMargin;
    }
    private boolean isViewVisible(View view, boolean completelyVisible) {
        int left = getPaddingLeft();
        int top = getPaddingTop();
        int right = getWidth() - getPaddingRight();
        int bottom = getHeight() - getPaddingBottom();
        int childLeft = getChildLeft(view);
        int childTop = getChildTop(view);
        int childRight = getChildRight(view);
        int childBottom = getChildBottom(view);

        boolean horizontalCompletelyVisible = false;
        boolean horizontalPartiallyVisible = false;
        boolean verticalCompletelyVisible = false;
        boolean verticalPartiallyVisible = false;
        if (left <= childLeft && right >= childRight) {
            horizontalCompletelyVisible = true;
        }
        if (childLeft >= right || childRight >= left) {
            horizontalPartiallyVisible = true;
        }

        if (top <= childTop && bottom >= childBottom) {
            verticalCompletelyVisible = true;
        }
        if (childTop >= bottom || childBottom >= top) {
            verticalPartiallyVisible = true;
        }
        if (completelyVisible) {
            return horizontalCompletelyVisible && verticalCompletelyVisible;
        } else {
            return horizontalPartiallyVisible && verticalPartiallyVisible;
        }
    }
    @SuppressWarnings("WeakerAccess")
    public int findFirstVisibleItemPosition() {
        final View child = findOneVisibleChild(0, getChildCount(), false);
        return child == null ? NO_POSITION : getPosition(child);
    }

    @SuppressWarnings("WeakerAccess")
    public int findFirstCompletelyVisibleItemPosition() {
        final View child = findOneVisibleChild(0, getChildCount(), true);
        return child == null ? NO_POSITION : getPosition(child);
    }

    @SuppressWarnings("WeakerAccess")
    public int findLastVisibleItemPosition() {
        final View child = findOneVisibleChild(getChildCount() - 1, -1, false);
        return child == null ? NO_POSITION : getPosition(child);
    }
    @SuppressWarnings("WeakerAccess")
    public int findLastCompletelyVisibleItemPosition() {
        final View child = findOneVisibleChild(getChildCount() - 1, -1, true);
        return child == null ? NO_POSITION : getPosition(child);
    }
    private View findOneVisibleChild(int fromIndex, int toIndex, boolean completelyVisible) {
        int next = toIndex > fromIndex ? 1 : -1;
        for (int i = fromIndex; i != toIndex; i += next) {
            View view = getChildAt(i);
            if (isViewVisible(view, completelyVisible)) {
                return view;
            }
        }
        return null;
    }

    int getPositionToFlexLineIndex(int position) {
        assert mFlexboxHelper.mIndexToFlexLine != null;
        return mFlexboxHelper.mIndexToFlexLine[position];
    }
    public static class LayoutParams extends RecyclerView.LayoutParams implements FlexItem {
        private float mFlexGrow = FlexItem.FLEX_GROW_DEFAULT;

        private float mFlexShrink = FlexItem.FLEX_SHRINK_DEFAULT;
        private int mAlignSelf = AlignSelf.AUTO;
private float mFlexBasisPercent = FlexItem.FLEX_BASIS_PERCENT_DEFAULT;

        private int mMinWidth;
        private int mMinHeight;
        private int mMaxWidth = MAX_SIZE;
        private int mMaxHeight = MAX_SIZE;
private boolean mWrapBefore;

        @Override
        public int getWidth() {
            return width;
        }

        @Override
        public void setWidth(int width) {
            this.width = width;
        }

        @Override
        public int getHeight() {
            return height;
        }

        @Override
        public void setHeight(int height) {
            this.height = height;
        }

        @Override
        public float getFlexGrow() {
            return mFlexGrow;
        }

        @Override
        public void setFlexGrow(float flexGrow) {
            this.mFlexGrow = flexGrow;
        }

        @Override
        public float getFlexShrink() {
            return mFlexShrink;
        }

        @Override
        public void setFlexShrink(float flexShrink) {
            this.mFlexShrink = flexShrink;
        }

        @AlignSelf
        @Override
        public int getAlignSelf() {
            return mAlignSelf;
        }

        @Override
        public void setAlignSelf(@AlignSelf int alignSelf) {
            this.mAlignSelf = alignSelf;
        }

        @Override
        public int getMinWidth() {
            return mMinWidth;
        }

        @Override
        public void setMinWidth(int minWidth) {
            this.mMinWidth = minWidth;
        }

        @Override
        public int getMinHeight() {
            return mMinHeight;
        }

        @Override
        public void setMinHeight(int minHeight) {
            this.mMinHeight = minHeight;
        }

        @Override
        public int getMaxWidth() {
            return mMaxWidth;
        }

        @Override
        public void setMaxWidth(int maxWidth) {
            this.mMaxWidth = maxWidth;
        }

        @Override
        public int getMaxHeight() {
            return mMaxHeight;
        }

        @Override
        public void setMaxHeight(int maxHeight) {
            this.mMaxHeight = maxHeight;
        }

        @Override
        public boolean isWrapBefore() {
            return mWrapBefore;
        }

        @Override
        public void setWrapBefore(boolean wrapBefore) {
            this.mWrapBefore = wrapBefore;
        }

        @Override
        public float getFlexBasisPercent() {
            return mFlexBasisPercent;
        }

        @Override
        public void setFlexBasisPercent(float flexBasisPercent) {
            this.mFlexBasisPercent = flexBasisPercent;
        }

        @Override
        public int getMarginLeft() {
            return leftMargin;
        }

        @Override
        public int getMarginTop() {
            return topMargin;
        }

        @Override
        public int getMarginRight() {
            return rightMargin;
        }

        @Override
        public int getMarginBottom() {
            return bottomMargin;
        }

        public LayoutParams(Context c, AttributeSet attrs) {
            super(c, attrs);
        }

        public LayoutParams(int width, int height) {
            super(width, height);
        }

        public LayoutParams(ViewGroup.MarginLayoutParams source) {
            super(source);
        }

        public LayoutParams(ViewGroup.LayoutParams source) {
            super(source);
        }

        public LayoutParams(RecyclerView.LayoutParams source) {
            super(source);
        }

        public LayoutParams(CustomeLayoutManager.LayoutParams source) {
            super(source);

            mFlexGrow = source.mFlexGrow;
            mFlexShrink = source.mFlexShrink;
            mAlignSelf = source.mAlignSelf;
            mFlexBasisPercent = source.mFlexBasisPercent;
            mMinWidth = source.mMinWidth;
            mMinHeight = source.mMinHeight;
            mMaxWidth = source.mMaxWidth;
            mMaxHeight = source.mMaxHeight;
            mWrapBefore = source.mWrapBefore;
        }

        @Override
        public int getOrder() {
            return FlexItem.ORDER_DEFAULT;
        }

        @Override
        public void setOrder(int order) {
            throw new UnsupportedOperationException("Setting the order in the "
                    + "CustomeLayoutManager is not supported. Use FlexboxLayout "
                    + "if you need to reorder using the attribute.");
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeFloat(this.mFlexGrow);
            dest.writeFloat(this.mFlexShrink);
            dest.writeInt(this.mAlignSelf);
            dest.writeFloat(this.mFlexBasisPercent);
            dest.writeInt(this.mMinWidth);
            dest.writeInt(this.mMinHeight);
            dest.writeInt(this.mMaxWidth);
            dest.writeInt(this.mMaxHeight);
            dest.writeByte(this.mWrapBefore ? (byte) 1 : (byte) 0);
            dest.writeInt(this.bottomMargin);
            dest.writeInt(this.leftMargin);
            dest.writeInt(this.rightMargin);
            dest.writeInt(this.topMargin);
            dest.writeInt(this.height);
            dest.writeInt(this.width);
        }

        protected LayoutParams(Parcel in) {
            super(WRAP_CONTENT, WRAP_CONTENT);
            this.mFlexGrow = in.readFloat();
            this.mFlexShrink = in.readFloat();
            this.mAlignSelf = in.readInt();
            this.mFlexBasisPercent = in.readFloat();
            this.mMinWidth = in.readInt();
            this.mMinHeight = in.readInt();
            this.mMaxWidth = in.readInt();
            this.mMaxHeight = in.readInt();
            this.mWrapBefore = in.readByte() != 0;
            this.bottomMargin = in.readInt();
            this.leftMargin = in.readInt();
            this.rightMargin = in.readInt();
            this.topMargin = in.readInt();
            this.height = in.readInt();
            this.width = in.readInt();
        }

        public static final Parcelable.Creator<CustomeLayoutManager.LayoutParams> CREATOR
                = new Parcelable.Creator<CustomeLayoutManager.LayoutParams>() {
            @Override
            public CustomeLayoutManager.LayoutParams createFromParcel(Parcel source) {
                return new CustomeLayoutManager.LayoutParams(source);
            }

            @Override
            public CustomeLayoutManager.LayoutParams[] newArray(int size) {
                return new CustomeLayoutManager.LayoutParams[size];
            }
        };
    }

    private class AnchorInfo {

        private int mPosition;

        private int mFlexLinePosition;

        private int mCoordinate;
        private int mPerpendicularCoordinate = 0;

        private boolean mLayoutFromEnd;

        private boolean mValid;

        private boolean mAssignedFromSavedState;

        private void reset() {
            mPosition = NO_POSITION;
            mFlexLinePosition = NO_POSITION;
            mCoordinate = INVALID_OFFSET;
            mValid = false;
            mAssignedFromSavedState = false;
            if (isMainAxisDirectionHorizontal()) {
                if (mFlexWrap == FlexWrap.NOWRAP) {
                    mLayoutFromEnd = mFlexDirection == FlexDirection.ROW_REVERSE;
                } else {
                    mLayoutFromEnd = mFlexWrap == FlexWrap.WRAP_REVERSE;
                }
            } else {
                if (mFlexWrap == FlexWrap.NOWRAP) {
                    mLayoutFromEnd = mFlexDirection == FlexDirection.COLUMN_REVERSE;
                } else {
                    mLayoutFromEnd = mFlexWrap == FlexWrap.WRAP_REVERSE;
                }
            }
        }

        private void assignCoordinateFromPadding() {
            if (!isMainAxisDirectionHorizontal() && mIsRtl) {
                mCoordinate = mLayoutFromEnd ? mOrientationHelper.getEndAfterPadding()
                        : getWidth() - mOrientationHelper.getStartAfterPadding();
            } else {
                mCoordinate = mLayoutFromEnd ? mOrientationHelper.getEndAfterPadding()
                        : mOrientationHelper.getStartAfterPadding();
            }
        }

        private void assignFromView(View anchor) {
            if (!isMainAxisDirectionHorizontal() && mIsRtl) {
                if (mLayoutFromEnd) {
                    mCoordinate = mOrientationHelper.getDecoratedStart(anchor) +
                            mOrientationHelper.getTotalSpaceChange();
                } else {
                    mCoordinate = mOrientationHelper.getDecoratedEnd(anchor);
                }
            } else {
                if (mLayoutFromEnd) {
                    mCoordinate = mOrientationHelper.getDecoratedEnd(anchor) +
                            mOrientationHelper.getTotalSpaceChange();
                } else {
                    mCoordinate = mOrientationHelper.getDecoratedStart(anchor);
                }
            }
            mPosition = getPosition(anchor);
            mAssignedFromSavedState = false;
            assert mFlexboxHelper.mIndexToFlexLine != null;
            int flexLinePosition = mFlexboxHelper.mIndexToFlexLine[mPosition];
            mFlexLinePosition = flexLinePosition != NO_POSITION ? flexLinePosition : 0;
            if (mFlexLines.size() > mFlexLinePosition) {
                mPosition = mFlexLines.get(mFlexLinePosition).mFirstIndex;
            }
        }

        @Override
        public String toString() {
            return "AnchorInfo{" +
                    "mPosition=" + mPosition +
                    ", mFlexLinePosition=" + mFlexLinePosition +
                    ", mCoordinate=" + mCoordinate +
                    ", mPerpendicularCoordinate=" + mPerpendicularCoordinate +
                    ", mLayoutFromEnd=" + mLayoutFromEnd +
                    ", mValid=" + mValid +
                    ", mAssignedFromSavedState=" + mAssignedFromSavedState +
                    '}';
        }
    }

    private static class LayoutState {

        private final static int SCROLLING_OFFSET_NaN = Integer.MIN_VALUE;

        private static final int LAYOUT_START = -1;
        private static final int LAYOUT_END = 1;

        private static final int ITEM_DIRECTION_TAIL = 1;

        private int mAvailable;

        private boolean mInfinite;

        private int mFlexLinePosition;

        private int mPosition;
        private int mOffset;
        private int mScrollingOffset;

        private int mLastScrollDelta;

        private int mItemDirection = CustomeLayoutManager.LayoutState.ITEM_DIRECTION_TAIL;

        private int mLayoutDirection = CustomeLayoutManager.LayoutState.LAYOUT_END;

        private boolean mShouldRecycle;
        private boolean hasMore(RecyclerView.State state, List<FlexLine> flexLines) {
            return mPosition >= 0 && mPosition < state.getItemCount() &&
                    mFlexLinePosition >= 0 && mFlexLinePosition < flexLines.size();
        }

        @Override
        public String toString() {
            return "LayoutState{" +
                    "mAvailable=" + mAvailable +
                    ", mFlexLinePosition=" + mFlexLinePosition +
                    ", mPosition=" + mPosition +
                    ", mOffset=" + mOffset +
                    ", mScrollingOffset=" + mScrollingOffset +
                    ", mLastScrollDelta=" + mLastScrollDelta +
                    ", mItemDirection=" + mItemDirection +
                    ", mLayoutDirection=" + mLayoutDirection +
                    '}';
        }
    }
    private static class SavedState implements Parcelable {
        private int mAnchorPosition;
        private int mAnchorOffset;

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(this.mAnchorPosition);
            dest.writeInt(this.mAnchorOffset);
        }

        SavedState() {
        }

        private SavedState(Parcel in) {
            this.mAnchorPosition = in.readInt();
            this.mAnchorOffset = in.readInt();
        }

        private SavedState(CustomeLayoutManager.SavedState savedState) {
            mAnchorPosition = savedState.mAnchorPosition;
            mAnchorOffset = savedState.mAnchorOffset;
        }

        private void invalidateAnchor() {
            mAnchorPosition = NO_POSITION;
        }

        private boolean hasValidAnchor(int itemCount) {
            return mAnchorPosition >= 0 && mAnchorPosition < itemCount;
        }

        public static final Creator<CustomeLayoutManager.SavedState> CREATOR = new Creator<CustomeLayoutManager.SavedState>() {
            @Override
            public CustomeLayoutManager.SavedState createFromParcel(Parcel source) {
                return new CustomeLayoutManager.SavedState(source);
            }

            @Override
            public CustomeLayoutManager.SavedState[] newArray(int size) {
                return new CustomeLayoutManager.SavedState[size];
            }
        };

        @Override
        public String toString() {
            return "SavedState{" +
                    "mAnchorPosition=" + mAnchorPosition +
                    ", mAnchorOffset=" + mAnchorOffset +
                    '}';
        }
    }
}
